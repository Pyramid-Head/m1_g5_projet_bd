/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

/**
 *
 * @author Julien Rull
 */
public class PhotoHebergement {
    private long idPhoto;
    private long idHebergement;
    private String urlPhoto;

    public PhotoHebergement(long idPhoto, long idHebergement, String urlPhoto) {
        this.idPhoto = idPhoto;
        this.idHebergement = idHebergement;
        this.urlPhoto = urlPhoto;
    }

    public long getIdPhoto() {
        return idPhoto;
    }

    public long getIdHebergement() {
        return idHebergement;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setIdPhoto(long idPhoto) {
        this.idPhoto = idPhoto;
    }

    public void setIdHebergement(long idHebergement) {
        this.idHebergement = idHebergement;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }
    
    
}
