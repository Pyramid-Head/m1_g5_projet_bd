/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

/**
 *
 * @author Julien Rull
 */
public class NoteFestival {
    private long idNotation;
    private long idcompte;
    private long idFestival;
    private int note;

    public NoteFestival(long idNotation, long idcompte, long idFestival, int note) {
        this.idNotation = idNotation;
        this.idcompte = idcompte;
        this.idFestival = idFestival;
        this.note = note;
    }

    public long getIdNotation() {
        return idNotation;
    }

    public long getIdcompte() {
        return idcompte;
    }

    public long getIdFestival() {
        return idFestival;
    }

    public int getNote() {
        return note;
    }

    public void setIdNotation(long idNotation) {
        this.idNotation = idNotation;
    }

    public void setIdcompte(long idcompte) {
        this.idcompte = idcompte;
    }

    public void setIdFestival(long idFestival) {
        this.idFestival = idFestival;
    }

    public void setNote(int note) {
        this.note = note;
    }
    
    
    
}
