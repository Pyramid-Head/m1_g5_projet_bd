/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

/**
 *
 * @author Julien Rull
 */
public class NoteInfrastructure {
    private long idNotation;
    private long idCompte;
    private long idInfrastructure;
    private int note;

    public NoteInfrastructure(long idNotation, long idCompte, long idInfrastructure, int note) {
        this.idNotation = idNotation;
        this.idCompte = idCompte;
        this.idInfrastructure = idInfrastructure;
        this.note = note;
    }

    public long getIdNotation() {
        return idNotation;
    }

    public long getIdCompte() {
        return idCompte;
    }

    public long getIdInfrastructure() {
        return idInfrastructure;
    }

    public int getNote() {
        return note;
    }

    public void setIdNotation(long idNotation) {
        this.idNotation = idNotation;
    }

    public void setIdCompte(long idCompte) {
        this.idCompte = idCompte;
    }

    public void setIdInfrastructure(long idInfrastructure) {
        this.idInfrastructure = idInfrastructure;
    }

    public void setNote(int note) {
        this.note = note;
    }
    
    
}
