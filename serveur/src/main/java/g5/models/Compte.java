/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

/**
 *
 * @author Julien Rull
 */
public class Compte {
    private long idCompte;
    
    private String motDePasse;
    private String nom;
    private String prenom;
    private String courriel;
    private int telephone;
    private String facebook;
    private String hebergeur;
    private String organisateur;

    public Compte(long idCompte, String motDePasse, String nom, String prenom, String courriel, int telephone, String facebook, String hebergeur, String organisateur) {
        this.idCompte = idCompte;
        this.motDePasse = motDePasse;
        this.nom = nom;
        this.prenom = prenom;
        this.courriel = courriel;
        this.telephone = telephone;
        this.facebook = facebook;
        this.hebergeur = hebergeur;
        this.organisateur = organisateur;
    }

    public long getIdCompte() {
        return idCompte;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getCourriel() {
        return courriel;
    }

    public int getTelephone() {
        return telephone;
    }

    public String getFacebook() {
        return facebook;
    }

    public String getHebergeur() {
        return hebergeur;
    }

    public String getOrganisateur() {
        return organisateur;
    }

    public void setIdCompte(long idCompte) {
        this.idCompte = idCompte;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public void setHebergeur(String hebergeur) {
        this.hebergeur = hebergeur;
    }

    public void setOrganisateur(String organisateur) {
        this.organisateur = organisateur;
    }
    
    
}
