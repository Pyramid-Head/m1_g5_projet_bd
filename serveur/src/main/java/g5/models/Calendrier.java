/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

import java.util.Date;

/**
 *
 * @author Julien Rull
 */
public class Calendrier {
    private Date dateCal;

    public Calendrier(Date dateCal) {
        this.dateCal = dateCal;
    }

    public Date getDateCal() {
        return dateCal;
    }

    public void setDateCal(Date dateCal) {
        this.dateCal = dateCal;
    }
    
    
}
