/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

/**
 *
 * @author Julien Rull
 */
public class Departement {
    private String numDepatement;
    private String nomDepartement;
    private String region;

    public Departement(String numDepatement, String nomDepartement, String region) {
        this.numDepatement = numDepatement;
        this.nomDepartement = nomDepartement;
        this.region = region;
    }

    public String getNumDepatement() {
        return numDepatement;
    }

    public String getNomDepartement() {
        return nomDepartement;
    }

    public String getRegion() {
        return region;
    }

    public void setNumDepatement(String numDepatement) {
        this.numDepatement = numDepatement;
    }

    public void setNomDepartement(String nomDepartement) {
        this.nomDepartement = nomDepartement;
    }

    public void setRegion(String region) {
        this.region = region;
    }
    
    
}
