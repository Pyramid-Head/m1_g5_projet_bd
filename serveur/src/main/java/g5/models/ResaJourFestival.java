/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

import java.util.Date;

/**
 *
 * @author Julien Rull
 */
public class ResaJourFestival {
    private long idResevation;
    private long idJourF; // date
    private  long nbrPlace;
    private Date dateHeureMiseEnPanier;

    public ResaJourFestival(long idResevation, long idJourF, Date dateHeureMiseEnPanier , long nbrPlace) {
        this.idResevation = idResevation;
        this.idJourF = idJourF;
        this.dateHeureMiseEnPanier = dateHeureMiseEnPanier;
        this.nbrPlace = nbrPlace;
    }

    public ResaJourFestival(Integer idReserv, int jourFestId, long nbrPlace) {
        this.idResevation = idReserv;
        this.idJourF = jourFestId;
        this.nbrPlace = nbrPlace;
    }



    public long getNbrPlace() {
        return nbrPlace;
    }

    public void setNbrPlace(long nbrPlace) {
        this.nbrPlace = nbrPlace;
    }
    public long getIdResevation() {
        return idResevation;
    }

    public long getIdJourF() {
        return idJourF;
    }

    public Date getDateHeureMiseEnPanier() {
        return dateHeureMiseEnPanier;
    }

    public void setIdResevation(long idResevation) {
        this.idResevation = idResevation;
    }

    public void setIdJourF(long idJourF) {
        this.idJourF = idJourF;
    }

    public void setDateHeureMiseEnPanier(Date dateHeureMiseEnPanier) {
        this.dateHeureMiseEnPanier = dateHeureMiseEnPanier;
    }
    
    
}
