/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

/**
 *
 * @author Julien Rull
 */
public class Hebergement {
    private long idHebergement;
    private long idInfrastructure;
    private String descriptionHeberg;
    private String typeHeberg;
    private int capacite;
    private double tarifForfaitaire;
    private int nbPlacesAdultes;
    private int nbPlacesEnfants;
    private double tarifEnfant;
    private double tarifAdulte;

    public Hebergement(long idHebergement, long idInfrastructure, String descriptionHeberg, String typeHeberg, int capacite, double tarifForfaitaire, int nbPlacesAdultes, int nbPlacesEnfants, double tarifEnfant, double tarifAdulte) {
        this.idHebergement = idHebergement;
        this.idInfrastructure = idInfrastructure;
        this.descriptionHeberg = descriptionHeberg;
        this.typeHeberg = typeHeberg;
        this.capacite = capacite;
        this.tarifForfaitaire = tarifForfaitaire;
        this.nbPlacesAdultes = nbPlacesAdultes;
        this.nbPlacesEnfants = nbPlacesEnfants;
        this.tarifEnfant = tarifEnfant;
        this.tarifAdulte = tarifAdulte;
    }

    public long getIdHebergement() {
        return idHebergement;
    }

    public long getIdInfrastructure() {
        return idInfrastructure;
    }

    public String getDescriptionHeberg() {
        return descriptionHeberg;
    }

    public String getTypeHeberg() {
        return typeHeberg;
    }

    public int getCapacite() {
        return capacite;
    }

    public double getTarifForfaitaire() {
        return tarifForfaitaire;
    }

    public int getNbPlacesAdultes() {
        return nbPlacesAdultes;
    }

    public int getNbPlacesEnfants() {
        return nbPlacesEnfants;
    }

    public double getTarifEnfant() {
        return tarifEnfant;
    }

    public double getTarifAdulte() {
        return tarifAdulte;
    }

    public void setIdHebergement(long idHebergement) {
        this.idHebergement = idHebergement;
    }

    public void setIdInfrastructure(long idInfrastructure) {
        this.idInfrastructure = idInfrastructure;
    }

    public void setDescriptionHeberg(String descriptionHeberg) {
        this.descriptionHeberg = descriptionHeberg;
    }

    public void setTypeHeberg(String typeHeberg) {
        this.typeHeberg = typeHeberg;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public void setTarifForfaitaire(double tarifForfaitaire) {
        this.tarifForfaitaire = tarifForfaitaire;
    }

    public void setNbPlacesAdultes(int nbPlacesAdultes) {
        this.nbPlacesAdultes = nbPlacesAdultes;
    }

    public void setNbPlacesEnfants(int nbPlacesEnfants) {
        this.nbPlacesEnfants = nbPlacesEnfants;
    }

    public void setTarifEnfant(double tarifEnfant) {
        this.tarifEnfant = tarifEnfant;
    }

    public void setTarifAdulte(double tarifAdulte) {
        this.tarifAdulte = tarifAdulte;
    }
    
    
}
