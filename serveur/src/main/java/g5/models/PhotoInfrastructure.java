/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

/**
 *
 * @author Julien Rull
 */
public class PhotoInfrastructure {
    private long idPhoto;
    private long idInfrastructure;
    private String urlPhoto;

    public PhotoInfrastructure(long idPhoto, long idInfrastructure, String urlPhoto) {
        this.idPhoto = idPhoto;
        this.idInfrastructure = idInfrastructure;
        this.urlPhoto = urlPhoto;
    }

    public long getIdPhoto() {
        return idPhoto;
    }

    public long getIdInfrastructure() {
        return idInfrastructure;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setIdPhoto(long idPhoto) {
        this.idPhoto = idPhoto;
    }

    public void setIdInfrastructure(long idInfrastructure) {
        this.idInfrastructure = idInfrastructure;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }
    
    
}
