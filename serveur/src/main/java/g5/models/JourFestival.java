/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

import java.util.Date;

/**
 *
 * @author Julien Rull
 */
public class JourFestival {
    private long idJourF;
    private long idFestival;
    private Date dateJf;
    private double prix;
    private int placesLibres;
    private String typePlace;

    public JourFestival(long idJourF, long idFestival, Date dateJf, double prix, int placesLibres, String typePlace) {
        this.idJourF = idJourF;
        this.idFestival = idFestival;
        this.dateJf = dateJf;
        this.prix = prix;
        this.placesLibres = placesLibres;
        this.typePlace = typePlace;
    }

    public long getIdJourF() {
        return idJourF;
    }

    public long getIdFestival() {
        return idFestival;
    }

    public Date getDateJf() {
        return dateJf;
    }

    public double getPrix() {
        return prix;
    }

    public int getPlacesLibres() {
        return placesLibres;
    }

    public String getTypePlace() {
        return typePlace;
    }

    public void setIdJourF(long idJourF) {
        this.idJourF = idJourF;
    }

    public void setIdFestival(long idFestival) {
        this.idFestival = idFestival;
    }

    public void setDateJf(Date dateJf) {
        this.dateJf = dateJf;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public void setPlacesLibres(int placesLibres) {
        this.placesLibres = placesLibres;
    }

    public void setTypePlace(String typePlace) {
        this.typePlace = typePlace;
    }
    
    
}
