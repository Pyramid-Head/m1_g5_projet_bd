/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

import java.util.Date;

/**
 *
 * @author Julien Rull
 */
public class ResaHebergement {
    private long idResevation;
    private long idHebergement;
    private Date dateDebut;
    private Date dateFin;
    
    private int nbEnfants;
    private int nbAdultes;
    private Date dateHeureMiseEnPanier;

    public ResaHebergement(long idResevation, long idHebergement, Date dateDebut, Date dateFin, int nbEnfants, int nbAdultes, Date dateHeureMiseEnPanier) {
        this.idResevation = idResevation;
        this.idHebergement = idHebergement;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.nbEnfants = nbEnfants;
        this.nbAdultes = nbAdultes;
        this.dateHeureMiseEnPanier = dateHeureMiseEnPanier;
    }

    public long getIdResevation() {
        return idResevation;
    }

    public long getIdHebergement() {
        return idHebergement;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public int getNbEnfants() {
        return nbEnfants;
    }

    public int getNbAdultes() {
        return nbAdultes;
    }

    public Date getDateHeureMiseEnPanier() {
        return dateHeureMiseEnPanier;
    }

    public void setIdResevation(long idResevation) {
        this.idResevation = idResevation;
    }

    public void setIdHebergement(long idHebergement) {
        this.idHebergement = idHebergement;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public void setNbEnfants(int nbEnfants) {
        this.nbEnfants = nbEnfants;
    }

    public void setNbAdultes(int nbAdultes) {
        this.nbAdultes = nbAdultes;
    }

    public void setDateHeureMiseEnPanier(Date dateHeureMiseEnPanier) {
        this.dateHeureMiseEnPanier = dateHeureMiseEnPanier;
    }
    
    
}
