/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

import java.util.Date;

/**
 *
 * @author Julien Rull
 */
public class DispoHebergement {
    private long idHebergement;
    private Date dateCal;

    public DispoHebergement(long idHebergement, Date dateCal) {
        this.idHebergement = idHebergement;
        this.dateCal = dateCal;
    }

    public long getIdHebergement() {
        return idHebergement;
    }

    public Date getDateCal() {
        return dateCal;
    }

    public void setIdHebergement(long idHebergement) {
        this.idHebergement = idHebergement;
    }

    public void setDateCal(Date dateCal) {
        this.dateCal = dateCal;
    }
    
    
            
}
