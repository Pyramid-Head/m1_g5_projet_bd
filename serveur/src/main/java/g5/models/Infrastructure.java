/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

/**
 *
 * @author Julien Rull
 */
public class Infrastructure {
    
    private long idInfrastructure;
    private long idCompte;
    private String numDepartement;
    private String nom;
    private String classement;
    private String categorie;
    private String telephone;
    private String siteInternet;
    private String courriel;
    private String coordonneesGps;
    private int note;
    private double montantEnCaisse;
    private String typeInfra;


    private  Double prixMin;

    public Infrastructure(long idInfrastructure, long idCompte, String numDepartement, String nom, String classement, String categorie, String telephone, String siteInternet, String courriel, String coordonneesGps, int note, double montantEnCaisse, String typeInfra) {
        this.idInfrastructure = idInfrastructure;
        this.idCompte = idCompte;
        this.numDepartement = numDepartement;
        this.nom = nom;
        this.classement = classement;
        this.categorie = categorie;
        this.telephone = telephone;
        this.siteInternet = siteInternet;
        this.courriel = courriel;
        this.coordonneesGps = coordonneesGps;
        this.note = note;
        this.montantEnCaisse = montantEnCaisse;
        this.typeInfra = typeInfra;
    }

    public Infrastructure(int idInfrastructure, String numDepartement, String nom, String classement,
                          String categorie, String telephone, String siteInternet, String courriel,
                          String coordonneesGps, int note, String typeInfra, Double minPrix) {
        this.idInfrastructure = idInfrastructure;
        this.numDepartement = numDepartement;
        this.nom = nom;
        this.classement = classement;
        this.categorie = categorie;
        this.telephone = telephone;
        this.siteInternet = siteInternet;
        this.courriel = courriel;
        this.coordonneesGps = coordonneesGps;
        this.note = note;
        this.typeInfra = typeInfra;
        this.prixMin=minPrix;

    }


    public Double getPrixMin() {
        return prixMin;
    }

    public void setPrixMin(Double prixMin) {
        this.prixMin = prixMin;
    }
    public long getIdInfrastructure() {
        return idInfrastructure;
    }

    public long getIdCompte() {
        return idCompte;
    }

    public String getNumDepartement() {
        return numDepartement;
    }

    public String getNom() {
        return nom;
    }

    public String getClassement() {
        return classement;
    }

    public String getCategorie() {
        return categorie;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getSiteInternet() {
        return siteInternet;
    }

    public String getCourriel() {
        return courriel;
    }

    public String getCoordonneesGps() {
        return coordonneesGps;
    }

    public int getNote() {
        return note;
    }


    public double getMontantEnCaisse() {
        return montantEnCaisse;
    }

    public String getTypeInfra() {
        return typeInfra;
    }

    public void setIdInfrastructure(long idInfrastructure) {
        this.idInfrastructure = idInfrastructure;
    }

    public void setIdCompte(long idCompte) {
        this.idCompte = idCompte;
    }

    public void setNumDepartement(String numDepartement) {
        this.numDepartement = numDepartement;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setClassement(String classement) {
        this.classement = classement;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setSiteInternet(String siteInternet) {
        this.siteInternet = siteInternet;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    public void setCoordonneesGps(String coordonneesGps) {
        this.coordonneesGps = coordonneesGps;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public void setMontantEnCaisse(double montantEnCaisse) {
        this.montantEnCaisse = montantEnCaisse;
    }

    public void setTypeInfra(String typeInfra) {
        this.typeInfra = typeInfra;
    }
    
    
}
