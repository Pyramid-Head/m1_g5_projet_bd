/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

import java.util.Date;

/**
 *
 * @author Julien Rull
 */
public class Festival {
    private Long idFestival;
    private String numDepartement;
    private Long idCompte;
    private String nom;
    private String siteWeb;
    private String complementDomaine;
    private String commentaires;
    private Date dateDeCreation;
    private Date dateDeDebut;
    private Date dateDeFin;
    private String coordonneesGps;
    private Double montantEnCaisse;
    private String typeFest;
    private Integer numEdition;
    //private Double prixMin;

    public Festival(long idFestival, String numDepartement, long idCompte, String nom, String siteWeb, String complementDomaine,
                    String commentaires,int numEdition, Date dateDeCreation, Date dateDeDebut, Date dateDeFin, String coordonneesGps,
                    double montantEnCaisse, String typeFest) {
        this.idFestival = idFestival;
        this.numEdition =  numEdition;
        this.numDepartement = numDepartement;
        this.idCompte = idCompte;
        this.nom = nom;
        this.siteWeb = siteWeb;
        this.complementDomaine = complementDomaine;
        this.commentaires = commentaires;
        this.dateDeCreation = dateDeCreation;
        this.dateDeDebut = dateDeDebut;
        this.dateDeFin = dateDeFin;
        this.coordonneesGps = coordonneesGps;
        this.montantEnCaisse = montantEnCaisse;
        this.typeFest = typeFest;
       // this.prixMin = prixMin;
    }

    public Festival(long idFestival, String numDepartement, String nom,
                    Date dateDeDebut, Date dateDeFin, String typeFest,String siteWeb) {
        this.idFestival = idFestival;
        this.numDepartement = numDepartement;
        this.nom = nom;
        this.dateDeDebut = dateDeDebut;
        this.dateDeFin = dateDeFin;
        this.typeFest = typeFest;
        this.siteWeb=siteWeb;
       // this.prixMin = prixMin;
    }

    /*public Double getPrixMin() {
        return prixMin;
    }

    public void setPrixMin(Double prixMin) {
        this.prixMin = prixMin;
    }*/

    public Integer getNumEdition() {
        return numEdition == null? 0:numEdition;
    }

    public void setNumEdition(int numEdition) {
        this.numEdition = numEdition;
    }

    public long getIdFestival() {
        return idFestival== null? 0:idFestival;
    }

    public String getNumDepartement() {
        return numDepartement;
    }

    public Long getIdCompte() {
        return idCompte==null?0:idCompte;
    }

    public String getNom() {
        return nom==null?"":nom;
    }

    public String getSiteWeb() {
        return siteWeb==null?"":siteWeb;
    }

    public String getComplementDomaine() {
        return complementDomaine==null?"":complementDomaine;
    }

    public String getCommentaires() {
        return commentaires==null?"":commentaires;
    }

    public Date getDateDeCreation() {
        return dateDeCreation==null?null:dateDeCreation;
    }

    public Date getDateDeDebut() {
        return dateDeDebut;
    }

    public Date getDateDeFin() {
        return dateDeFin;
    }

    public String getCoordonneesGps() {
        return coordonneesGps;
    }

    public Double getMontantEnCaisse() {
        return montantEnCaisse;
    }

    public String getTypeFest() {
        return typeFest;
    }

    public void setIdFestival(long idFestival) {
        this.idFestival = idFestival;
    }

    public void setNumDepartement(String numDepartement) {
        this.numDepartement = numDepartement;
    }

    public void setIdCompte(long idCompte) {
        this.idCompte = idCompte;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public void setComplementDomaine(String complementDomaine) {
        this.complementDomaine = complementDomaine;
    }

    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }


    public void setDateDeCreation(Date dateDeCreation) {
        this.dateDeCreation = dateDeCreation;
    }

    public void setDateDeDebut(Date dateDeDebut) {
        this.dateDeDebut = dateDeDebut;
    }

    public void setDateDeFin(Date dateDeFin) {
        this.dateDeFin = dateDeFin;
    }

    public void setCoordonneesGps(String coordonneesGps) {
        this.coordonneesGps = coordonneesGps;
    }

    public void setMontantEnCaisse(double montantEnCaisse) {
        this.montantEnCaisse = montantEnCaisse;
    }

    public void setTypeFest(String typeFest) {
        this.typeFest = typeFest;
    }
}
