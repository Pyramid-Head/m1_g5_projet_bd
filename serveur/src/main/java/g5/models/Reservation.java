/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.models;

import java.util.Date;

/**
 *
 * @author Julien Rull
 */
public class Reservation {
    private long idReservation;
    private long idCompte;
    private Date datePaiement;
    private double montantTotal;

    public Reservation(long idReservation, long idCompte, Date datePaiement, double montantTotal) {
        this.idReservation = idReservation;
        this.idCompte = idCompte;
        this.datePaiement = datePaiement;
        this.montantTotal = montantTotal;
    }
    public Reservation(long idCompte, Date datePaiement, double montantTotal) {
        this.idReservation = idReservation;
        this.idCompte = idCompte;
        this.datePaiement = datePaiement;
        this.montantTotal = montantTotal;
    }

    public long getIdReservation() {
        return idReservation;
    }

    public long getIdCompte() {
        return idCompte;
    }

    public Date getDatePaiement() {
        return datePaiement;
    }

    public double getMontantTotal() {
        return montantTotal;
    }

    public void setIdReservation(long idReservation) {
        this.idReservation = idReservation;
    }

    public void setIdCompte(long idCompte) {
        this.idCompte = idCompte;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public void setMontantTotal(double montantTotal) {
        this.montantTotal = montantTotal;
    }
    
    
}
