package g5.backgroundThreads;

import g5.dbAccess.ConnectToDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ThreadTimer implements Runnable {

Connection conn ;
Statement stmt;

    public ThreadTimer() {
      try {
          conn = ConnectToDB.getConnection();
          stmt = conn.createStatement();
        } catch (SQLException e) {
          System.out.println("ERROR IN CREATING THREAD DELETER");
            e.printStackTrace();
        }
    }

        @Override
    public void run() {
            try {
        while (true){
            System.out.println("START DELETING!");
            //get all the unvalid reservations
            ArrayList<Integer> resaInvAL = new ArrayList<>();
            String hop = "SELECT IDRESERVATION FROM RESERVATION " +
                    "WHERE DATEDECREATION <= (CURRENT_TIMESTAMP - INTERVAL '30' minute(1)) " +
                    "AND DATEPAIEMENT IS NULL";
            ResultSet resaInvalide =  stmt.executeQuery(hop);
            System.out.println(hop);
            while (resaInvalide.next()){
                    resaInvAL.add(resaInvalide.getInt("IDRESERVATION"));
            }

            //loop on these unvalid reservations and delete their resaHeb then their ResaFest then them
                for (int i = 0; i<resaInvAL.size();i++){
                    stmt.executeQuery("DELETE  FROM RESAHEBERGEMENT WHERE IDRESERVATION =  " +resaInvAL.get(i));
                    stmt.executeQuery("DELETE  FROM RESAJOURFESTIVAL WHERE IDRESERVATION =  " +resaInvAL.get(i));
                    stmt.executeQuery("DELETE  FROM RESERVATION WHERE IDRESERVATION =  " +resaInvAL.get(i));
                }
            System.out.println("END DELETING!");
                Thread.sleep(1000 * 60 *10);
        }

            } catch (InterruptedException | SQLException e) {
                System.out.println("ERROR IN DELETING THREAD");
                e.printStackTrace();
            }

    }
}
