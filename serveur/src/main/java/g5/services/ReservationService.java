/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.services;

import com.google.gson.Gson;
import g5.dao.produits.implementations.oracle.*;
import g5.dao.produits.interfaces.*;
import g5.models.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Julien Rull
 */
public class ReservationService {
    
    ReservationDAO reservationDAO;
    ResaJourFestivalDAO jourFestReservationDAO;
    ResaHebergementDAO resaHebergementDAO;
    JourFestivalDAO jourFestivalDAO;
    FestivalDAO festivalDAO; 
    HebergementDAO hebergDAO;
    InfrastructureService infastructureService;
    FestivalService festService;
    public ReservationService(){
        this.reservationDAO = new OracleReservationDAO();
        this.jourFestReservationDAO = new OracleResaJourFestivalDAO();
        this.resaHebergementDAO = new OracleResaHebergementDAO();
        this.jourFestivalDAO = new OracleJourFestivalDAO();
        this.festivalDAO = new OracleFestivalDAO();
        this.infastructureService = new InfrastructureService();
        this.hebergDAO = new OracleHebergementDAO();
        this.festService = new FestivalService();
                
    }
    
    public boolean createService(long idFestival, int jour, int nbPlaceCat1, int nbPlaceCat2, long idReservation){
        
        return true;
    }


    //adding code patrick

    public String reserverJourFestival(String jourFestId, int compteId, String resaId, String nbrPlace ) throws Exception {
        JSONObject toReturn = new JSONObject();
        //checking reservation and creating it if not present...
        Integer idReserv;
        if( resaId == null||resaId.equals("")  || resaId.equals(" ")){ //there are no reservation... we should create a new one!
            Reservation resa = new Reservation(compteId,null,0);
            idReserv = reservationDAO.create(resa);
            if(idReserv == -1){ throw new Exception("Reservation Not Created! l"); }
            //toReturn.put("ReservationId", idReserv);
        } else {//reservation is already created... add a resaFestival for her!
            System.out.println("YA A5iii " + resaId);
            idReserv = Integer.parseInt(resaId);
           // toReturn.put("ReservationId", "");
        }


        String[] jourFestToReserveID =  jourFestId.split(",");
        String[] nbrPlaceParJourFest =  nbrPlace.split(",");
        ResaJourFestival rjf = null;
        for (int i = 0; i<jourFestToReserveID.length;i++){
            //Creating ResaJourFest
                try {
                    rjf = new ResaJourFestival(idReserv,
                            Integer.parseInt(jourFestToReserveID[i]),
                            Integer.parseInt(nbrPlaceParJourFest[i]));
                    jourFestReservationDAO.create(rjf);
                    toReturn.put("ERROR","NON");
                } catch (SQLException e) {
                    e.printStackTrace();
                    reservationDAO.delete(idReserv);
                    idReserv=null;
                    toReturn.put("ERROR","OUI");
                }
        }
        //toReturn.put("JourFest","Vous avez cree "+jourFestToReserveID.length+" nouveaux ResaJourFest!");
        
        long nbPlaces = rjf.getNbrPlace();

        if (idReserv != null){
            JourFestival jf = this.jourFestivalDAO.getJourFestivalById((int)rjf.getIdJourF());

            Festival f =  this.festivalDAO.getFestivalById((int)jf.getIdFestival());
            String departement = f.getNumDepartement();

            Date date = jf.getDateJf();
            toReturn.put("JourFest","Vous avez cree "+jourFestToReserveID.length+" nouveaux ResaJourFest!");

            toReturn.put("infrastructures", this.infastructureService.getInfrastructures(1, null,
                    departement, (int)nbPlaces, 0, null, null,
                    date, date, null,null));
        }

        toReturn.put("ReservationId", idReserv==null?"":idReserv);


        return toReturn.toJSONString();
    }

    public String reserverHebergement(String resaId, int compteId, int hebergementId, Integer nbEnfants, Integer nbAdultes, String dateDebut, String dateFin) throws Exception {
        JSONObject toReturn = new JSONObject();
        //checking reservation and creating it if not present...
        Integer idReserv;
        if( resaId == null||resaId.equals("")  || resaId.equals(" ")){ //there are no reservation... we should create a new one!
            Reservation resa = new Reservation(compteId,null,0);
            idReserv = reservationDAO.create(resa);
            if(idReserv == -1){ throw new Exception("Reservation Not Created! "); }
            toReturn.put("ReservationId",idReserv);
        } else {//reservation is already created... add a resaFestival for her!
            System.out.println("YA A5iii " + resaId);
            idReserv = Integer.parseInt(resaId);
        }


        Date dateD = UsedFunctions.StringToDate(dateDebut);
        Date datef= UsedFunctions.StringToDate(dateFin);

       ResaHebergement resaHebergement = new ResaHebergement(idReserv, hebergementId, dateD, datef, nbEnfants, nbAdultes, null);

        try {
            this.resaHebergementDAO.create(resaHebergement);
            toReturn.put("ERROR","NON");
        } catch (SQLException e) {
            e.printStackTrace();
            idReserv=null;
            reservationDAO.delete(idReserv);
            toReturn.put("ERROR","OUI");
        }

          ArrayList<Festival> festsToRet = (ArrayList<Festival>) festivalDAO.getFestivalsByHeberg(resaHebergement.getIdHebergement(),resaHebergement.getDateDebut(),
        resaHebergement.getDateFin(),resaHebergement.getNbAdultes());

          JSONArray festivalsJson = new JSONArray();
        toReturn.put("ReservationId", idReserv==null?"":idReserv);
        for(Festival f:festsToRet){
            festivalsJson.add(this.festivalToJson(f));
        }


        toReturn.put("festivals",festivalsJson);

        return toReturn.toJSONString();
    }




    public JSONObject festivalToJson(Festival festival){

        JSONObject festivalJson = new JSONObject();

        festivalJson.put("idFestival", festival.getIdFestival());
        festivalJson.put("numDepartement", festival.getNumDepartement()==null?0:festival.getNumDepartement());
        festivalJson.put("idCompte", festival.getIdCompte()==null?0:festival.getIdCompte());
        festivalJson.put("nom", festival.getNom()==null?"":festival.getNom());
        festivalJson.put("siteWeb", festival.getSiteWeb()==null?"":festival.getSiteWeb());
        festivalJson.put("complementDomaine", festival.getComplementDomaine()==null?"":festival.getComplementDomaine());
        festivalJson.put("commentaires", festival.getCommentaires()==null?"":festival.getCommentaires());
        festivalJson.put("numEdition", festival.getNumEdition()==null?0:festival.getNumEdition());
        festivalJson.put("dateDeCreation", festival.getDateDeCreation()==null?"":festival.getDateDeCreation().toString());
        festivalJson.put("dateDeDebut", festival.getDateDeDebut()==null?"":festival.getDateDeDebut().toString());
        festivalJson.put("dateDeFin", festival.getDateDeFin()==null?"":festival.getDateDeFin().toString());
        festivalJson.put("coordonneesGps", festival.getCoordonneesGps()==null?"":festival.getCoordonneesGps());
        festivalJson.put("montantEnCaisse", festival.getMontantEnCaisse()==null?0:festival.getMontantEnCaisse());
        festivalJson.put("typeFest", festival.getTypeFest()==null?"":festival.getTypeFest());


        return festivalJson;
    }




    public JSONObject getReservationInfo(Integer resaID){

        try {
            //getting reservation
            Reservation reservation = reservationDAO.getReservationById(resaID);

            //getting resa jour fest
            ArrayList<ResaJourFestival> resaJourFestivals
                    = (ArrayList<ResaJourFestival>) jourFestReservationDAO.getResaJourFestParReservation(resaID);


            //getting resa heberg
            ArrayList<ResaHebergement> resaHebergements
                    = (ArrayList<ResaHebergement>) resaHebergementDAO.getByResaId(resaID);


            Gson gson = new Gson(); //json parser
            JSONParser parser = new JSONParser();
            JSONArray resaJfJSON = new JSONArray();

            //casting array to JSON ARRAY resaJourFestivals
            for(int i = 0 ; i<resaJourFestivals.size();i++){
                String jfresaJSON = gson.toJson(resaJourFestivals.get(i));
                resaJfJSON.add(parser.parse(jfresaJSON));
            }


            JSONArray resaHebgJSON = new JSONArray();
            //casting array to JSON ARRAY resaHebergements
            for(int i = 0 ; i<resaHebergements.size();i++){
                String hebResaJSON = gson.toJson(resaHebergements.get(i));
                resaHebgJSON.add(parser.parse(hebResaJSON));
            }

            //jsoning reservation
            String resaStringJSON = gson.toJson(reservation);
            Object resaJSON = parser.parse(resaStringJSON);

            JSONObject toRet = new JSONObject();
            toRet.put("Reservation",resaJSON);
            toRet.put("ResaHebergement",resaHebgJSON);
            toRet.put("ResaJourFestival",resaJfJSON);

                return  toRet;
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
            return null;
        }

    }



    public boolean deleteReservation(Integer resaID) throws SQLException {
        reservationDAO.delete(resaID);
        return true;
    }


}



