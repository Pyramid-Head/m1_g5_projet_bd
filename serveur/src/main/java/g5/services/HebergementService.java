/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.services;


import com.google.gson.Gson;
import g5.dao.produits.implementations.oracle.OracleHebergementDAO;
import g5.dao.produits.implementations.oracle.OracleInfrastructureDAO;
import g5.dao.produits.interfaces.HebergementDAO;
import g5.models.Hebergement;
import g5.models.Infrastructure;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


/**
 *
 * @author Julien Rull
 */
public class HebergementService {
    
    private HebergementDAO hebergementDAO;
    
    public HebergementService(){
        this.hebergementDAO = new OracleHebergementDAO();
    }
    
    public String getHebergements(Integer idInfra, Integer nbEnfants, Integer nbAdultes) throws SQLException {
                
        List<Hebergement> hebergements = this.hebergementDAO.getByInfrastructureId(idInfra, nbEnfants, nbAdultes);
        JSONObject toRet = new JSONObject(); //to return json object
        Gson gson = new Gson(); //json parser

        JSONParser parser = new JSONParser();


        JSONArray hebergementsJSON = new JSONArray();

        try {
            //casting array to JSON ARRAY
            for(int i = 0 ; i<hebergements.size();i++){
                String hebergementJSON = gson.toJson(hebergements.get(i));
                hebergementsJSON.add(parser.parse(hebergementJSON));
            }

            toRet.put("hebergements",hebergementsJSON);
        } catch (org.json.simple.parser.ParseException e) {
            System.out.println("JSON PARSER ERROR!");
            e.printStackTrace();
        }


        System.out.println("toret = "+ toRet.toJSONString());
        return toRet.toJSONString();
    }
    
}
