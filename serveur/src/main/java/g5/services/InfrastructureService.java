/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.services;

import com.google.gson.Gson;
import g5.dao.produits.implementations.oracle.OracleInfrastructureDAO;
import g5.dao.produits.implementations.oracle.UsedFunctions;
import g5.dao.produits.interfaces.InfrastructureDAO;
import g5.models.Infrastructure;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class InfrastructureService {
    private InfrastructureDAO infrastructureDAO;
    
    public InfrastructureService(){
        this.infrastructureDAO = new OracleInfrastructureDAO();
    }
    
       public String getInfrastructures(int page, String nom, String departement, Integer nbrPlaceAdult, Integer nbrPlacEnfant ,
                                     Double prixMin, Double prixMax, String dateDeDebut, String dateDeFin,
                                     String typeInfra,String lieu) throws SQLException, ParseException {
                /*
        String nom = request.getParameter("nom");
        String departement = request.getParameter("departement");
        Integer nbAdultes =  request.getParameter("nbAdultes")==null?0:Integer.parseInt(request.getParameter("nbAdultes"));
        Integer nbEnfants = request.getParameter("nbEnfants")==null?0: Integer.parseInt(request.getParameter("nbEnfants"));
        Double prixMin = Double.parseDouble(request.getParameter("prixMin"));
        Double prixMax = Double.parseDouble(request.getParameter("prixMax"));
        String dateDeDebut = request.getParameter("dateDeDebut");
        String dateDeFin = request.getParameter("dateDeFin");
        String typesInfra = request.getParameter("typesInfra");
        */
        Date dateDebut = null;
        Date dateFin=null;
        if(dateDeDebut!=null && dateDeFin!=null){
            dateDebut  = UsedFunctions.StringToDate(dateDeDebut);
            dateFin = UsedFunctions.StringToDate(dateDeFin);
        }

        int offset = (page-1)*20;
        List<Infrastructure> infrastructures = this.infrastructureDAO.getByAnyParam(nom, departement,
                typeInfra, dateDebut, dateFin, prixMin, prixMax, nbrPlaceAdult, nbrPlacEnfant,
                lieu, offset, offset + 19);
        JSONObject toRet = new JSONObject(); //to return json object
        Gson gson = new Gson(); //json parser

        JSONParser parser = new JSONParser();


        JSONArray infrastructuredJSON = new JSONArray();

        try {
            //casting array to JSON ARRAY
            for(int i = 0 ; i<infrastructures.size();i++){
                String infrastructureJSON = gson.toJson(infrastructures.get(i));
                infrastructuredJSON.add(parser.parse(infrastructureJSON));
            }

            toRet.put("infrastructures",infrastructuredJSON);
        } catch (org.json.simple.parser.ParseException e) {
            System.out.println("JSON PARSER ERROR!");
            e.printStackTrace();
        }


        System.out.println("toret = "+ toRet.toJSONString());
        return toRet.toJSONString();
    }





    public JSONObject getInfrastructures(int page, String nom, String departement, Integer nbrPlaceAdult, Integer nbrPlacEnfant ,
                                     Double prixMin, Double prixMax, Date dateDeDebut, Date dateDeFin,
                                     String typeInfra,String lieu) throws SQLException, ParseException {
                /*
        String nom = request.getParameter("nom");
        String departement = request.getParameter("departement");
        Integer nbAdultes =  request.getParameter("nbAdultes")==null?0:Integer.parseInt(request.getParameter("nbAdultes"));
        Integer nbEnfants = request.getParameter("nbEnfants")==null?0: Integer.parseInt(request.getParameter("nbEnfants"));
        Double prixMin = Double.parseDouble(request.getParameter("prixMin"));
        Double prixMax = Double.parseDouble(request.getParameter("prixMax"));
        String dateDeDebut = request.getParameter("dateDeDebut");
        String dateDeFin = request.getParameter("dateDeFin");
        String typesInfra = request.getParameter("typesInfra");
        */
        Date dateDebut = null;
        Date dateFin=null;
        if(dateDeDebut!=null && dateDeFin!=null){
            dateDebut  = dateDeDebut;
            dateFin = dateDeFin;
        }

        int offset = page*20;
        List<Infrastructure> infrastructures = this.infrastructureDAO.getByAnyParam(nom, departement, typeInfra, dateDebut, dateFin, prixMin, prixMax, nbrPlaceAdult, nbrPlacEnfant,lieu, offset, offset + 19);
        JSONObject toRet = new JSONObject(); //to return json object
        Gson gson = new Gson(); //json parser

        JSONParser parser = new JSONParser();


        JSONArray infrastructuredJSON = new JSONArray();

        try {
            //casting array to JSON ARRAY
            for(int i = 0 ; i<infrastructures.size();i++){
                String infrastructureJSON = gson.toJson(infrastructures.get(i));
                infrastructuredJSON.add(parser.parse(infrastructureJSON));
            }

            toRet.put("infrastructures",infrastructuredJSON);
        } catch (org.json.simple.parser.ParseException e) {
            System.out.println("JSON PARSER ERROR!");
            e.printStackTrace();
        }


        System.out.println("toret = "+ toRet.toJSONString());
        return toRet;
    }









    public String getInfrastructuresByDep(String numDep, int nbAdultes, int nbEnfants, int offset, int limite) throws SQLException{

        
        List<Infrastructure> infrastructures = this.infrastructureDAO.getInfraByDept(numDep, nbAdultes, nbEnfants, offset, limite);
        JSONObject toRet = new JSONObject(); //to return json object
        Gson gson = new Gson(); //json parser

        JSONParser parser = new JSONParser();


        JSONArray infrastructuredJSON = new JSONArray();

        try {
            //casting array to JSON ARRAY
            for(int i = 0 ; i<infrastructures.size();i++){
                String infrastructureJSON = gson.toJson(infrastructures.get(i));
                infrastructuredJSON.add(parser.parse(infrastructureJSON));
            }

            toRet.put("infrastructures",infrastructuredJSON);
        } catch (org.json.simple.parser.ParseException e) {
            System.out.println("JSON PARSER ERROR!");
            e.printStackTrace();
        }


        System.out.println("toret = "+ toRet.toJSONString());
        return toRet.toJSONString();
        
    }
}
