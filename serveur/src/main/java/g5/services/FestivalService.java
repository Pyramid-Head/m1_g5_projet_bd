/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.services;

import com.google.gson.Gson;
import g5.dao.produits.implementations.oracle.OracleFestivalDAO;
import g5.dao.produits.implementations.oracle.OracleJourFestivalDAO;
import g5.dao.produits.interfaces.FestivalDAO;
import g5.models.Festival;
import g5.models.JourFestival;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class FestivalService {
    private FestivalDAO festivalDAO;
    private OracleJourFestivalDAO jourFesDAO;
    
    public FestivalService(){
        this.festivalDAO = new OracleFestivalDAO();
        this.jourFesDAO = new OracleJourFestivalDAO();

    }
    
    public String getFestivals(int numPage ,String nom,String lieu,Date dateDebut, Date dateFin,
                                                 Double prixMin, Double prixMax,
                                                String typeFest,int nbrPersonne) throws SQLException, ParseException{
        numPage--;
        int offset = (numPage * 20);
        Date deb = null;
        Date fin = null;
        if(dateDebut != null){
            deb = dateDebut;
        }
        
         if(dateFin != null){
            fin = dateFin;
        }

         /*
        Date dateDebut = null;
        Date dateFin=null;
        if(dateDeDebut!=null && dateDeFin!=null){
            dateDebut  = dateDeDebut;
            dateFin = dateDeFin;
        }
*/



        List<Festival> festivals = this.festivalDAO.getFestivalsByAnyParam(nom, lieu, deb, fin,
                                                  prixMin, prixMax, typeFest, nbrPersonne, offset, offset+19);
       
        JSONArray festivalsJson = new JSONArray();
        
        for(Festival f:festivals){
            festivalsJson.add(this.festivalToJson(f));
        }
         //System.out.println("Festival    " + festivals.get(0).getNom() + festivals.get(0).getSiteWeb()); 
        //System.out.println(festivalsJson.toJSONString());
        return festivalsJson.toJSONString();
    }
    
    public String getFestival(int id) throws SQLException{
        Festival festival = this.festivalDAO.getFestivalById(id);
        JSONArray festivalsJson = new JSONArray();
        festivalsJson.add(festivalToJson(festival));
        
        return festivalsJson.toJSONString();
        
    }



    public String getFestivalInfo(int id, int nbrPersonnes) throws SQLException {
        Festival festival = this.festivalDAO.getFestivalById(id);
        ArrayList<JourFestival> joursDuFest = (ArrayList<JourFestival>) this.jourFesDAO.getJoursDeFestival(id,nbrPersonnes);
        JSONObject toRet = new JSONObject(); //to return json object
        Gson gson = new Gson(); //json parser

       JSONParser parser = new JSONParser();

        String festivalJSON = gson.toJson(festival);
        JSONArray joursFest = new JSONArray();

        try {
            //casting array to JSON ARRAY
            for(int i = 0 ; i<joursDuFest.size();i++){
                String jourDuFestJSON = gson.toJson(joursDuFest.get(i));
                joursFest.add(parser.parse(jourDuFestJSON));
            }
            toRet.put("festivalInfo",(JSONObject) parser.parse(festivalJSON));
            toRet.put("jours",joursFest);
        } catch (org.json.simple.parser.ParseException e) {
            System.out.println("JSON PARSER ERROR!");
            e.printStackTrace();
        }


        System.out.println("toret = "+ toRet.toJSONString());
        return toRet.toJSONString();
    }


    public JSONObject festivalToJson(Festival festival){
        
        JSONObject festivalJson = new JSONObject();
        
        festivalJson.put("idFestival", festival.getIdFestival());
        festivalJson.put("numDepartement", festival.getNumDepartement()==null?0:festival.getNumDepartement());
        festivalJson.put("idCompte", festival.getIdCompte()==null?0:festival.getIdCompte());
        festivalJson.put("nom", festival.getNom()==null?"":festival.getNom());
        festivalJson.put("siteWeb", festival.getSiteWeb()==null?"":festival.getSiteWeb());
        festivalJson.put("complementDomaine", festival.getComplementDomaine()==null?"":festival.getComplementDomaine());
        festivalJson.put("commentaires", festival.getCommentaires()==null?"":festival.getCommentaires());
        festivalJson.put("numEdition", festival.getNumEdition()==null?0:festival.getNumEdition());
        festivalJson.put("dateDeCreation", festival.getDateDeCreation()==null?"":festival.getDateDeCreation().toString());
        festivalJson.put("dateDeDebut", festival.getDateDeDebut()==null?"":festival.getDateDeDebut().toString());
        festivalJson.put("dateDeFin", festival.getDateDeFin()==null?"":festival.getDateDeFin().toString());
        festivalJson.put("coordonneesGps", festival.getCoordonneesGps()==null?"":festival.getCoordonneesGps());
        festivalJson.put("montantEnCaisse", festival.getMontantEnCaisse()==null?0:festival.getMontantEnCaisse());
        festivalJson.put("typeFest", festival.getTypeFest()==null?"":festival.getTypeFest());
        
        
        return festivalJson;
    }


    public boolean deleteFest(int idFest) throws SQLException {
        festivalDAO.delete(idFest);
        return true;
    }


    public  boolean updateFest(int idFest,int compteID) throws SQLException {
        festivalDAO.update( idFest,compteID);
        return true;
    }

}
