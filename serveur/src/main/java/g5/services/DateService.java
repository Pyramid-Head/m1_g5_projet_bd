/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Julien Rull
 */
public class DateService {
    public static final String CLIENT_FORMAT = "MM/dd/yyyy";
    public static final String SERVER_FORMAT = "yyyy-MM-dd";

    public static Date changeServerDateFormat(String date) throws ParseException{
        SimpleDateFormat format = new SimpleDateFormat(SERVER_FORMAT);
        Date oldDateFormat = format.parse(date);
        format.applyPattern(CLIENT_FORMAT);
        String newDateS = format.format(oldDateFormat);
        Date newDate = format.parse(newDateS);
        return newDate;
    }
    
    public static Date changeClientDateFormat(String date) throws ParseException{
        SimpleDateFormat format = new SimpleDateFormat(CLIENT_FORMAT);
        Date oldDateFormat = format.parse(date);
        format.applyPattern(SERVER_FORMAT);
        String newDateS = format.format(oldDateFormat);
        Date newDate = format.parse(newDateS);
        return newDate;
    }
    
    public static Date toDateClient(String date) throws ParseException{
        SimpleDateFormat format = new SimpleDateFormat(CLIENT_FORMAT);
        Date dateD = format.parse(date);
        return dateD;
    }
}
