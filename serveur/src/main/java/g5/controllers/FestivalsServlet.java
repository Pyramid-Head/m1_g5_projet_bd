/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.controllers;

import g5.dao.produits.implementations.oracle.UsedFunctions;
import g5.services.FestivalService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Julien Rull
 */
public class FestivalsServlet extends HttpServlet {
    

    private FestivalService fastivalService;

    private HttpServletResponse setConstHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        response.addHeader("Access-Control-Max-Age", "1728000");
        return response;
    }


    public FestivalsServlet(){
        this.fastivalService = new FestivalService();
    }
     
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            String messageJSON="";
        response.setContentType("application/json");
            response = setConstHeaders(response);

        String name = request.getParameter("name");
        String lieu = request.getParameter("lieu");
        String dateDebut = request.getParameter("dateDebut");
        String dateFin = request.getParameter("dateFin");
        String prixMin = request.getParameter("prixMin");
        String prixMax = request.getParameter("prixMax");
        String nbAdultes = request.getParameter("nbAdultes");
        String nbEnfants = request.getParameter("nbEnfants");
        String typeFest = request.getParameter("typeFest");
        String numPage = request.getParameter("numPage");

        Double prixMinD = null;
        Double prixMaxD = null;
        Integer nbAdultesI = null;
        Integer nbEnfantsI = null;
        Integer somme = 1;
        Date datdeb = null;
        Date datfin = null;

        if(dateDebut!=null && !dateDebut.equals("")){
            try {
                datdeb = UsedFunctions.StringToDate(dateDebut);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(dateFin!=null && !dateFin.equals("")){
            try {
                datfin = UsedFunctions.StringToDate(dateFin);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }



        if(prixMin != null && !prixMin.equals("")){
            prixMinD = Double.parseDouble(prixMin);
        }

        if(prixMax != null && !prixMax.equals("")){
            prixMaxD = Double.parseDouble(prixMax);
        }

        if(nbAdultes != null && !nbAdultes.equals("")){
            somme = new Integer(0);
            nbAdultesI = Integer.parseInt(nbAdultes);
            somme += nbAdultesI;
        }

        if(nbEnfants != null && !nbEnfants.equals("")){
            if(nbEnfants == null){
                somme = new Integer(0);
            }
            nbEnfantsI = Integer.parseInt(nbEnfants);
            somme += nbEnfantsI;
        }

        if(numPage != null && !numPage.equals("")){
            try {
                messageJSON = this.fastivalService.getFestivals(Integer.parseInt(numPage) ,name, lieu, datdeb, datfin,
                        prixMinD, prixMaxD,
                        typeFest,somme);
            } catch (SQLException ex) {
                Logger.getLogger(FestivalsServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(FestivalsServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            throw new NullPointerException("Aucun numéro de page reçu !");
        }

        if (messageJSON != null) {
            response.getWriter().println(messageJSON);
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            System.out.println("La réponse GET est null !");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
            
    }


}
