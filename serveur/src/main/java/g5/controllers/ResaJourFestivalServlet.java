package g5.controllers;

import g5.services.ReservationService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResaJourFestivalServlet extends HttpServlet {

    private ReservationService resaServ;
    

    public ResaJourFestivalServlet(){
        this.resaServ = new ReservationService();
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Procedure Started");
        String jourFestId = req.getParameter("idJourFest");
        String resaId =req.getParameter("reservationId");



        int compteId =Integer.parseInt(req.getParameter("compteId"));
        String nbPlace =req.getParameter("nbrPlace");

        try {
            String responce = resaServ.reserverJourFestival(jourFestId,compteId,resaId,nbPlace);

            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
            resp.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
            resp.addHeader("Access-Control-Max-Age", "1728000");

            resp.setContentType("application/json");

                resp.getWriter().println(responce);
                resp.setStatus(HttpServletResponse.SC_OK);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
