/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.controllers;

import g5.services.FestivalService;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Julien Rull
 */
public class FestivalServlet extends HttpServlet {
    
    public static String messageJSON = "";
    private FestivalService fastivalService;

    private HttpServletResponse setConstHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        response.addHeader("Access-Control-Max-Age", "1728000");
        return response;
    }


    public FestivalServlet(){
        this.fastivalService = new FestivalService();
    }
     
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            response.setContentType("application/json");
            response = setConstHeaders(response);
            if (messageJSON != null) {
            response.getWriter().println(messageJSON);
            response.setStatus(HttpServletResponse.SC_OK);

        } else {
                System.out.println("La réponse GET est null !");
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
    }


    /*____________________________________________________________________________________________________________________
     * doPost is expecting a HTTP parameter userId
     * It sends back a XML serialization of the previous command with HTTP code 200 if a userId is specifyed
     * It sends back a HTTP code 401 error if the userId is not specified or empty
     * It sends back a HTTP code 500 error if a problem occured when accessing to the database
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        response = setConstHeaders(response);
        String idFestival = request.getParameter("idFestival");
        if(idFestival != null){
            try {
                messageJSON = this.fastivalService.getFestival(Integer.parseInt(idFestival));
            } catch (SQLException ex) {
                Logger.getLogger(FestivalServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            throw new NullPointerException("Aucun id de festival de page reçu !");
        }

        response.getWriter().println(messageJSON);
        response.setStatus(HttpServletResponse.SC_OK);
    }


    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idFestivalS = req.getParameter("idFestival");
        Integer idFestival = null;
        if(idFestivalS!=null && !idFestivalS.equals("")){
            idFestival = Integer.parseInt(idFestivalS);
        }
        JSONObject toRet = new JSONObject();

        try {
            fastivalService.deleteFest(idFestival);
            toRet.put("Status","Deleted");
        } catch (SQLException e) {
            e.printStackTrace();
            toRet.put("Status","Not Deleted");
            toRet.put("ERROR", e.getMessage() );
            toRet.put("SQL STATE",e.getSQLState());
        }
        resp.setContentType("application/json");
        resp = setConstHeaders(resp);
        resp.getWriter().println(toRet.toJSONString());
        resp.setStatus(HttpServletResponse.SC_OK);
    }


    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //get idFest
        String idFestivalS = req.getParameter("idFestival");
        Integer idFestival = null;
        if(idFestivalS!=null && !idFestivalS.equals("")){
            idFestival = Integer.parseInt(idFestivalS);
        }
        //get Compte ID
        String idCompteS = req.getParameter("idCompte");
        Integer idCompte = null;
        if(idCompteS!=null && !idCompteS.equals("")){
            idCompte = Integer.parseInt(idCompteS);
        }

        JSONObject toRet = new JSONObject();

        try {
            fastivalService.updateFest(idFestival,idCompte);
            toRet.put("Status","Updated");
        } catch (SQLException e) {
            e.printStackTrace();
            toRet.put("Status","Not Updated");
            toRet.put("ERROR", e.getMessage() );
            toRet.put("SQL STATE",e.getSQLState());
        }
        resp.setContentType("application/json");
        resp = setConstHeaders(resp);
        resp.getWriter().println(toRet.toJSONString());
        resp.setStatus(HttpServletResponse.SC_OK);



    }

    private String processQueryTest(HttpServletRequest request) {
            String res = "{";
            Enumeration<String> P = request.getParameterNames();
            while (P.hasMoreElements()) {
                    String p = P.nextElement();
                    res += "\"" + p + "\": \"" + request.getParameter(p) + "\", ";
            }
            return res + "}";
    }

}

