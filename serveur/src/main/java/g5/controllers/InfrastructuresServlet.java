/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.controllers;

import g5.services.InfrastructureService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Julien Rull
 */
public class InfrastructuresServlet extends HttpServlet {
     private InfrastructureService infrastructureService;

    private HttpServletResponse setConstHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        response.addHeader("Access-Control-Max-Age", "1728000");
        return response;
    }

    public InfrastructuresServlet(){
        this.infrastructureService = new InfrastructureService();
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //we should check the sent params get the festival ID and send back a JSON object with
        //all the festival details and the available days for this festival.
        response = setConstHeaders(response);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        
        Integer page = Integer.parseInt(request.getParameter("numPage"));

        String nom = request.getParameter("name");
        String departement = request.getParameter("departement");
        Integer nbAdultes =  request.getParameter("nbAdultes")==null?0:Integer.parseInt(request.getParameter("nbAdultes"));
        Integer nbEnfants = request.getParameter("nbEnfants")==null?0: Integer.parseInt(request.getParameter("nbEnfants"));
        String sPrixMin = request.getParameter("prixMin");
        Double prixMin = null;
        if(sPrixMin!=null && !sPrixMin.equals("")){
             prixMin = Double.parseDouble(sPrixMin);
        }
        String sPrixMax = request.getParameter("prixMax");
        Double prixMax = null;
        if(sPrixMax!=null && !sPrixMax.equals("")){
          prixMax = Double.parseDouble(sPrixMax);
        }

        String dateDeDebut = request.getParameter("dateDeDebut");
        String dateDeFin = request.getParameter("dateDeFin");
        String typesInfra = request.getParameter("typeInfra");
        String lieu = request.getParameter("lieu");

        try {
            response.getWriter().println(infrastructureService.getInfrastructures(page, nom,
                    departement, nbAdultes, nbEnfants , prixMin, prixMax, dateDeDebut, dateDeFin, typesInfra,lieu));
        } catch (SQLException e) {
            response.getWriter().println("OOPS");
            e.printStackTrace();
        } catch (ParseException ex) {
             Logger.getLogger(InfrastructuresServlet.class.getName()).log(Level.SEVERE, null, ex);
         }

        response.setStatus(HttpServletResponse.SC_OK);

    }   
}
