/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.controllers;

import g5.services.ReservationService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Julien Rull
 */
public class ResaHebergementServlet  extends HttpServlet {

    private ReservationService resaServ;

    public ResaHebergementServlet(){
        this.resaServ = new ReservationService();
    }

    private HttpServletResponse setConstHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        response.addHeader("Access-Control-Max-Age", "1728000");
        return response;
    }   
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            setConstHeaders(resp);
            resp.setContentType("application/json");
            System.out.println("Procedure Started");
            String resaId =req.getParameter("reservationId");
           
            
            Integer nbAdultes =  req.getParameter("nbAdultes")==null?0:Integer.parseInt(req.getParameter("nbAdultes"));
            Integer nbEnfants = req.getParameter("nbEnfants")==null?0: Integer.parseInt(req.getParameter("nbEnfants"));
            String dateDebut =req.getParameter("dateDeDebut");
            String dateFin =req.getParameter("dateDeFin");
            int compteId = 0;
            int hebergementId = 0;
            
            if(req.getParameter("compteId") != null && !req.getParameter("compteId").equalsIgnoreCase("")){
                 compteId =Integer.parseInt(req.getParameter("compteId"));
            }else{
                throw new IOException("L'ID du compte est invalide !");
            }
            
            if(req.getParameter("idHebergement") != null && !req.getParameter("idHebergement").equalsIgnoreCase("")){
                 hebergementId =Integer.parseInt(req.getParameter("idHebergement"));
            }else{
                throw new IOException("L'ID de l'hebergement est invalide !");
            }
            
            if(req.getParameter("dateDeDebut") == null || req.getParameter("dateDeDebut").equalsIgnoreCase("")){
                throw new IOException("Date debut est invalide !");
            }
            
            if(req.getParameter("dateDeFin") == null || req.getParameter("dateDeFin").equalsIgnoreCase("")){
                 throw new IOException("Date fin est invalide !");
            }

        try {
           String responce = resaServ.reserverHebergement(resaId, compteId, hebergementId, nbEnfants, nbAdultes, dateDebut, dateFin);
                //resajour fest added successfully
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
            resp.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
            resp.addHeader("Access-Control-Max-Age", "1728000");

            resp.setContentType("application/json");

                resp.getWriter().println(responce);
                resp.setStatus(HttpServletResponse.SC_OK);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
}
