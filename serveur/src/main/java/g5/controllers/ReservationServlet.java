package g5.controllers;

import g5.services.ReservationService;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class ReservationServlet extends HttpServlet {

    private ReservationService resaServ;


    public ReservationServlet(){
        this.resaServ = new ReservationService();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer resaId =Integer.parseInt(req.getParameter("reservationID"));


        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        resp.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        resp.addHeader("Access-Control-Max-Age", "1728000");

        resp.setContentType("application/json");

        resp.getWriter().println(resaServ.getReservationInfo(resaId).toJSONString());
        resp.setStatus(HttpServletResponse.SC_OK);


    }


    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //get resaId
        Integer resaId =Integer.parseInt(req.getParameter("reservationID"));
        JSONObject toRet = new JSONObject();
        try {
            resaServ.deleteReservation(resaId);
            toRet.put("Status","Reservation deleted");
        } catch (SQLException e) {
            e.printStackTrace();
            toRet.put("Status","Reservation NOT deleted");
            toRet.put("ERROR",e.getErrorCode());
            toRet.put("Message",e.getMessage());
        }


        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        resp.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        resp.addHeader("Access-Control-Max-Age", "1728000");
        resp.setContentType("application/json");
        resp.getWriter().println(toRet.toJSONString());
        resp.setStatus(HttpServletResponse.SC_OK);
    }
}
