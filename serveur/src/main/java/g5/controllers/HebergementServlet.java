/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.controllers;

import g5.services.HebergementService;
import g5.services.InfrastructureService;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Julien Rull
 */
public class HebergementServlet extends HttpServlet{
    
    
    
     private HebergementService hebergementService;
     
     public HebergementServlet(){
         this.hebergementService = new HebergementService();
     }

    private HttpServletResponse setConstHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        response.addHeader("Access-Control-Max-Age", "1728000");
        return response;
    }
    
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //we should check the sent params get the festival ID and send back a JSON object with
        //all the festival details and the available days for this festival.
        response = setConstHeaders(response);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        
        
        Integer nbAdultes =  request.getParameter("nbAdultes")==null?0:Integer.parseInt(request.getParameter("nbAdultes"));
        Integer nbEnfants = request.getParameter("nbEnfants")==null?0: Integer.parseInt(request.getParameter("nbEnfants"));
        Integer idInfra = request.getParameter("idInfra")==null?1: Integer.parseInt(request.getParameter("idInfra"));
        
        try {
            response.getWriter().println(hebergementService.getHebergements(idInfra, nbEnfants, nbAdultes));
        } catch (SQLException e) {
            response.getWriter().println("OOPS");
            e.printStackTrace();
        }

        response.setStatus(HttpServletResponse.SC_OK);

    }  
}
