/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.controllers;

import g5.services.FestivalService;
import g5.services.ReservationService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Julien Rull
 */
public class GetReservationServlet extends HttpServlet {
    
    public static String messageJSON = "";
    private ReservationService reservationService;

    private HttpServletResponse setConstHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        response.addHeader("Access-Control-Max-Age", "1728000");
        return response;
    }


    public GetReservationServlet(){
        this.reservationService = new ReservationService();
    }
     
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            response.setContentType("application/json");
            response = setConstHeaders(response);
            if (messageJSON != null) {
            response.getWriter().println(messageJSON);
            response.setStatus(HttpServletResponse.SC_OK);

        } else {
            System.out.println("La réponse GET est null !");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
            
    }



    /*____________________________________________________________________________________________________________________
     * doPost is expecting a HTTP parameter userId
     * It sends back a XML serialization of the previous command with HTTP code 200 if a userId is specifyed
     * It sends back a HTTP code 401 error if the userId is not specified or empty
     * It sends back a HTTP code 500 error if a problem occured when accessing to the database
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        response = setConstHeaders(response);
        String idFestival = request.getParameter("idFestival");
        if(idFestival != null){

        }else{
            throw new NullPointerException("Aucun id de festival de page reçu !");
        }

        response.getWriter().println(messageJSON);
        response.setStatus(HttpServletResponse.SC_OK);
    }
    
        private String processQueryTest(HttpServletRequest request) {
            String res = "{";
            Enumeration<String> P = request.getParameterNames();
            while (P.hasMoreElements()) {
                    String p = P.nextElement();
                    res += "\"" + p + "\": \"" + request.getParameter(p) + "\", ";
            }
            return res + "}";
    }

}

