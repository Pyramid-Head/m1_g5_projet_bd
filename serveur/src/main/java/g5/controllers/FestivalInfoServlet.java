package g5.controllers;

import g5.services.FestivalService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class FestivalInfoServlet extends HttpServlet {

    private FestivalService fastivalService;

    private HttpServletResponse setConstHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        response.addHeader("Access-Control-Max-Age", "1728000");
        return response;
    }

    public FestivalInfoServlet(){
        this.fastivalService = new FestivalService();
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //we should check the sent params get the festival ID and send back a JSON object with
        //all the festival details and the available days for this festival.
        response = setConstHeaders(response);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        Integer festId = Integer.parseInt(request.getParameter("idFestival"));
        Integer nbAdultes =  request.getParameter("nbAdultes")==null?0:Integer.parseInt(request.getParameter("nbAdultes"));
        Integer nbEnfants = request.getParameter("nbEnfants")==null?0: Integer.parseInt(request.getParameter("nbEnfants"));

        try {
            response.getWriter().println(fastivalService.getFestivalInfo(festId,nbAdultes+nbEnfants));
        } catch (SQLException e) {
            response.getWriter().println("OOPS");
            e.printStackTrace();
        }

        response.setStatus(HttpServletResponse.SC_OK);

    }

}
