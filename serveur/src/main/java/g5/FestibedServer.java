package g5;

import g5.backgroundThreads.ThreadTimer;
import g5.controllers.*;
import g5.dbAccess.ConnectToDB;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import javax.servlet.http.HttpServlet;


public class FestibedServer extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Server server;
    //public static GestionnaireClient gestClient;
    //public static ArrayList<GestionnaireCommande> gestCommande;
    //public static ArrayList<GestionnaireMenu> gestMenu;
    
    void start() throws Exception {
        int maxThreads = 100;
        int minThreads = 10;
        int idleTimeout = 120;

        QueuedThreadPool threadPool = new QueuedThreadPool(maxThreads, minThreads, idleTimeout);

        server = new Server(threadPool);
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8090);
        server.setConnectors(new Connector[] { connector });

        ServletHandler servletHandler = new ServletHandler();
        server.setHandler(servletHandler);

        servletHandler.addServletWithMapping(FestivalsServlet.class, "/api/festivals");
        servletHandler.addServletWithMapping(FestivalServlet.class, "/api/detailsFestival");
        servletHandler.addServletWithMapping(FestivalServlet.class, "/api/ajouterFestivalPanier");
        servletHandler.addServletWithMapping(FestivalInfoServlet.class, "/api/festivalInfo");
        servletHandler.addServletWithMapping(ResaJourFestivalServlet.class, "/api/addFestPanier");
        servletHandler.addServletWithMapping(InfrastructuresServlet.class, "/api/infrastructures");
        servletHandler.addServletWithMapping(HebergementServlet.class, "/api/hebergements");

        servletHandler.addServletWithMapping(CompteServlet.class, "/api/authentification");
        servletHandler.addServletWithMapping(ResaHebergementServlet.class, "/api/addHebPanier");
        servletHandler.addServletWithMapping(ReservationServlet.class, "/api/reservation");


        //servletHandler.addServletWithMapping(ClientAuthentificationServlet.class, "/api/authentification");
       // servletHandler.addServletWithMapping(RequestServlet.class, "/api/requete");
       // servletHandler.addServletWithMapping(HistoriqueCommandeServlet.class, "/api/topCommande");
        
        server.start();
    }

    void stop() throws Exception {
        System.out.println("Server stop");
    	server.stop();
    }

    public static void main(String[] args) throws Exception {
        FestibedServer server = new FestibedServer();

       //   TO ACTIVATE BEFORE RENDUE!!!!!
        ThreadTimer deleterOfRequests = new ThreadTimer();
        Thread dltThread = new Thread(deleterOfRequests);
        dltThread.start();
        dltThread.isDaemon();


        server.start();
        
        //Connexion à la BDD (on se connecte juste une fois ici car la connexion peu prendre un certain temps)
        ConnectToDB.getConnection();

        System.out.println("Le client peut désormais être lancé.");
    }

}
