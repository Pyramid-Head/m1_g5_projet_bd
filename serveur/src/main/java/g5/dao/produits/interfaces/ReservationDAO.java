/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;


import g5.models.Reservation;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface ReservationDAO {
    public Reservation getReservationById(int idReservation) throws SQLException;
    public List<Reservation> getReservationParUtilisateur(int idCompte) throws SQLException;
    public Integer create(Reservation reservation);
    public boolean delete(Integer idReserv) throws SQLException;
    public boolean update(Reservation reservation);
}
