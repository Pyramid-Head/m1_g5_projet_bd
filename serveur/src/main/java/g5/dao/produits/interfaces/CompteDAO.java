/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.Compte;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface CompteDAO {
    public Compte getCompteById(int idCompte);
    public Compte getCompteByFbKey(String fbKey);
    public Integer getCompteIDByMail(String mail) throws SQLException;

    public List<Compte> getHebergeurs();
    public List<Compte> getOrganizateurs();
    public List<Compte> getUtilisateurs();
    public List<Compte> getComptes();

    public boolean create(Compte compte);
    public boolean delete(Compte compte);
    public boolean update(Compte compte);

}
