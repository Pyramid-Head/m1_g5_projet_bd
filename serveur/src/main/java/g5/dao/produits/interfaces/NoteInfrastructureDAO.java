/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.NoteInfrastructure;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface NoteInfrastructureDAO {
    public boolean create(NoteInfrastructure noteInfrastructure);
    public NoteInfrastructure getById(long id);
    public List<NoteInfrastructure> getAll();
    public boolean delete(NoteInfrastructure noteInfrastructure);
    public boolean update(NoteInfrastructure noteInfrastructure);
}
