/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.Infrastructure;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface InfrastructureDAO {

    public Infrastructure getById(long id) throws SQLException;
    public List<Infrastructure> getAll(int offset, int limit) throws SQLException;
    public List<Infrastructure> getInfraByName(String infraName , int nbrpAdulte , int nbrpEnfant,int offset, int limit) throws SQLException;
    public List<Infrastructure> getInfraByDept(String deptNumb , int nbrpAdulte , int nbrpEnfant,int offset, int limit) throws SQLException;
    public List<Infrastructure> getInfraByDate(Date dateDebut , Date dateFin , int nbrpAdulte , int nbrpEnfant,int offset, int limit);
    public List<Infrastructure> getInfraByType(String type , int nbrpAdulte , int nbrpEnfant,int offset, int limit) throws SQLException;
    public List<Infrastructure> getInfraByPrice(double prixMin, double prixMax , int nbrpAdulte , int nbrpEnfant,int offset, int limit);
    public List<Infrastructure> getInfraBynbPersonnes(int nbrpAdulte , int nbrpEnfant,int offset, int limit) throws SQLException;


    public List<Infrastructure> getByAnyParam(String nom , String numDept, String typeInfra ,
                                              Date dateDebut, Date dateFin , Double prixMin, Double PrixMax,
                                              Integer nbrPlaceAdult, Integer nbrPlacEnfant,String lieu,int offset, int limit
                                                ) throws SQLException;

    public boolean create(Infrastructure infrastructure);
    public boolean delete(Infrastructure infrastructure);
    public boolean update(Infrastructure infrastructure);
}
