/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.DispoHebergement;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface DispoHebergementDAO {
    public boolean create(DispoHebergement dispoHebergement);
    public DispoHebergement getById(long hebergId, Date calDate) throws SQLException;
    public List<DispoHebergement> getDispoParDate(Date dateDebut,Date dateFin,int offset, int limit) throws SQLException;
    public List<DispoHebergement> getDateParHeberg(long hebergId,int offset, int limit) throws SQLException;
    public List<DispoHebergement> getAll(int offset, int limit) throws SQLException;
    public boolean delete(DispoHebergement dispoHebergement);
    //public boolean update(DispoHebergement dispoHebergement);  we  Restrict That!
}
