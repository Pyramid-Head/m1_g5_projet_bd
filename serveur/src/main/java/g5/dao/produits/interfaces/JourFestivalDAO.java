/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.JourFestival;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface JourFestivalDAO {
    public JourFestival getJourFestivalById(int idJourFest) throws SQLException;
    public List<JourFestival> getJoursDeFestival(int festivalId , int nbrPersonnes) throws SQLException;
    public List<JourFestival> getJoursDeFestivalPremiereClasse(int festivalId ,int nbrPersonnes) throws SQLException; //type c1
    public List<JourFestival> getJoursDeFestivalDeuxiemmeClasse(int festivalId,int nbrPersonnes) throws SQLException; //type c2
    public List<JourFestival> getJoursDeFestivalSansClasse(int festivalId,int nbrPersonnes) throws SQLException; // type sanstype
    public boolean deleteJourDuFestival(int festivalID);
        public boolean create(JourFestival jourFest);
    public boolean delete(JourFestival jourFest);
    public boolean update(JourFestival jourFest);

}
