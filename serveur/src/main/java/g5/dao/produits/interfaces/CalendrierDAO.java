/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.Calendrier;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface CalendrierDAO {
    //public boolean create(Calendrier calendrier);   RESTRICTED!!!
    public Calendrier getById(Date idCal) throws SQLException;
    public List<Calendrier> getAll() throws SQLException;
    public List<Calendrier> getDateFromTo(Date start, Date end) throws SQLException;
    //public boolean delete(Calendrier calendrier);    RESTRICTED!!!
   // public boolean update(Calendrier calendrier);     RESTRICTED!!!
}
