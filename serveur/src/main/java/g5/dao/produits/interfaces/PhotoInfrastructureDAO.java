/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;
import g5.models.PhotoInfrastructure;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface PhotoInfrastructureDAO {
    public boolean create(PhotoInfrastructure photoInfrastructure);
    public PhotoInfrastructure getById(long id);
    public List<PhotoInfrastructure> getAll();
    public boolean delete(PhotoInfrastructure photoInfrastructure);
    public boolean update(PhotoInfrastructure photoInfrastructure);
}
