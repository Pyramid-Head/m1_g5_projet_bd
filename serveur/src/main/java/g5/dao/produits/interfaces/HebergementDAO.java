/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.Hebergement;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface HebergementDAO {
    public boolean create(Hebergement hebergement);
    public Hebergement getById(long id) throws SQLException;
    public List<Hebergement> getAll(int nbrEnfant, int nbrAdulte,int offset, int limit) throws SQLException;
    public List<Hebergement> getByInfrastructureId(int infraId,int nbrEnfant, int nbrAdulte) throws SQLException;
    public List<Hebergement> getByInfrastructureName(String infraName,int nbrEnfant, int nbrAdulte, int offset, int limit) throws SQLException;
    public List<Hebergement> getByDepartment(String dept,int nbrEnfant, int nbrAdulte, int offset, int limit) throws SQLException;
    public List<Hebergement> getByType(String type,int nbrEnfant, int nbrAdulte, int offset, int limit) throws SQLException;
    public boolean delete(Hebergement hebergement);
    public boolean update(Hebergement hebergement);
}
