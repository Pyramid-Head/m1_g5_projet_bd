/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.ResaHebergement;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface ResaHebergementDAO {
    public boolean create(ResaHebergement resaHebergement) throws SQLException;
    public ResaHebergement getById(long id);
    public List<ResaHebergement> getAll();
    public List<ResaHebergement> getResaHebPanier();
    public boolean delete(ResaHebergement resaHebergement);
    public boolean update(ResaHebergement resaHebergement);
    public List<ResaHebergement> getByResaId(Integer resaId)  throws SQLException;
}
