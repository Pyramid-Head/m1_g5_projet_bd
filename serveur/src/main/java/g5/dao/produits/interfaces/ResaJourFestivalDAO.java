/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;


import g5.models.ResaJourFestival;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface ResaJourFestivalDAO {

    public ResaJourFestival getResaJourFestivalById(int idReservation, int idJourFest) throws SQLException;
    public List<ResaJourFestival> getResaJourFestParReservation(int idReservation) throws SQLException;
    public List<ResaJourFestival> getResaJourFestParJourFest(int idJourFest) throws SQLException;

    public boolean create(ResaJourFestival resaJourFestival) throws SQLException;
    public boolean delete(ResaJourFestival resaJourFestival);
    public boolean update(ResaJourFestival resaJourFestival);
}
