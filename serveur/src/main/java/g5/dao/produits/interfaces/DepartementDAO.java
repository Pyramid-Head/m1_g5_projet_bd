/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.Departement;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface DepartementDAO {

    public Departement getDepartementParNumero(String numeroDept) throws SQLException;
    public Departement getDepartementParNom(String nomDept) throws SQLException;

    //public boolean create(Departement dept);      RESTRICTED
    //public boolean delete(Departement dept);      RESTRICTED
    //public boolean update(Departement dept);      RESTRICTED

    
}
