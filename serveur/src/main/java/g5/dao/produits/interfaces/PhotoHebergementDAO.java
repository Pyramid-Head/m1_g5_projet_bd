/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.PhotoHebergement;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface PhotoHebergementDAO {
        public boolean create(PhotoHebergement photoHebergement);
    public PhotoHebergement getById(long id);
    public List<PhotoHebergement> getAll();
    public boolean delete(PhotoHebergement photoHebergement);
    public boolean update(PhotoHebergement photoHebergement);
}
