/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;


import g5.models.Festival;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface FestivalDAO {
    /*public boolean create(Infrastructure infrastructure);
    public Infrastructure getById(long id);
    public List<Infrastructure> getAll();
    public boolean delete(Infrastructure infrastructure);
    public boolean update(Infrastructure infrastructure);*/
    public Festival getFestivalById(int idFestival) throws SQLException;
    public List<Festival> getFestivalByName(String festName,int nbrPersonne, int offset, int limit) throws SQLException;
    public List<Festival> getFestivalBetween(Date startingDate, Date endDate,int nbrPersonne, int offset, int limit) throws SQLException;
    public List<Festival> getFestivalByType(String typeFest,int nbrPersonne, int offset, int limit) throws SQLException;
    public List<Festival> getFestivalByDept(String numDepartement,int nbrPersonne, int offset, int limit) throws SQLException;
    public List<Festival> getFestivalsByAnyParam(String nom,String lieu,Date dateDebut, Date dateFin,
                                                 Double prixMin, Double prixMax,
                                                 String typeFest,int nbrPersonne, int offset, int limit) throws SQLException;
    public boolean create(Festival festival);
    public boolean delete(Integer idFest) throws SQLException;
    public boolean update(int idFest,int idCompte) throws SQLException;
    public List<Festival> getFestivalsByHeberg(Long idHeberg,Date dateDebut,Date dateFin, Integer nbrPersonne) throws SQLException;


}
