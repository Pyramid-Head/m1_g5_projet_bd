/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.interfaces;

import g5.models.NoteFestival;

import java.util.List;

/**
 *
 * @author Julien Rull
 */
public interface NoteFestivalDAO {
    public NoteFestival getNoteFestById(int noteFestId);
    public List<NoteFestival> getNotesDeFestival(int idFestival);
    public List<NoteFestival> getNotesParClient(int idCompte);

    public boolean create(NoteFestival noteFestival);
    public boolean delete(NoteFestival noteFestival);
    public boolean update(NoteFestival noteFestival);




}
