/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.InfrastructureDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.Infrastructure;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class OracleInfrastructureDAO implements InfrastructureDAO {

    Connection conn = null;
    public OracleInfrastructureDAO(){
        conn = ConnectToDB.getConnection();
    }

    private Infrastructure generateOneInfra(ResultSet rs) throws SQLException {
        int IDINFRASTRUCTURE;
        String NUMDEPARTEMENT;
        int IDCOMPTE;
        String NOM;
        String CLASSEMENT;
        String  CATEGORIE;
        String TELEPHONE;
        String SITEINTERNET;
        String COURRIEL;
        String COORDONNEESGPS;
        int NOTE;
        Double MONTANTENCAISSE;
        String TYPEINFRA;
        rs.next();

        IDINFRASTRUCTURE =rs.getInt("IDINFRASTRUCTURE");
                NUMDEPARTEMENT = rs.getString("NUMDEPARTEMENT");
        IDCOMPTE = rs.getInt("IDCOMPTE");
                NOM = rs.getString("NOM");
        CLASSEMENT = rs.getString("CLASSEMENT");
                CATEGORIE = rs.getString("CATEGORIE");
        TELEPHONE = rs.getString("TELEPHONE");
                SITEINTERNET = rs.getString("SITEINTERNET");
        COURRIEL = rs.getString("COURRIEL");
                COORDONNEESGPS = rs.getString("COORDONNEESGPS");
        NOTE = rs.getInt("NOTE");
                MONTANTENCAISSE = rs.getDouble("MONTANTENCAISSE");
        TYPEINFRA = rs.getString("TYPEINFRA");

        return new Infrastructure(IDINFRASTRUCTURE,IDCOMPTE,NUMDEPARTEMENT,NOM,CLASSEMENT,CATEGORIE,TELEPHONE,
                SITEINTERNET,COURRIEL,COORDONNEESGPS,NOTE,MONTANTENCAISSE,TYPEINFRA);

    }

    private List<Infrastructure> generateInfras(ResultSet rs) throws SQLException {
        int IDINFRASTRUCTURE;
        String NUMDEPARTEMENT;
        int IDCOMPTE;
        String NOM;
        String CLASSEMENT;
        String  CATEGORIE;
        String TELEPHONE;
        String SITEINTERNET;
        String COURRIEL;
        String COORDONNEESGPS;
        int NOTE;
        Double MONTANTENCAISSE;
        String TYPEINFRA;
        ArrayList<Infrastructure> toRet = new ArrayList<Infrastructure>();

        while (rs.next()){
            IDINFRASTRUCTURE =rs.getInt("IDINFRASTRUCTURE");
            NUMDEPARTEMENT = rs.getString("NUMDEPARTEMENT");
            IDCOMPTE = rs.getInt("IDCOMPTE");
            NOM = rs.getString("NOM");
            CLASSEMENT = rs.getString("CLASSEMENT");
            CATEGORIE = rs.getString("CATEGORIE");
            TELEPHONE = rs.getString("TELEPHONE");
            SITEINTERNET = rs.getString("SITEINTERNET");
            COURRIEL = rs.getString("COURRIEL");
            COORDONNEESGPS = rs.getString("COORDONNEESGPS");
            NOTE = rs.getInt("NOTE");
            MONTANTENCAISSE = rs.getDouble("MONTANTENCAISSE");
            TYPEINFRA = rs.getString("TYPEINFRA");

            toRet.add(new Infrastructure(IDINFRASTRUCTURE,IDCOMPTE,NUMDEPARTEMENT,NOM,CLASSEMENT,CATEGORIE,TELEPHONE,
                    SITEINTERNET,COURRIEL,COORDONNEESGPS,NOTE,MONTANTENCAISSE,TYPEINFRA));
        }
        return toRet;
    }



        @Override
    public Infrastructure getById(long id) throws SQLException {
            Statement stmt = conn.createStatement();
            String query = "SELECT * FROM INFRASTRUCTURE WHERE IDINFRASTRUCTURE = " + id;
            Infrastructure IToRet = generateOneInfra(stmt.executeQuery(query));
            stmt.close();
            return IToRet;
    }

    @Override
    public List<Infrastructure> getAll(int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM (SELECT i.*, ROW_NUMBER() over (order by i.IDINFRASTRUCTURE) R  FROM INFRASTRUCTURE i) "
                + " WHERE R BETWEEN "+ offset +" AND " + limit;
        ArrayList<Infrastructure> IToRet = (ArrayList<Infrastructure>) generateInfras(stmt.executeQuery(query));
        stmt.close();
        return IToRet;
    }


    @Override
    public List<Infrastructure> getInfraByName(String infraName, int nbrpAdulte, int nbrpEnfant, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM (SELECT i.*, ROW_NUMBER() over (order by i.IDINFRASTRUCTURE) R " +
                "FROM INFRASTRUCTURE i, HEBERGEMENT h WHERE i.NOM LIKE '%"+infraName+"%' " +
                "AND h.IDINFRASTRUCTURE = i.IDINFRASTRUCTURE AND h.NBPLACESADULTES > " + nbrpAdulte +
                " AND h.NBPLACESENFANTS > "+nbrpEnfant + ")  WHERE R BETWEEN "+offset+" AND " + limit;
        ArrayList<Infrastructure> IToRet = (ArrayList<Infrastructure>) generateInfras(stmt.executeQuery(query));
        stmt.close();
        return IToRet;
    }

    @Override
    public List<Infrastructure> getInfraByDept(String deptNumb, int nbrpAdulte, int nbrpEnfant, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM (SELECT i.*, ROW_NUMBER() over (order by i.IDINFRASTRUCTURE) R " +
                "FROM INFRASTRUCTURE i, HEBERGEMENT h WHERE i.NUMDEPARTEMENT = '"+deptNumb+"' " +
                "AND h.IDINFRASTRUCTURE = i.IDINFRASTRUCTURE AND h.NBPLACESADULTES > " + nbrpAdulte +
                " AND h.NBPLACESENFANTS > "+nbrpEnfant + ")  WHERE R BETWEEN "+offset+" AND " + limit;
        ArrayList<Infrastructure> IToRet = (ArrayList<Infrastructure>) generateInfras(stmt.executeQuery(query));
        stmt.close();
        return IToRet;
    }



    @Override
    public List<Infrastructure> getInfraByType(String type, int nbrpAdulte, int nbrpEnfant, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM (SELECT i.*, ROW_NUMBER() over (order by i.IDINFRASTRUCTURE) R " +
                "FROM INFRASTRUCTURE i, HEBERGEMENT h WHERE i.TYPEINFRA LIKE '%"+type+"%' " +
                "AND h.IDINFRASTRUCTURE = i.IDINFRASTRUCTURE AND h.NBPLACESADULTES > " + nbrpAdulte +
                " AND h.NBPLACESENFANTS > "+nbrpEnfant + ")  WHERE R BETWEEN "+offset+" AND " + limit;
        ArrayList<Infrastructure> IToRet = (ArrayList<Infrastructure>) generateInfras(stmt.executeQuery(query));
        stmt.close();
        return IToRet;
    }


    @Override
    public List<Infrastructure> getInfraByDate(Date dateDebut, Date dateFin, int nbrpAdulte, int nbrpEnfant, int offset, int limit) {
        return null;
    }
    @Override
    public List<Infrastructure> getInfraByPrice(double prixMin, double prixMax, int nbrpAdulte, int nbrpEnfant, int offset, int limit) {
        return null;
    }

    @Override
    public List<Infrastructure> getInfraBynbPersonnes(int nbrpAdulte, int nbrpEnfant, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM (SELECT i.*, ROW_NUMBER() over (order by i.IDINFRASTRUCTURE) R " +
                "FROM INFRASTRUCTURE i, HEBERGEMENT h WHERE " +
                "h.IDINFRASTRUCTURE = i.IDINFRASTRUCTURE AND h.NBPLACESADULTES > " + nbrpAdulte +
                " AND h.NBPLACESENFANTS > "+nbrpEnfant + ")  WHERE R BETWEEN "+offset+" AND " + limit;
        ArrayList<Infrastructure> IToRet = (ArrayList<Infrastructure>) generateInfras(stmt.executeQuery(query));
        stmt.close();
        return IToRet;
    }



    @Override
    public boolean create(Infrastructure infrastructure) {
        try {
            Statement stmt = conn.createStatement();
            String requeststr = "INSERT INTO INFRASTRUCTURE (NUMDEPARTEMENT,IDCOMPTE,NOM,CLASSEMENT,CATEGORIE,"
                    + "TELEPHONE,SITEINTERNET,COURRIEL,COORDONNEESGPS,NOTE,MONTANTENCAISSE,TYPEINFRA) "
                    + "VALUES ('" + infrastructure.getNumDepartement() +"' , "+infrastructure.getIdCompte()
                    + ", '"+infrastructure.getNom()+"', '"+infrastructure.getClassement()+"','"+infrastructure.getCategorie()
                    +"','"+infrastructure.getTelephone()+"','" + infrastructure.getSiteInternet()
                    + "','"+infrastructure.getCourriel()+"','"+infrastructure.getCoordonneesGps()
                    +"',"+infrastructure.getNote()+","+infrastructure.getMontantEnCaisse()
                    +",'"+infrastructure.getTypeInfra()+"')";
            stmt.executeQuery(requeststr);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Infrastructure infrastructure) {
        try {
            Statement stmt = conn.createStatement();
            String requeststr = "DELETE FROM INFRASTRUCTURE WHERE IDINFRASTRUCTURE = "
                    +infrastructure.getIdInfrastructure();
            stmt.executeQuery(requeststr);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(Infrastructure infrastructure) {
        try {
            Statement stmt = conn.createStatement();
            String requeststr = "UPDATE INFRASTRUCTURE SET "+
                    "NUMDEPARTEMENT = '"+infrastructure.getNumDepartement()+"', "+
                    "IDCOMPTE = " +infrastructure.getIdCompte()+", "+
                    "NOM = '"+infrastructure.getNom()+"', "+
                    "CLASSEMENT = '"+infrastructure.getClassement()+"', "+
                    "CATEGORIE = '" + infrastructure.getCategorie()+"', "+
                    "TELEPHONE = '" + infrastructure.getTelephone() +"', "+
                    "SITEINTERNET = '"+infrastructure.getSiteInternet()+"', "+
                    "COURRIEL = '"+infrastructure.getCourriel()+"', "+
                    "COORDONNEESGPS = '"+infrastructure.getCoordonneesGps()+"', "+
                    "NOTE = " + infrastructure.getNote()+", "+
                    "MONTANTENCAISSE = "+infrastructure.getMontantEnCaisse()+", "+
                    "TYPEINFRA = '" +infrastructure.getTypeInfra()+"' WHERE IDINFRASTRUCTURE = "
                    +infrastructure.getIdInfrastructure();
            stmt.executeQuery(requeststr);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }




    @Override
    public List<Infrastructure> getByAnyParam(String nom, String numDept, String typeInfra, Date dateDebut,
                                              Date dateFin, Double prixMin, Double prixMax, Integer nbrPlaceAdult,
                                              Integer nbrPlacEnfant, String lieu, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = genericQueryGenerator(nom,numDept,typeInfra,dateDebut,
                dateFin,prixMin,prixMax,nbrPlaceAdult,nbrPlacEnfant,offset,limit,lieu);
        System.out.println(query);
        ArrayList<Infrastructure> IToRet = (ArrayList<Infrastructure>) generateLimitedInfras(stmt.executeQuery(query));
        stmt.close();
        return IToRet;

    }



    private List<Infrastructure> generateLimitedInfras(ResultSet rs) throws SQLException {
        int IDINFRASTRUCTURE;
        String NUMDEPARTEMENT;
        String NOM;
        String CLASSEMENT;
        String  CATEGORIE;
        String TELEPHONE;
        String SITEINTERNET;
        String COURRIEL;
        String COORDONNEESGPS;
        int NOTE;
        String TYPEINFRA;
        Double minPrix;


        ArrayList<Infrastructure> toRet = new ArrayList<Infrastructure>();

        while (rs.next()){
            IDINFRASTRUCTURE =rs.getInt("IDINFRASTRUCTURE");
            NUMDEPARTEMENT = rs.getString("NUMDEPARTEMENT");
            NOM = rs.getString("NOM");
            CLASSEMENT = rs.getString("CLASSEMENT");
            CATEGORIE = rs.getString("CATEGORIE");
            TELEPHONE = rs.getString("TELEPHONE");
            SITEINTERNET = rs.getString("SITEINTERNET");
            COURRIEL = rs.getString("COURRIEL");
            COORDONNEESGPS = rs.getString("COORDONNEESGPS");
            NOTE = rs.getInt("NOTE");
            TYPEINFRA = rs.getString("TYPEINFRA");

            //getting min price
            Double prixEnfant = rs.getDouble("TARIFENFANTMIN");
            Double prixAdult = rs.getDouble("TARIFADULTEMIN");
            Double prixForft = rs.getDouble("TARIFFORFAITAIREMIN");
            //checking if present
            if(prixForft != null){
                minPrix = prixForft;
            }else if(prixEnfant == null) {
               minPrix = prixAdult;
            }else if(prixAdult == null){
                minPrix = prixEnfant;
            }
            else {
                minPrix = prixEnfant<prixAdult?prixEnfant:prixAdult;
            }


            toRet.add(new Infrastructure(IDINFRASTRUCTURE,NUMDEPARTEMENT,NOM,CLASSEMENT,CATEGORIE,TELEPHONE,
                    SITEINTERNET,COURRIEL,COORDONNEESGPS,NOTE,TYPEINFRA,minPrix));
        }
        return toRet;
    }

    private String genericQueryGenerator(String nom , String numDept, String typeInfra ,
                              Date dateDebut, Date dateFin , Double prixMin, Double PrixMax,
                              Integer nbrPlaceAdult, Integer nbrPlacEnfant,int offset, int limit, String lieu ) {
        String sql = "SELECT IDINFRASTRUCTURE,TARIFENFANTMIN,TARIFFORFAITAIREMIN,TARIFADULTEMIN,NUMDEPARTEMENT "
                +",NOM,CLASSEMENT,CATEGORIE,TELEPHONE,SITEINTERNET,COURRIEL,COORDONNEESGPS,NOTE,TYPEINFRA"
                + " FROM ( "+
                "SELECT O.IDINFRASTRUCTURE , ROW_NUMBER() over (order by O.IDINFRASTRUCTURE) R , "+
                "	MIN(O.TARIFENFANT) AS TARIFENFANTMIN , MIN(O.TARIFFORFAITAIRE) AS TARIFFORFAITAIREMIN, "+
                "MIN(O.TARIFADULTE) AS TARIFADULTEMIN "
                +", O.NUMDEPARTEMENT,O.NOM,O.CLASSEMENT,O.CATEGORIE,O.TELEPHONE,O.SITEINTERNET,O.COURRIEL,O.COORDONNEESGPS,O.NOTE,O.TYPEINFRA "
                +"FROM ( "+
                "SELECT i.IDINFRASTRUCTURE,i.NUMDEPARTEMENT,i.NOM,i.CLASSEMENT,i.CATEGORIE,i.TELEPHONE,i.SITEINTERNET, "+
                "i.COURRIEL,i.COORDONNEESGPS,i.NOTE,i.TYPEINFRA,h.TARIFADULTE,h.TARIFENFANT,h.TARIFFORFAITAIRE "+
                "FROM INFRASTRUCTURE i, HEBERGEMENT h,DISPOHEBERGEMENT d "
                ;

        sql+="WHERE i.NOM LIKE '%";
        sql += nom==null?"":nom;
        sql+="%' ";
        Integer capacitee = null;
        Integer placeAd = nbrPlaceAdult;
        Integer placeEn = nbrPlacEnfant;
        if(lieu !=null && !lieu.equals("")){
            sql+=" AND i.NUMDEPARTEMENT = '"+lieu+"'";
        }
        if(numDept != null && !numDept.equals("")) {
            sql+=" AND i.NUMDEPARTEMENT = '"+numDept+"' ";
        }

        if(typeInfra != null && !typeInfra.equals("")) {
            String[] arrDeType = typeInfra.split(";");

                sql+=" AND " ;
            if (arrDeType.length>1){sql+=" ( ";}
            sql+= " i.TYPEINFRA = '"+arrDeType[0]+"' ";
            if (arrDeType[0].equals("CAMPING")|| arrDeType[0].equals("RESIDENCE DE TOURISME")
                    || arrDeType[0].equals("PARC RESIDENTIEL DE LOISIRS")){
                capacitee=null;
                placeAd = null;
                placeEn = null;
            }
                for (int i = 1 ; i<arrDeType.length;i++){
                    if (arrDeType[i].equals("CAMPING")|| arrDeType[i].equals("RESIDENCE DE TOURISME")
                            || arrDeType[i].equals("PARC RESIDENTIEL DE LOISIRS")){
                        capacitee=null;
                        placeAd = null;
                        placeEn = null;
                    }
                    sql+=" OR i.TYPEINFRA = '"+arrDeType[i]+"' ";
            }

            if (arrDeType.length>1){sql+=" ) ";}


        }
        if(placeAd != null) {
            sql+= " AND h.NBPLACESADULTES >= " +nbrPlaceAdult;
        }
        if(placeEn != null && placeEn != 0) {
            sql+=" AND h.NBPLACESENFANTS >= " +nbrPlacEnfant;
        }
        if(capacitee != null ) {
            sql+=" AND h.CAPACITE >= " +capacitee;
        }
        //now work with the dates!
        if(dateDebut !=null && dateFin != null) {
            //get all the dates between them and add them in
            ArrayList<Date> dtes = (ArrayList<Date>) UsedFunctions.getDatesBetween(dateDebut, dateFin);
            for (int i = 0; i < dtes.size(); i++) {
                sql += " AND (SELECT COUNT(*) FROM DISPOHEBERGEMENT ";
                sql+= "WHERE DATECAL = " + UsedFunctions.formatDate(dtes.get(i));
                sql += " AND IDHEBERGEMENT = h.IDHEBERGEMENT) >=1 ";
            }

        }

        //joining
        sql+= "AND i.IDINFRASTRUCTURE=h.IDINFRASTRUCTURE "+
                "AND h.IDHEBERGEMENT=d.IDHEBERGEMENT) ";
        sql+="O GROUP BY O.IDINFRASTRUCTURE "
                + ",O.NUMDEPARTEMENT ,O.NOM,O.CLASSEMENT,O.CATEGORIE,O.TELEPHONE,O.SITEINTERNET,O.COURRIEL,O.COORDONNEESGPS,O.NOTE,O.TYPEINFRA ";

        //now work with price if present
        if(prixMin != null && PrixMax!=null && PrixMax>=prixMin) {
            sql += "HAVING MIN(O.TARIFADULTE) BETWEEN "+prixMin+" AND "+PrixMax
                    + " OR MIN(O.TARIFENFANT) BETWEEN "+prixMin+" AND "+PrixMax
                    + " OR MIN(O.TARIFFORFAITAIRE) BETWEEN "+prixMin+" AND "+PrixMax;
        }

        //finish the query
        sql+= ")WHERE R BETWEEN " +offset+ " AND "+limit;

        System.out.println("TEST");

        System.out.println(sql);
        return sql;
    }


}
