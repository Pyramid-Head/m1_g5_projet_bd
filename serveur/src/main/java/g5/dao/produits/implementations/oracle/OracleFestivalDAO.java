/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.FestivalDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.Festival;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class OracleFestivalDAO implements  FestivalDAO{
    Connection conn = null;
    public OracleFestivalDAO(){
        conn = ConnectToDB.getConnection();
    }

    private Festival generateOneFestival(ResultSet rs) throws SQLException {
        //(long idFestival, String numDepartement, long idCompte, long nom, String siteWeb,
        // String complementDomaine, String commentaires, long numeroDomaine, Date dateDeCreation,
        // Date dateDeDebut, Date dateDeFin, String coordonneesGps, double montantEnCaisse, String typeFest)
        long idFestival;
        String numDepartement;
        long idCompte;
        String nom;
        String siteWeb;
        String complementDomaine;
        String commentaires;
        Date dateDeCreation;
        Date dateDeDebut;
        Date dateDeFin;
        String coordonneesGps;
        double montantEnCaisse;
        String typeFest;
        int numEdition;

        rs.next();
            idFestival = rs.getInt("IDFESTIVAL");
            numDepartement = rs.getString("NUMDEPARTEMENT");
            idCompte = rs.getLong("IDCOMPTE");
            nom = rs.getString("NOM");
            siteWeb = rs.getString("SITEWEB");
            complementDomaine = rs.getString("COMPLEMENTDOMAINE");
            commentaires = rs.getString("COMMENTAIRES");
            numEdition = rs.getInt("NUMEROEDITION");
            dateDeCreation = rs.getDate("DATEDECREATION");
            dateDeDebut = rs.getDate("DATEDEDEBUT");
            dateDeFin=rs.getDate("DATEDEFIN");
            coordonneesGps = rs.getString("COORDONNEESGPS");
            montantEnCaisse = rs.getDouble("MONTANTENCAISSE");
            typeFest = rs.getString("TYPEFEST");

        return new Festival(idFestival,numDepartement,idCompte,nom,siteWeb,complementDomaine,commentaires,
                numEdition,dateDeCreation,dateDeDebut,dateDeFin,coordonneesGps,montantEnCaisse,typeFest);
    }


    private List<Festival> generateFestivals(ResultSet rs) throws SQLException {
        ArrayList<Festival> toReturn = new ArrayList<Festival>();

        long idFestival;
        String numDepartement;
        long idCompte;
        String nom;
        String siteWeb;
        String complementDomaine;
        String commentaires;
        Date dateDeCreation;
        Date dateDeDebut;
        Date dateDeFin;
        String coordonneesGps;
        double montantEnCaisse;
        String typeFest;
        int numEdition;

        while (rs.next()){
            idFestival = rs.getInt("IDFESTIVAL");
            numDepartement = rs.getString("NUMDEPARTEMENT");
            idCompte = rs.getLong("IDCOMPTE");
            nom = rs.getString("NOM");
            siteWeb = rs.getString("SITEWEB");
            complementDomaine = rs.getString("COMPLEMENTDOMAINE");
            commentaires = rs.getString("COMMENTAIRES");
            numEdition = rs.getInt("NUMEROEDITION");
            dateDeCreation = rs.getDate("DATEDECREATION");
            dateDeDebut = rs.getDate("DATEDEDEBUT");
            dateDeFin=rs.getDate("DATEDEFIN");
            coordonneesGps = rs.getString("COORDONNEESGPS");
            montantEnCaisse = rs.getDouble("MONTANTENCAISSE");
            typeFest = rs.getString("TYPEFEST");

           toReturn.add(new Festival(idFestival,numDepartement,idCompte,nom,siteWeb,complementDomaine,commentaires,
                    numEdition,dateDeCreation,dateDeDebut,dateDeFin,coordonneesGps,montantEnCaisse,typeFest));

        }
        return toReturn;
    }

    private List<Festival> generateLimitedFestivals(ResultSet rs) throws SQLException {
        ArrayList<Festival> toReturn = new ArrayList<Festival>();

        long idFestival;
        String numDepartement;
        String nom;
        Date dateDeDebut;
        Date dateDeFin;
        String typeFest;
        String siteWeb;

        while (rs.next()){
            idFestival = rs.getInt("IDFESTIVAL");
            numDepartement = rs.getString("NUMDEPARTEMENT");
            nom = rs.getString("NOM");
            dateDeDebut = rs.getDate("DATEDEDEBUT");
            dateDeFin=rs.getDate("DATEDEFIN");
            typeFest = rs.getString("TYPEFEST");
            siteWeb = rs.getString("SITEWEB");
            toReturn.add(new Festival(idFestival,numDepartement,nom,dateDeDebut,dateDeFin,typeFest,siteWeb));
        }
        return toReturn;
    }

    @Override
    public Festival getFestivalById(int idFestival) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM FESTIVAL WHERE IDFESTIVAL = " + idFestival;
        Festival fToRet = generateOneFestival(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }

    @Override
    public List<Festival> getFestivalByName(String festName,int nbrPersonne, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        /* tested query
 *  SELECT * FROM
   (SELECT f.*, ROW_NUMBER() over (order by f.IDFESTIVAL) R FROM FESTIVAL f, JOURFESTIVAL j
  WHERE f.NOM LIKE '%%' AND j.IDFESTIVAL = f.IDFESTIVAL AND j.PLACESLIBRES >= 1)
   WHERE R BETWEEN 51 and 100;
                */

        String query = "SELECT DISTINCT(B.IDFESTIVAL) ,B.* FROM ( SELECT f.* , ROW_NUMBER() over (order by f.IDFESTIVAL) R FROM FESTIVAL f, JOURFESTIVAL j" +
                " WHERE f.NOM LIKE '%"+festName+"%' "+ "AND j.IDFESTIVAL = f.IDFESTIVAL AND j.PLACESLIBRES >= " +
                nbrPersonne +") B" +
                " WHERE R BETWEEN "+ offset +" AND " + limit;

        System.out.print(query);
        List<Festival> listToRet = generateFestivals(stmt.executeQuery(query));
        stmt.close();
        return listToRet;
    }


    @Override
    public List<Festival> getFestivalBetween(Date startingDate, Date endDate,int nbrPersonne, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        /*
                    SELECT * FROM
            (SELECT f.* ,ROW_NUMBER() over (order by f.IDFESTIVAL) R  FROM FESTIVAL f,JOURFESTIVAL j
             WHERE f.DATEDEDEBUT >= TO_DATE('10/12/2018' , 'mm/dd/yyyy')
             AND f.DATEDEFIN <= TO_DATE('10/12/2019' , 'mm/dd/yyyy')
            AND j.IDFESTIVAL = f.IDFESTIVAL
            AND j.PLACESLIBRES >= 0
             ORDER BY f.IDFESTIVAL) WHERE R BETWEEN 1 and 10;
         */
        String getByIdQuery = "  SELECT * FROM ( SELECT f.* ,ROW_NUMBER() over (order by f.IDFESTIVAL) R " +
                "FROM FESTIVAL f,JOURFESTIVAL j" +
                " WHERE f.DATEDEDEBUT >= " + UsedFunctions.formatDate(startingDate)
                +" AND f.DATEDEFIN <= " +UsedFunctions.formatDate(endDate) +
                "AND j.IDFESTIVAL = f.IDFESTIVAL " +
                "AND j.PLACESLIBRES >= " + nbrPersonne
                +" ORDER BY f.IDFESTIVAL) WHERE R BETWEEN " +offset + " AND "+limit;
        List<Festival> listToRet = generateFestivals(stmt.executeQuery(getByIdQuery));
        stmt.close();
        return listToRet;
    }

    @Override
    public List<Festival> getFestivalByType(String typeFest,int nbrPersonne, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM (SELECT f.* , ROW_NUMBER() over (order by f.IDFESTIVAL) R " +
                "FROM FESTIVAL f,JOURFESTIVAL j WHERE f.TYPEFEST = '" + typeFest +"' " +
                "AND j.IDFESTIVAL = f.IDFESTIVAL AND j.PLACESLIBRES >= "+nbrPersonne+") " +
                "WHERE R BETWEEN "+ offset + " AND " + limit;
        List<Festival> listToRet = generateFestivals(stmt.executeQuery(query));
        stmt.close();
        return listToRet;
    }

    @Override
    public List<Festival> getFestivalByDept(String numDepartement,int nbrPersonne, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        /*
         SELECT * FROM (SELECT f.* , ROW_NUMBER() over (order by f.IDFESTIVAL) R
  FROM FESTIVAL f WHERE f.NUMDEPARTEMENT = '1')  WHERE R BETWEEN 1 AND 7;
         */
        String query = "SELECT * FROM (SELECT f.* , ROW_NUMBER() over (order by f.IDFESTIVAL) R " +
                "FROM FESTIVAL f,JOURFESTIVAL j WHERE f.NUMDEPARTEMENT = '" + numDepartement +"' " +
                "AND j.IDFESTIVAL = f.IDFESTIVAL AND j.PLACESLIBRES >= "+nbrPersonne+") " +
                "WHERE R BETWEEN "+ offset + " AND " + limit;
        List<Festival> listToRet = generateFestivals(stmt.executeQuery(query));
        stmt.close();
        return listToRet;
    }

    @Override
    public boolean create(Festival festival) {
        try {
            Statement stmt = conn.createStatement();
            String query ="INSERT INTO FESTIVAL (NUMDEPARTEMENT,IDCOMPTE,NOM,SITEWEB,COMPLEMENTDOMAINE,"
                    + "COMMENTAIRES,NUMEROEDITION,DATEDECREATION,DATEDEDEBUT,DATEDEFIN,COORDONNEESGPS," +
                    "MONTANTENCAISSE,TYPEFEST )"
                    + " VALUES ('"+festival.getNumDepartement() +"'," + festival.getIdCompte() + ",'"
                    + festival.getNom()+"','"+festival.getSiteWeb()+"','"+festival.getComplementDomaine()
                    +"','"+festival.getCommentaires()+"'," + festival.getNumEdition()
                    +","+UsedFunctions.formatDate(festival.getDateDeCreation()) + " , " +
                    UsedFunctions.formatDate(festival.getDateDeDebut())+" , " +
                    UsedFunctions.formatDate(festival.getDateDeFin())+" , " +
                    festival.getCoordonneesGps() +"'," + festival.getMontantEnCaisse() + ",'"+festival.getTypeFest() + "')";
            stmt.executeQuery(query);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Integer idFest) throws SQLException {
            Statement stmt = conn.createStatement();
            String query ="DELETE FROM FESTIVAL WHERE IDFESTIVAL = " + idFest;
            stmt.executeQuery(query);
            stmt.close();
            return true;
    }

    @Override
    public boolean update(int idFest,int idCompte) throws SQLException {

            Statement stmt = conn.createStatement();
        String updateQuery ="UPDATE FESTIVAL SET " +
               // "NUMDEPARTEMENT = '"+festival.getNumDepartement()+"'," +
                "IDCOMPTE = "+idCompte+
               // "NOM = '"+festival.getNom()+"' ," +
               // "SITEWEB = '"+festival.getSiteWeb()+"'," +
                //"COMPLEMENTDOMAINE = '"+festival.getComplementDomaine()+"',"+
                //"COMMENTAIRES = '"+festival.getCommentaires()+"'," +
                //"NUMEROEDITION = "+festival.getNumEdition()+"," +
               // "DATEDECREATION = "+UsedFunctions.formatDate(festival.getDateDeCreation())+"," +
               // "DATEDEDEBUT = "+UsedFunctions.formatDate(festival.getDateDeDebut())+"," +
               // "DATEDEFIN = "+UsedFunctions.formatDate(festival.getDateDeFin())+"," +
               // "COORDONNEESGPS = '"+festival.getCoordonneesGps()+"'," +
               // "MONTANTENCAISSE = "+festival.getMontantEnCaisse()+"," +
               // "TYPEFEST = '"+festival.getTypeFest()+"' "+
                " WHERE IDFESTIVAL = " + idFest;
            stmt.executeQuery(updateQuery);
            stmt.close();
            return true;

    }



    @Override
    public List<Festival> getFestivalsByAnyParam(String nom,String lieu,Date dateDebut, Date dateFin,
                                                 Double prixMin, Double prixMax,
                                                String typeFest,int nbrPersonne, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        /* tested query
 *  SELECT * FROM
   (SELECT f.*, ROW_NUMBER() over (order by f.IDFESTIVAL) R FROM FESTIVAL f, JOURFESTIVAL j
  WHERE f.NOM LIKE '%%' AND j.IDFESTIVAL = f.IDFESTIVAL AND j.PLACESLIBRES >= 1)
   WHERE R BETWEEN 51 and 100;
                */

        String query = "SELECT DISTINCT(B.IDFESTIVAL) ,B.* FROM ( SELECT DISTINCT(f.IDFESTIVAL),f.NUMDEPARTEMENT," +
                "f.NOM,f.DATEDEDEBUT,f.DATEDEFIN,f.TYPEFEST,f.SITEWEB, ROW_NUMBER() over (order by f.IDFESTIVAL) R " +
                "FROM FESTIVAL f, JOURFESTIVAL j" ;
        query += " WHERE j.PLACESLIBRES >= " +nbrPersonne;
        //Adding the needed Where clauses depending on given params!
        if(nom != "" && nom != null){
            query += " AND f.NOM LIKE '%"+nom+"%'";
        }
        if(lieu != "" && lieu != null){
            query += " AND f.NUMDEPARTEMENT LIKE '%"+lieu+"%'";
        }
        if(dateDebut != null && dateFin != null){
            query += " AND j.DATEJF BETWEEN "+ UsedFunctions.formatDate(dateDebut)+" AND "+
                    UsedFunctions.formatDate(dateFin);
        }
        if(prixMin != null && prixMax != null){
            query += " AND j.PRIX BETWEEN "+ prixMin +" AND "+ prixMax;
        }



        if(typeFest != "" && typeFest != null) {
            String[] arrDeTypes = typeFest.split(";");

            query += " AND " ;
            query += arrDeTypes.length>1?"(":"";
             query+= " f.TYPEFEST LIKE '%" + arrDeTypes[0] + "%'";
             for (int i = 1; i<arrDeTypes.length;i++){
                 query+= " OR f.TYPEFEST LIKE '%" + arrDeTypes[i] + "%'";
             }
            query += arrDeTypes.length>1?")":"";
        }



        query += " AND j.IDFESTIVAL = f.IDFESTIVAL "
                + " ORDER BY (DATEDEDEBUT) ) B" +
                " WHERE R BETWEEN "+ offset +" AND " + limit;

        System.out.println("TEST");
        System.out.print(query);
        System.out.println("TEST");

        List<Festival> listToRet = generateLimitedFestivals(stmt.executeQuery(query));
        stmt.close();
        return listToRet;

    }



@Override
    public List<Festival> getFestivalsByHeberg(Long idHeberg,Date dateDebut,Date dateFin, Integer nbrPersonne) throws SQLException {
        Statement stmt = conn.createStatement();
        nbrPersonne= nbrPersonne==null?nbrPersonne:0;


        /*
        SELECT DISTINCT(B.IDFESTIVAL) ,B.* FROM ( SELECT DISTINCT(f.IDFESTIVAL),f.NUMDEPARTEMENT,f.NOM,f.DATEDEDEBUT,f.DATEDEFIN,
            f.TYPEFEST,f.SITEWEB, ROW_NUMBER() over (order by f.IDFESTIVAL) R
            FROM FESTIVAL f, JOURFESTIVAL j
      ,HEBERGEMENT h, INFRASTRUCTURE i
            WHERE j.PLACESLIBRES >= 0
            --AND j.DATEJF BETWEEN TO_DATE('23/01/2018', 'dd/mm/yyyy') AND TO_DATE('27/10/2018', 'dd/mm/yyyy')
      AND h.IDHEBERGEMENT=49694
            AND j.IDFESTIVAL = f.IDFESTIVAL
            AND i.IDINFRASTRUCTURE=h.IDINFRASTRUCTURE
            AND i.NUMDEPARTEMENT = f.NUMDEPARTEMENT
            ORDER BY (DATEDEDEBUT) ) B
            WHERE R BETWEEN 0 AND 19;
         */



        String query = "SELECT DISTINCT(B.IDFESTIVAL) ,B.* FROM ( SELECT DISTINCT(f.IDFESTIVAL),f.NUMDEPARTEMENT," +
                "f.NOM,f.DATEDEDEBUT,f.DATEDEFIN,f.TYPEFEST,f.SITEWEB, ROW_NUMBER() over (order by f.IDFESTIVAL) R " +
                "FROM FESTIVAL f, JOURFESTIVAL j , HEBERGEMENT h, INFRASTRUCTURE i" ;
        query += " WHERE j.PLACESLIBRES >= " +nbrPersonne;
        //Adding the needed Where clauses depending on given params!
        if(dateDebut != null && dateFin != null){
            query += " AND j.DATEJF BETWEEN "+ UsedFunctions.formatDate(dateDebut)+" AND "+
                    UsedFunctions.formatDate(dateFin);
        }
        query+=  " AND h.IDHEBERGEMENT= " + idHeberg;
        query += " AND j.IDFESTIVAL = f.IDFESTIVAL " +
                " AND i.IDINFRASTRUCTURE=h.IDINFRASTRUCTURE " +
                " AND i.NUMDEPARTEMENT = f.NUMDEPARTEMENT "
                + " ORDER BY (DATEDEDEBUT) ) B" +
                " WHERE R BETWEEN "+ 0 +" AND " + 19;
        System.out.println("TEST");
        System.out.print(query);
        System.out.println("TEST");

        List<Festival> listToRet = generateLimitedFestivals(stmt.executeQuery(query));
        stmt.close();
        return listToRet;

    }





}
