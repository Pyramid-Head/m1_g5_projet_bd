/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.DispoHebergementDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.DispoHebergement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class OracleDispoHebergementDAO implements DispoHebergementDAO {
    Connection conn = null;
    public OracleDispoHebergementDAO(){
        conn = ConnectToDB.getConnection();
    }

    private DispoHebergement generateOneDispoHeberg (ResultSet rs) throws SQLException {
        rs.next();
        return new DispoHebergement(rs.getLong("IDHEBERGEMENT"),rs.getDate("DATECAL"));
    }

    private List<DispoHebergement> generateMultiDispoHeberg (ResultSet rs) throws SQLException {
        ArrayList<DispoHebergement> toRet = new ArrayList<>();
        while(rs.next()){
            toRet.add(new DispoHebergement(rs.getLong("IDHEBERGEMENT"),rs.getDate("DATECAL")));
        }
        return toRet;
    }

    @Override
    public boolean create(DispoHebergement dispoHebergement) {
        try {
            Statement stmt = conn.createStatement();
            String requeststr = "INSERT INTO DISPOHEBERGEMENT (IDHEBERGEMENT,DATECAL)" +
                    "VALUES ("+dispoHebergement.getIdHebergement()+", "
                    +UsedFunctions.formatDate(dispoHebergement.getDateCal())+")";
            stmt.executeQuery(requeststr);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public DispoHebergement getById(long hebergId, Date calDate) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM DISPOHEBERGEMENT WHERE IDHEBERGEMENT = "+hebergId+
                " AND DATECAL = "+ UsedFunctions.formatDate(calDate);
        DispoHebergement IToRet = generateOneDispoHeberg(stmt.executeQuery(query));
        stmt.close();
        return IToRet;
    }

    @Override
    public List<DispoHebergement> getDispoParDate(Date dateDebut,Date dateFin, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM (SELECT i.*, ROW_NUMBER() over (order by i.DATECAL) R  FROM DISPOHEBERGEMENT i" +
                " WHERE i.DATECAL BETWEEN "+UsedFunctions.formatDate(dateDebut)+" AND "+
                UsedFunctions.formatDate(dateFin)+") "
                + " WHERE R BETWEEN "+ offset +" AND " + limit;
        ArrayList<DispoHebergement> IToRet = (ArrayList<DispoHebergement>) generateMultiDispoHeberg(stmt.executeQuery(query));
        stmt.close();
        return IToRet;
    }

    @Override
    public List<DispoHebergement> getDateParHeberg(long hebergId, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM (SELECT i.*, ROW_NUMBER() over (order by i.DATECAL) R  FROM DISPOHEBERGEMENT i" +
                " WHERE i.IDHEBERGEMENT = "+hebergId +")"
                + " WHERE R BETWEEN "+ offset +" AND " + limit;
        ArrayList<DispoHebergement> IToRet = (ArrayList<DispoHebergement>) generateMultiDispoHeberg(stmt.executeQuery(query));
        stmt.close();
        return IToRet;
    }

    @Override
    public List<DispoHebergement> getAll(int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM (SELECT i.*, ROW_NUMBER() over (order by i.DATECAL) R  FROM DISPOHEBERGEMENT i) "
                + " WHERE R BETWEEN "+ offset +" AND " + limit;
        ArrayList<DispoHebergement> IToRet = (ArrayList<DispoHebergement>) generateMultiDispoHeberg(stmt.executeQuery(query));
        stmt.close();
        return IToRet;
    }




    @Override
    public boolean delete(DispoHebergement dispoHebergement) {
        try {
            Statement stmt = conn.createStatement();
            String requeststr = "DELETE FROM DISPOHEBERGEMENT WHERE IDHEBERGEMENT = " +dispoHebergement.getIdHebergement()
                    +" AND DATECAL = " +UsedFunctions.formatDate(dispoHebergement.getDateCal());
            stmt.executeQuery(requeststr);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
         }


}
