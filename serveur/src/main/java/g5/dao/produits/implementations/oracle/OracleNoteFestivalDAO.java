/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.NoteFestivalDAO;
import g5.models.NoteFestival;

import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class OracleNoteFestivalDAO implements NoteFestivalDAO {

    @Override
    public NoteFestival getNoteFestById(int noteFestId) {
        return null;
    }

    @Override
    public List<NoteFestival> getNotesDeFestival(int idFestival) {
        return null;
    }

    @Override
    public List<NoteFestival> getNotesParClient(int idCompte) {
        return null;
    }

    @Override
    public boolean create(NoteFestival noteFestival) {
        return false;
    }

    @Override
    public boolean delete(NoteFestival noteFestival) {
        return false;
    }

    @Override
    public boolean update(NoteFestival noteFestival) {
        return false;
    }
}
