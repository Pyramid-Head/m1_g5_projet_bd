package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.DepartementDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.Departement;
import g5.models.JourFestival;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class OracleDepartementDAO implements DepartementDAO {
    Connection conn;
    public OracleDepartementDAO(){
        conn = ConnectToDB.getConnection();
    }

    private Departement generateDept(ResultSet rs) throws SQLException {
        rs.next();
        String dNbr = rs.getString("NUMDEPARTEMENT");
        String dName = rs.getString("NOMDEPARTEMENT");

        return new Departement(dNbr,dName,"");
    }

    @Override
    public Departement getDepartementParNumero(String numeroDept) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM DEPARTEMENT WHERE NUMDEPARTEMENT = " + numeroDept;
        Departement dToRet = generateDept(stmt.executeQuery(query));
        stmt.close();
        return dToRet;
    }

    @Override
    public Departement getDepartementParNom(String nomDept) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM DEPARTEMENT WHERE NOMDEPARTEMENT = " + nomDept;
        Departement dToRet = generateDept(stmt.executeQuery(query));
        stmt.close();
        return dToRet;    }
}
