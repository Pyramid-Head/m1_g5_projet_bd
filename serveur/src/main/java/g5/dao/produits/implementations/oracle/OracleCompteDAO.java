/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.CompteDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.Compte;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class OracleCompteDAO implements CompteDAO{

    Connection conn;
    public OracleCompteDAO(){
        conn = ConnectToDB.getConnection();
    }

    private static Compte   generateCompte(){
       // Compte toRet = new Compte()

        return null;
    }




    @Override
    public Compte getCompteById(int idCompte) {
        return null;
    }

    @Override
    public Compte getCompteByFbKey(String fbKey) {
        return null;
    }

    @Override
    public Integer getCompteIDByMail(String mail) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT IDCOMPTE FROM COMPTE WHERE COURRIEL = '"+mail+"'";
        ResultSet rs = stmt.executeQuery(query);
        rs.next();
        Integer toRet = rs.getInt("IDCOMPTE");
        stmt.close();
        return toRet;
    }

    @Override
    public List<Compte> getHebergeurs() {
        return null;
    }

    @Override
    public List<Compte> getOrganizateurs() {
        return null;
    }

    @Override
    public List<Compte> getUtilisateurs() {
        return null;
    }

    @Override
    public List<Compte> getComptes() {
        return null;
    }

    @Override
    public boolean create(Compte compte) {
        return false;
    }

    @Override
    public boolean delete(Compte compte) {
        return false;
    }

    @Override
    public boolean update(Compte compte) {
        return false;
    }
}
