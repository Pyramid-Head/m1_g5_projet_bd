/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.ReservationDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.Reservation;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class OracleReservationDAO implements ReservationDAO {

    Connection conn = null;
    public OracleReservationDAO(){
        conn = ConnectToDB.getConnection();
    }

    private Reservation generateUneReservation(ResultSet rs) throws SQLException {
        long idReservation;
        long idCompte;
        Date datePaiement;
        double montantTotal;
        rs.next();
        idReservation = rs.getLong("IDRESERVATION");
        idCompte = rs.getLong("IDCOMPTE");
        datePaiement = rs.getDate("DATEPAIEMENT");
        montantTotal = rs.getDouble("MONTANTTOTAL");
        return new Reservation(idReservation,idCompte,datePaiement,montantTotal);
    }



    private List<Reservation> generatePlusieurReservation(ResultSet rs) throws SQLException {
        long idReservation;
        long idCompte;
        Date datePaiement;
        double montantTotal;
        ArrayList<Reservation> toRet = new ArrayList<>();

        while (rs.next()){
            idReservation = rs.getLong("IDRESERVATION");
            idCompte = rs.getLong("IDCOMPTE");
            datePaiement = rs.getDate("DATEPAIEMENT");
            montantTotal = rs.getDouble("MONTANTTOTAL");
            toRet.add(new Reservation(idReservation,idCompte,datePaiement,montantTotal));
        }
        return toRet;
    }


    @Override
    public Reservation getReservationById(int idReservation) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM RESERVATION WHERE IDRESERVATION = " + idReservation ;
        Reservation fToRet = generateUneReservation(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }

    @Override
    public List<Reservation> getReservationParUtilisateur(int idCompte) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM RESERVATION WHERE IDCOMPTE = " + idCompte;
        List<Reservation> fToRet = generatePlusieurReservation(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }

    @Override
    public Integer create(Reservation reservation) {
        try {
            CallableStatement statement = null;
            String query;
            if(reservation.getDatePaiement()==null){
                query = "BEGIN INSERT INTO RESERVATION (IDCOMPTE,MONTANTTOTAL,DATEDECREATION)" +
                        " VALUES ("+reservation.getIdCompte()
                        +","+reservation.getMontantTotal()+",CURRENT_TIMESTAMP)"+ " RETURNING IDRESERVATION INTO ?; "
                        + "END;";
                System.out.println("MA FI RESERVATION DATE"+query);
            }else{
                 query = "BEGIN INSERT INTO RESERVATION (IDCOMPTE,DATEPAIEMENT,MONTANTTOTAL)" +
                        " VALUES ("+reservation.getIdCompte()+","+ UsedFunctions.formatDate(reservation.getDatePaiement())
                        +","+reservation.getMontantTotal()+")"+ " RETURNING IDRESERVATION INTO ?; "
                         + "END;";
                System.out.println("FI RESERVATION DATE"+query);

            }

            statement = conn.prepareCall(query);
            statement.registerOutParameter(1, Types.INTEGER);
            statement.execute();
            int resaID = statement.getInt(1);
            return resaID;
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public boolean delete(Integer idReserv) throws SQLException {

            Statement stmt = conn.createStatement();
            stmt.executeQuery("DELETE  FROM RESAHEBERGEMENT WHERE IDRESERVATION =  " +idReserv);
            stmt.executeQuery("DELETE  FROM RESAJOURFESTIVAL WHERE IDRESERVATION =  " +idReserv);
            stmt.executeQuery("DELETE  FROM RESERVATION WHERE IDRESERVATION =  " +idReserv);
            stmt.close();
            return true;

    }

    @Override
    public boolean update(Reservation reservation) {
        try {
            Statement stmt = conn.createStatement();
            String query = "UPDATE RESERVATION SET "+
                    "DATEPAIEMENT = "+UsedFunctions.formatDate(reservation.getDatePaiement())+","+
                    "MONTANTTOTAL = " + reservation.getMontantTotal()+
                    " WHERE IDRESERVATION = " + reservation.getIdReservation();
            stmt.executeQuery(query);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
