/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.HebergementDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.Hebergement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class OracleHebergementDAO implements HebergementDAO {
    Connection conn = null;
    public OracleHebergementDAO(){
        conn = ConnectToDB.getConnection();
    }


    private Hebergement generateOneHebergement(ResultSet rs) throws SQLException {
       int IDHEBERGEMENT;
               int IDINFRASTRUCTURE;
        String DESCRIPTIONHEBERG;
        String TYPEHEBERG;
        int CAPACITE;
                double TARIFFORFAITAIRE;
        int NBPLACESADULTES;
               int NBPLACESENFANTS;
        double TARIFENFANT;
        double    TARIFADULTE;

        rs.next();
        IDHEBERGEMENT = rs.getInt("IDHEBERGEMENT");
                IDINFRASTRUCTURE = rs.getInt("IDINFRASTRUCTURE");
        DESCRIPTIONHEBERG=rs.getString("DESCRIPTIONHEBERG");
                TYPEHEBERG = rs.getString("TYPEHEBERG");
        CAPACITE= rs.getInt("CAPACITE");
                TARIFFORFAITAIRE= rs.getDouble("TARIFFORFAITAIRE");
        NBPLACESADULTES=rs.getInt("NBPLACESADULTES");
                NBPLACESENFANTS=rs.getInt("NBPLACESENFANTS");
        TARIFENFANT = rs.getDouble("TARIFENFANT");
                TARIFADULTE = rs.getDouble("TARIFADULTE");

                return new Hebergement(IDHEBERGEMENT,IDINFRASTRUCTURE,DESCRIPTIONHEBERG,TYPEHEBERG,CAPACITE
                ,TARIFFORFAITAIRE,NBPLACESADULTES,NBPLACESENFANTS,TARIFENFANT,TARIFADULTE);
    }

    private List<Hebergement> generateHebergementS(ResultSet rs) throws SQLException {
        int IDHEBERGEMENT;
        int IDINFRASTRUCTURE;
        String DESCRIPTIONHEBERG;
        String TYPEHEBERG;
        int CAPACITE;
        double TARIFFORFAITAIRE;
        int NBPLACESADULTES;
        int NBPLACESENFANTS;
        double TARIFENFANT;
        double    TARIFADULTE;
        ArrayList<Hebergement> toRet = new ArrayList<>();

        while (rs.next()){
            IDHEBERGEMENT = rs.getInt("IDHEBERGEMENT");
            IDINFRASTRUCTURE = rs.getInt("IDINFRASTRUCTURE");
            DESCRIPTIONHEBERG=rs.getString("DESCRIPTIONHEBERG");
            TYPEHEBERG = rs.getString("TYPEHEBERG");
            CAPACITE= rs.getInt("CAPACITE");
            TARIFFORFAITAIRE= rs.getDouble("TARIFFORFAITAIRE");
            NBPLACESADULTES=rs.getInt("NBPLACESADULTES");
            NBPLACESENFANTS=rs.getInt("NBPLACESENFANTS");
            TARIFENFANT = rs.getDouble("TARIFENFANT");
            TARIFADULTE = rs.getDouble("TARIFADULTE");

            toRet.add(new Hebergement(IDHEBERGEMENT,IDINFRASTRUCTURE,DESCRIPTIONHEBERG,TYPEHEBERG,CAPACITE
                    ,TARIFFORFAITAIRE,NBPLACESADULTES,NBPLACESENFANTS,TARIFENFANT,TARIFADULTE));
        }
        return toRet;
         }


    @Override
    public Hebergement getById(long id) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM HEBERGEMENT WHERE IDHEBERGEMENT = " + id;
        Hebergement fToRet = generateOneHebergement(stmt.executeQuery(query));
        stmt.close();
        return fToRet;    }

    @Override
    public List<Hebergement> getAll(int nbrEnfant, int nbrAdulte, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
        /*
         SELECT * FROM (SELECT h.*, ROW_NUMBER() over (order by h.IDHEBERGEMENT) R FROM HEBERGEMENT h
               WHERE h.NBPLACESADULTES > 8 AND h.NBPLACESENFANTS > 2)  WHERE R BETWEEN
              2 AND 4;
         */
        String query = "SELECT * FROM (SELECT h.*, ROW_NUMBER() over (order by h.IDHEBERGEMENT) R FROM HEBERGEMENT h" +
              " WHERE h.NBPLACESADULTES >= "+nbrAdulte+" AND h.NBPLACESENFANTS >= "+nbrEnfant+")  WHERE R BETWEEN "
              +offset+" AND "+limit;
        List<Hebergement> fToRet = generateHebergementS(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }

    @Override
    public List<Hebergement> getByInfrastructureId(int infraId, int nbrEnfant, int nbrAdulte) throws SQLException {
        Statement stmt = conn.createStatement();

        String query = "SELECT h.*, ROW_NUMBER() over (order by h.IDHEBERGEMENT) R FROM HEBERGEMENT h" +
                " WHERE h.NBPLACESADULTES >= "+nbrAdulte+" AND h.NBPLACESENFANTS >= "+nbrEnfant+
                " AND h.IDINFRASTRUCTURE = "+infraId;
        List<Hebergement> fToRet = generateHebergementS(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }

    @Override
    public List<Hebergement> getByType(String type, int nbrEnfant, int nbrAdulte, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();

        String query = "SELECT * FROM (SELECT h.*, ROW_NUMBER() over (order by h.IDHEBERGEMENT) R FROM HEBERGEMENT h" +
                " WHERE h.NBPLACESADULTES >= "+nbrAdulte+" AND h.NBPLACESENFANTS >= "+nbrEnfant+
                " AND h.TYPEHEBERG = '"+type
                +"')  WHERE R BETWEEN "
                +offset+" AND "+limit;
        List<Hebergement> fToRet = generateHebergementS(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }

    @Override
    public List<Hebergement> getByInfrastructureName(String infraName, int nbrEnfant, int nbrAdulte, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
/**
 *   SELECT * FROM (SELECT i.NOM,h.*, ROW_NUMBER() over (order by h.IDHEBERGEMENT) R FROM HEBERGEMENT h,INFRASTRUCTURE i
 *                WHERE h.NBPLACESADULTES >= 1 AND h.NBPLACESENFANTS >= 0
 *                AND i.NOM LIKE '%PP%' AND h.IDINFRASTRUCTURE = i.IDINFRASTRUCTURE)  WHERE R BETWEEN
 *               0 AND 20;
 */
        String query = "SELECT * FROM (SELECT h.*, ROW_NUMBER() over (order by h.IDHEBERGEMENT) R FROM HEBERGEMENT h,INFRASTRUCTURE i" +
                " WHERE h.NBPLACESADULTES >= "+nbrAdulte+" AND h.NBPLACESENFANTS >= "+nbrEnfant+
                " AND i.NOM LIKE '%"+infraName.toUpperCase()+"%'"
                +" AND i.IDINFRASTRUCTURE = h.IDINFRASTRUCTURE "
                +")  WHERE R BETWEEN "+offset+" AND "+limit;
        List<Hebergement> fToRet = generateHebergementS(stmt.executeQuery(query));
        stmt.close();
        return fToRet;

    }


    @Override
    public List<Hebergement> getByDepartment(String dept, int nbrEnfant, int nbrAdulte, int offset, int limit) throws SQLException {
        Statement stmt = conn.createStatement();
/**
 *
 SELECT * FROM (SELECT i.NOM,h.*, ROW_NUMBER() over (order by h.IDHEBERGEMENT) R FROM HEBERGEMENT h,INFRASTRUCTURE i
 WHERE h.NBPLACESADULTES >= 1 AND h.NBPLACESENFANTS >= 0 AND i.NUMDEPARTEMENT = '3' AND h.IDINFRASTRUCTURE = i.IDINFRASTRUCTURE)  WHERE R BETWEEN
 0 AND 20;
 */
        String query = "SELECT * FROM (SELECT h.*, ROW_NUMBER() over (order by h.IDHEBERGEMENT) R FROM HEBERGEMENT h,INFRASTRUCTURE i" +
                " WHERE h.NBPLACESADULTES >= "+nbrAdulte+" AND h.NBPLACESENFANTS >= "+nbrEnfant+
                " AND i.NUMDEPARTEMENT = '"+dept+"'"
                +" AND i.IDINFRASTRUCTURE = h.IDINFRASTRUCTURE "
                +")  WHERE R BETWEEN "+offset+" AND "+limit;
        List<Hebergement> fToRet = generateHebergementS(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }




    @Override
    public boolean create(Hebergement hebergement) {
        try {
            Statement stmt = conn.createStatement();
        String query = "INSERT INTO HEBERGEMENT (IDINFRASTRUCTURE,DESCRIPTIONHEBERG,TYPEHEBERG," +
                "CAPACITE,TARIFFORFAITAIRE,NBPLACESADULTES,NBPLACESENFANTS,TARIFENFANT,TARIFADULTE)" +
                " VALUES ("+hebergement.getIdInfrastructure()+", '"+hebergement.getDescriptionHeberg()+"', '" +
                hebergement.getTypeHeberg()+"',"+hebergement.getCapacite()+","+hebergement.getTarifForfaitaire()+", " +
                hebergement.getNbPlacesAdultes()+", "+hebergement.getNbPlacesEnfants()+", "+hebergement.getTarifEnfant()
        +", "+hebergement.getTarifAdulte()+")";

        stmt.executeQuery(query);
        stmt.close();
        return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    @Override
    public boolean update(Hebergement hebergement) {
        try {
            Statement stmt = conn.createStatement();
            String query = "UPDATE HEBERGEMENT SET " +
                    "IDINFRASTRUCTURE = "+hebergement.getIdInfrastructure() +", "+
                    "DESCRIPTIONHEBERG = '"+hebergement.getDescriptionHeberg()+"' ," +
                    "TYPEHEBERG = '"+hebergement.getTypeHeberg()+"', " +
                    "CAPACITE = "+hebergement.getCapacite()+", " +
                    "TARIFFORFAITAIRE = "+hebergement.getTarifForfaitaire()+", " +
                    "NBPLACESADULTES = "+hebergement.getNbPlacesAdultes()+", " +
                    "NBPLACESENFANTS = "+hebergement.getNbPlacesEnfants()+", " +
                    "TARIFENFANT = "+hebergement.getTarifEnfant()+", " +
                    "TARIFADULTE = "+hebergement.getTarifAdulte();
            stmt.executeQuery(query);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Hebergement hebergement) {
        try {
            Statement stmt = conn.createStatement();
            String query = "DELETE FROM HEBERGEMENT WHERE IDHEBERGEMENT = "+hebergement.getIdHebergement();
            stmt.executeQuery(query);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }




}
