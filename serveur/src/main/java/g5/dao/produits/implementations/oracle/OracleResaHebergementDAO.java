/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.ResaHebergementDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.ResaHebergement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class OracleResaHebergementDAO implements ResaHebergementDAO {
    Connection conn = null;
    public OracleResaHebergementDAO(){
        conn = ConnectToDB.getConnection();
    }
    @Override
    public boolean create(ResaHebergement resaHebergement)  throws SQLException{
            Statement stmt = conn.createStatement();
            String query ="INSERT INTO RESAHEBERGEMENT (IDRESERVATION,IDHEBERGEMENT,DATEDEBUT, DATEFIN, " +
                    "NBENFANTS, NBADULTES, DATEHEUREMISEENPANIER)" +
                    "VALUES ("+resaHebergement.getIdResevation()+" , "+resaHebergement.getIdHebergement()+" , "
                    + UsedFunctions.formatDate(resaHebergement.getDateDebut())+" , " +
                    UsedFunctions.formatDate(resaHebergement.getDateFin())+" , " +
                    resaHebergement.getNbEnfants() +" , " + resaHebergement.getNbAdultes() +
                    ", CURRENT_TIMESTAMP)";

        System.out.println("!!!!! "+ query);
            stmt.executeQuery(query);
            stmt.close();
            return true;
    }

    @Override
    public ResaHebergement getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ResaHebergement> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<ResaHebergement> getResaHebPanier(){
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(ResaHebergement resaHebergement) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(ResaHebergement resaHebergement) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ResaHebergement> getByResaId(Integer resaId) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM RESAHEBERGEMENT WHERE IDRESERVATION = " + resaId;
        List<ResaHebergement> fToRet = generatePlusieursResaHebrg(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }



    private static  List<ResaHebergement> generatePlusieursResaHebrg(ResultSet rs) throws SQLException {
        Integer IDRESERVATION;
            Integer    IDHEBERGEMENT;
        Date DATEDEBUT;
        Date      DATEFIN;
        Integer NBENFANTS;
        Integer        NBADULTES;
        Date DATEHEUREMISEENPANIER;
        ArrayList<ResaHebergement> arToret = new ArrayList<>();

        while (rs.next()){
            IDRESERVATION = rs.getInt("IDRESERVATION");
            IDHEBERGEMENT= rs.getInt("IDHEBERGEMENT");
            DATEDEBUT = rs.getDate("DATEDEBUT");
            DATEFIN = rs.getDate("DATEFIN");
            NBENFANTS=rs.getInt("NBENFANTS");
            NBADULTES=rs.getInt("NBADULTES");
            DATEHEUREMISEENPANIER=rs.getDate("DATEHEUREMISEENPANIER");
            arToret.add(new ResaHebergement(IDRESERVATION,IDHEBERGEMENT,DATEDEBUT,DATEFIN,NBENFANTS,NBADULTES,DATEHEUREMISEENPANIER));
        }
        return arToret;
    }

}
