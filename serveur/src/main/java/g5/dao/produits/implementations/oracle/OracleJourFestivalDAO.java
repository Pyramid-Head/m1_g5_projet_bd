/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.JourFestivalDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.JourFestival;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class OracleJourFestivalDAO implements JourFestivalDAO {

    Connection conn = null;
    public OracleJourFestivalDAO(){
        conn = ConnectToDB.getConnection();
    }

    private JourFestival generateOneJour(ResultSet rs) throws SQLException {
        rs.next();
         long idJourF = rs.getLong("IDJOURF");
         long idFestival = rs.getLong("IDFESTIVAL");
         Date dateJf = rs.getDate("DATEJF");
         double prix = Double.parseDouble(new DecimalFormat("#,##").format(rs.getDouble("PRIX")));
         int placesLibres = rs.getInt("PLACESLIBRES");
         String typePlace = rs.getString("TYPEPLACE");

       return new JourFestival(idJourF,idFestival,dateJf,prix,placesLibres,typePlace);
    }


    private List<JourFestival> generatePlusieurJours(ResultSet rs) throws SQLException {
         long idJourF;
         long idFestival;
         Date dateJf;
         double prix;
         int placesLibres;
         String typePlace;
         List<JourFestival> lToRet = new ArrayList<JourFestival>();

         while (rs.next()){
             idJourF = rs.getLong("IDJOURF");
             idFestival = rs.getLong("IDFESTIVAL");
             dateJf = rs.getDate("DATEJF");
             //new DecimalFormat("#.##").format(dblVar)
             prix = Double.parseDouble(new DecimalFormat("#,##").format(rs.getDouble("PRIX")));
             placesLibres = rs.getInt("PLACESLIBRES");
             typePlace = rs.getString("TYPEPLACE");

            lToRet.add(new JourFestival(idJourF,idFestival,dateJf,prix,placesLibres,typePlace));
         }

        return lToRet;
    }

    @Override
    public JourFestival getJourFestivalById(int idJourFest) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM JOURFESTIVAL WHERE IDJOURF = " + idJourFest;
        JourFestival jToRet = generateOneJour(stmt.executeQuery(query));
        stmt.close();
        return jToRet;
    }



    @Override
    public List<JourFestival> getJoursDeFestival(int festivalId,int nbrPersonnes) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM JOURFESTIVAL WHERE IDFESTIVAL = " + festivalId
                +" AND PLACESLIBRES >= "+nbrPersonnes;
        List<JourFestival> jToRet = generatePlusieurJours(stmt.executeQuery(query));
        stmt.close();
        return jToRet;
    }

    @Override
    public List<JourFestival> getJoursDeFestivalPremiereClasse(int festivalId,int nbrPersonnes) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM JOURFESTIVAL WHERE IDFESTIVAL = " + festivalId + " AND TYPEPLACE = 'CAT1'"
                +" AND PLACESLIBRES >= "+nbrPersonnes;
        List<JourFestival> jToRet = generatePlusieurJours(stmt.executeQuery(query));
        stmt.close();
        return jToRet;
    }

    @Override
    public List<JourFestival> getJoursDeFestivalDeuxiemmeClasse(int festivalId,int nbrPersonnes) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM JOURFESTIVAL WHERE IDFESTIVAL = " + festivalId + " AND TYPEPLACE = 'CAT2'"
                +" AND PLACESLIBRES >= "+nbrPersonnes;
        List<JourFestival> jToRet = generatePlusieurJours(stmt.executeQuery(query));
        stmt.close();
        return jToRet;
    }

    @Override
    public List<JourFestival> getJoursDeFestivalSansClasse(int festivalId,int nbrPersonnes) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM JOURFESTIVAL WHERE IDFESTIVAL = " + festivalId + " AND TYPEPLACE = 'SANSCAT'"
                +" AND PLACESLIBRES >= "+nbrPersonnes;
        List<JourFestival> jToRet = generatePlusieurJours(stmt.executeQuery(query));
        stmt.close();
        return jToRet;
    }

    @Override
    public boolean create(JourFestival jourFest) {
        try {
            Statement stmt = conn.createStatement();
        String requeststr = "INSERT INTO JOURFESTIVAL "
                + "(IDFESTIVAL,DATEJF,PRIX,PLACESLIBRES,TYPEPLACE) "+
                "VALUES ("+ jourFest.getIdFestival() +
                UsedFunctions.formatDate(jourFest.getDateJf()) + ", "
                + jourFest.getPrix() + " , "
                + jourFest.getPlacesLibres()
                + " , "+UsedFunctions.addSemiQuotes(jourFest.getTypePlace())+")";
            stmt.executeQuery(requeststr);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(JourFestival jourFest) {
        try {
            Statement stmt = conn.createStatement();
            String requeststr = "DELETE FROM JOURFESTIVAL WHERE IDJOURF = " + jourFest.getIdJourF();
            stmt.executeQuery(requeststr);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteJourDuFestival(int festivalID) {
        try {
            Statement stmt = conn.createStatement();
            String requeststr = "DELETE FROM JOURFESTIVAL WHERE IDFESTIVAL = " + festivalID;
            stmt.executeQuery(requeststr);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(JourFestival jourFest) {
        try {
            Statement stmt = conn.createStatement();
            String requeststr = "UPDATE JOURFESTIVAL SET "+
                    "IDFESTIVAL = " +jourFest.getIdFestival()+
                    "DATEJF = " +UsedFunctions.formatDate(jourFest.getDateJf())+
                    "PRIX = " + jourFest.getPrix() +
                    "PLACESLIBRES = " + jourFest.getPlacesLibres() +
                    "TYPEPLACE = "+ UsedFunctions.addSemiQuotes(jourFest.getTypePlace())
                    +" WHERE IDJOURF = " + jourFest.getIdJourF();
            stmt.executeQuery(requeststr);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
