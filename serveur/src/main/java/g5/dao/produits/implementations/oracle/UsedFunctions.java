package g5.dao.produits.implementations.oracle;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class UsedFunctions {

    public static String formatDate(Date dateToFormat) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return  "TO_DATE('"+df.format(dateToFormat)+"', 'dd/mm/yyyy')";
    }

    public static String myTrim(String toTrim) {
       if(toTrim != null || toTrim.length() == 0){
           return toTrim;
       }
        else {
            return toTrim.trim();
       }

    }

    public static String addSemiQuotes(String toSemQuote) {
        return "'"+toSemQuote+"'";
    }

    public static List<Date> getDatesBetween(
            Date startDate, Date endDate) {
        List<Date> datesInRange = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(endDate);

        while (calendar.before(endCalendar)) {
            Date result = calendar.getTime();
            datesInRange.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return datesInRange;
    }



    public static Date StringToDate(String dob) throws ParseException {
        //Instantiating the SimpleDateFormat class
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        //Parsing the given String to Date object
        Date date = formatter.parse(dob);
        System.out.println("Date object value: "+date);
        return date;
    }


}
