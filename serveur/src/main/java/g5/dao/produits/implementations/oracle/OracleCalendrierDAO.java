/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.CalendrierDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.Calendrier;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author Julien Rull
 */
public class OracleCalendrierDAO implements CalendrierDAO {
    Connection conn = null;
    public OracleCalendrierDAO(){
        conn = ConnectToDB.getConnection();
    }




    private Calendrier generateCalendarInstance(Date dateCal){
        return new Calendrier(dateCal);
    }

    @Override
    public Calendrier getById(Date idCal) throws SQLException {
        Statement stmt = conn.createStatement();
        String getByIdQuery = "SELECT * FROM CALENDRIER WHERE DATECAL = "+UsedFunctions.formatDate(idCal);
        ResultSet rs = stmt.executeQuery(getByIdQuery);
       Calendrier toReturnCal = generateCalendarInstance(rs.getDate("DATECAL"));
        stmt.close();
        return toReturnCal;
    }



    @Override
    public List<Calendrier> getAll() throws SQLException {
        Statement stmt = conn.createStatement();
        String getByIdQuery = "SELECT * FROM CALENDRIER";
        ResultSet rs = stmt.executeQuery(getByIdQuery);
        List<Calendrier> toRetList = new ArrayList<Calendrier>();
        while (rs.next()) {
            toRetList.add(generateCalendarInstance(rs.getDate("DATECAL")));
        }
        stmt.close();
        return  toRetList;
    }

    @Override
    public List<Calendrier> getDateFromTo(Date start, Date end) throws SQLException {
        Statement stmt = conn.createStatement();
        String getByIdQuery = "SELECT * FROM CALENDRIER WHERE DATECAL BETWEEN " + UsedFunctions.formatDate(start)
                +" AND " +  UsedFunctions.formatDate(end);
        ResultSet rs = stmt.executeQuery(getByIdQuery);
        List<Calendrier> toRetList = new ArrayList<Calendrier>();
        while (rs.next()) {
            toRetList.add(generateCalendarInstance(rs.getDate("DATECAL")));
        }
        stmt.close();
        return  toRetList;
    }

    
}
