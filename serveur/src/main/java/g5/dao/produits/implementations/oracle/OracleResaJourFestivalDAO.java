/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g5.dao.produits.implementations.oracle;

import g5.dao.produits.interfaces.ResaJourFestivalDAO;
import g5.dbAccess.ConnectToDB;
import g5.models.ResaJourFestival;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Julien Rull
 */
public class OracleResaJourFestivalDAO implements ResaJourFestivalDAO {

    Connection conn = null;
    public OracleResaJourFestivalDAO(){
        conn = ConnectToDB.getConnection();
    }

    private ResaJourFestival generateUneResaJourFestival(ResultSet rs) throws SQLException {
        long idReservation;
        long idJourF;
        Date dateHeureMiseEnPanier;
        long nbrPlace;
        idReservation = rs.getLong("IDRESERVATION");
        idJourF = rs.getLong("IDJOURF");
        dateHeureMiseEnPanier = rs.getDate("DATEHEUREMISEENPANIER");
        nbrPlace = rs.getLong("NBRPLACE");
        return new ResaJourFestival(idReservation,idJourF,dateHeureMiseEnPanier,nbrPlace);
    }

    private List<ResaJourFestival> generatePlusieursResJourFestival(ResultSet rs) throws SQLException {
        long idReservation;
        long idJourF;
        Date dateHeureMiseEnPanier;
        long nbrPlace;
        ArrayList<ResaJourFestival> toRet = new ArrayList<>();
        while (rs.next()){
            idReservation = rs.getLong("IDRESERVATION");
            idJourF = rs.getLong("IDJOURF");
            dateHeureMiseEnPanier = rs.getDate("DATEHEUREMISEENPANIER");
            nbrPlace = rs.getLong("NBRPLACE");
            toRet.add(new ResaJourFestival(idReservation,idJourF,dateHeureMiseEnPanier,nbrPlace));
        }
      return toRet;
    }

    @Override
    public ResaJourFestival getResaJourFestivalById(int idReservation, int idJourFest) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM RESAJOURFESTIVAL WHERE IDRESERVATION = " + idReservation +
                " AND IDJOURF = "+idJourFest;
        ResaJourFestival fToRet = generateUneResaJourFestival(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }

    @Override
    public List<ResaJourFestival> getResaJourFestParReservation(int idReservation) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM RESAJOURFESTIVAL WHERE IDRESERVATION = " + idReservation;
        List<ResaJourFestival> fToRet = generatePlusieursResJourFestival(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }

    @Override
    public List<ResaJourFestival> getResaJourFestParJourFest(int idJourFest) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = "SELECT * FROM RESAJOURFESTIVAL WHERE IDJOURF = "+idJourFest;
        List<ResaJourFestival> fToRet = generatePlusieursResJourFestival(stmt.executeQuery(query));
        stmt.close();
        return fToRet;
    }

    @Override
    public boolean create(ResaJourFestival resaJourFestival) throws SQLException {

            Statement stmt = conn.createStatement();
            String query ="INSERT INTO RESAJOURFESTIVAL (IDRESERVATION,IDJOURF,DATEHEUREMISEENPANIER, NBRPLACE)" +
                    "VALUES ("+resaJourFestival.getIdResevation()+","+resaJourFestival.getIdJourF()
                    +", CURRENT_TIMESTAMP, "+resaJourFestival.getNbrPlace()+")";

            ResultSet rs = stmt.executeQuery(query);
            stmt.close();
            return true;

    }

    @Override
    public boolean delete(ResaJourFestival resaJourFestival) {
        try {
            Statement stmt = conn.createStatement();
            String query = "DELETE FROM RESAJOURFESTIVAL WHERE IDRESERVATION = " + resaJourFestival.getIdResevation() +
                    " AND IDJOURF = "+ resaJourFestival.getIdJourF();
            stmt.executeQuery(query);
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(ResaJourFestival resaJourFestival) {
        return false;
    }
}
