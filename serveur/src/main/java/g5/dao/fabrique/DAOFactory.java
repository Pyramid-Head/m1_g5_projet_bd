package g5.dao.fabrique;

import g5.dao.produits.interfaces.CalendrierDAO;
import g5.dao.produits.interfaces.CompteDAO;
import g5.dao.produits.interfaces.DispoHebergementDAO;
import g5.dao.produits.interfaces.FestivalDAO;
import g5.dao.produits.interfaces.HebergementDAO;
import g5.dao.produits.interfaces.InfrastructureDAO;
import g5.dao.produits.interfaces.JourFestivalDAO;
import g5.dao.produits.interfaces.NoteFestivalDAO;
import g5.dao.produits.interfaces.NoteInfrastructureDAO;
import g5.dao.produits.interfaces.PhotoHebergementDAO;
import g5.dao.produits.interfaces.PhotoInfrastructureDAO;
import g5.dao.produits.interfaces.ResaHebergementDAO;
import g5.dao.produits.interfaces.ResaJourFestivalDAO;
import g5.dao.produits.interfaces.ReservationDAO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Julien Rull
 */
public interface DAOFactory {
    public CalendrierDAO getCalenderDAO();
    public CompteDAO getCompteDAO();
    public DispoHebergementDAO getDispoHebergementDAO();
    public FestivalDAO getFestivalDAO();
    public HebergementDAO getHebergementDAO();
    public InfrastructureDAO getInfrastructureDAO();
    public JourFestivalDAO getJourFestivalDAO();
    public NoteFestivalDAO getNoteFestivalDAO();
    public NoteInfrastructureDAO getNoteInfrastructureDAO();
    public PhotoHebergementDAO getPhotoHebergemenDAO();
    public PhotoInfrastructureDAO getPhotoInfrastructureDAO();
    public ResaHebergementDAO getResaHebergementDAO();
    public ResaJourFestivalDAO getResaJourFestivalDAO();
    public ReservationDAO getReservationDAO();
}
