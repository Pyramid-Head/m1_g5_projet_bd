package g5.dao.fabrique;

import g5.dao.produits.implementations.oracle.OracleCalendrierDAO;
import g5.dao.produits.implementations.oracle.OracleCompteDAO;
import g5.dao.produits.implementations.oracle.OracleDispoHebergementDAO;
import g5.dao.produits.implementations.oracle.OracleFestivalDAO;
import g5.dao.produits.implementations.oracle.OracleHebergementDAO;
import g5.dao.produits.implementations.oracle.OracleInfrastructureDAO;
import g5.dao.produits.implementations.oracle.OracleJourFestivalDAO;
import g5.dao.produits.implementations.oracle.OracleNoteFestivalDAO;
import g5.dao.produits.implementations.oracle.OracleNoteInfrastructureDAO;
import g5.dao.produits.implementations.oracle.OraclePhotoHebergementDAO;
import g5.dao.produits.implementations.oracle.OraclePhotoInfrastructureDAO;
import g5.dao.produits.implementations.oracle.OracleResaHebergementDAO;
import g5.dao.produits.implementations.oracle.OracleResaJourFestivalDAO;
import g5.dao.produits.implementations.oracle.OracleReservationDAO;
import g5.dao.produits.interfaces.CalendrierDAO;
import g5.dao.produits.interfaces.CompteDAO;
import g5.dao.produits.interfaces.DispoHebergementDAO;
import g5.dao.produits.interfaces.FestivalDAO;
import g5.dao.produits.interfaces.HebergementDAO;
import g5.dao.produits.interfaces.InfrastructureDAO;
import g5.dao.produits.interfaces.JourFestivalDAO;
import g5.dao.produits.interfaces.NoteFestivalDAO;
import g5.dao.produits.interfaces.NoteInfrastructureDAO;
import g5.dao.produits.interfaces.PhotoHebergementDAO;
import g5.dao.produits.interfaces.PhotoInfrastructureDAO;
import g5.dao.produits.interfaces.ResaHebergementDAO;
import g5.dao.produits.interfaces.ResaJourFestivalDAO;
import g5.dao.produits.interfaces.ReservationDAO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Julien Rull
 */
public class OracleDAOFactory implements DAOFactory {
    public CalendrierDAO getCalenderDAO(){
        return new OracleCalendrierDAO();
    }
    public CompteDAO getCompteDAO(){
        return new OracleCompteDAO();
    }
    public DispoHebergementDAO getDispoHebergementDAO(){
        return new OracleDispoHebergementDAO();
    }
    public FestivalDAO getFestivalDAO(){
        return new OracleFestivalDAO();
    }
    public HebergementDAO getHebergementDAO(){
        return new OracleHebergementDAO();
    }
    public InfrastructureDAO getInfrastructureDAO(){
        return new OracleInfrastructureDAO();
    }
    public JourFestivalDAO getJourFestivalDAO(){
        return new OracleJourFestivalDAO();
    }
    public NoteFestivalDAO getNoteFestivalDAO(){
        return new OracleNoteFestivalDAO();
    }
    public NoteInfrastructureDAO getNoteInfrastructureDAO(){
        return new OracleNoteInfrastructureDAO();
    }
    public PhotoHebergementDAO getPhotoHebergemenDAO(){
        return new OraclePhotoHebergementDAO();
    }
    public PhotoInfrastructureDAO getPhotoInfrastructureDAO(){
        return new OraclePhotoInfrastructureDAO();
    }
    public ResaHebergementDAO getResaHebergementDAO(){
        return new OracleResaHebergementDAO();
    }
    public ResaJourFestivalDAO getResaJourFestivalDAO(){
        return new OracleResaJourFestivalDAO();
    }
    public ReservationDAO getReservationDAO(){
        return new OracleReservationDAO();
    }
}
