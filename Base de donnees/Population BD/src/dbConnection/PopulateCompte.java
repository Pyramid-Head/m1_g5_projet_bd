package dbConnection;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.sql.Statement;

public class PopulateCompte {
	
	//POPULATE Comptes from its CSV file
		public static void populateComptes() throws SQLException, IOException {
			
			Statement stmt = ConnectToDB.getConnection().createStatement();
			String csvFilePath = "peuplementComptes.csv"; 
			String requeststr="";
			FileInputStream inputFile = new FileInputStream(csvFilePath);
			InputStreamReader ir = new InputStreamReader(inputFile, "UTF-8") ;
			BufferedReader lineReader = new BufferedReader(ir);
			
			String lineText = null;
			//compteur pour creer 50 hebergeur, 50 organizateurs et 51 utilisateur
			int comptchoix = 0;
			
			 while ((lineText = lineReader.readLine()) != null) {
				 comptchoix++;
	             String[] data = lineText.split(",");
	             String mdp = data[0].replaceAll("\'" , "\'\'");
	             String nom = data[1].replaceAll("\'" , "\'\'");
	             String prenom = data[2].replaceAll("\'" , "\'\'");
	             String courriel = data[3].replaceAll("\'" , "\'\'");
	             System.out.println(data[4]);
	             int telephone = Integer.parseInt(data[4]);
	             String fb = data[5].replaceAll("\'" , "\'\'");

	             if(comptchoix<=50) { //CREE ORGANIZATEUR
	            	 requeststr = "INSERT INTO COMPTE (MOTDEPASSE,NOM,PRENOM,COURRIEL,TELEPHONE,FACEBOOK,HEBERGEUR,ORGANISATEUR)"
	 	            		+ " VALUES ('"
	 	           		 + mdp + "' , '"+nom + "','"+prenom+"','"+courriel+"'," +telephone+",'"+fb+"','N','Y')";
	             }
	             else if (comptchoix > 50 && comptchoix <=100) { //CREE HEBERGEUR
	            	 requeststr = "INSERT INTO COMPTE (MOTDEPASSE,NOM,PRENOM,COURRIEL,TELEPHONE,FACEBOOK,HEBERGEUR,ORGANISATEUR)"
		 	            		+ " VALUES ('"
		 	           		 + mdp + "' , '"+nom + "','"+prenom+"','"+courriel+"'," +telephone+",'"+fb+"','Y','N')";
				}
	             else { //CREE UTILISATEUR
	            	 requeststr = "INSERT INTO COMPTE (MOTDEPASSE,NOM,PRENOM,COURRIEL,TELEPHONE,FACEBOOK,HEBERGEUR,ORGANISATEUR)"
		 	            		+ " VALUES ('"
		 	           		 + mdp + "' , '"+nom + "','"+prenom+"','"+courriel+"'," +telephone+",'"+fb+"','N','N')";
				}
	           
	            System.out.println(requeststr);
	            stmt.execute(requeststr);

	             }
			 lineReader.close();
			 stmt.close();
		}
		
		
		/* UNUSED METHOD BECAUSE WE REMOVED SUBCLASSES!
		public static void populateSubClasses() throws SQLException {
			Statement stmt = ConnectToDB.getConnection().createStatement();
			String requeststr;
			for (int i=1; i<=50 ; i++) {
				requeststr = "INSERT INTO UTILISATEUR VALUES ("+i+")";
				System.out.println(requeststr);
				 stmt.execute(requeststr);
			}
			for (int i=51; i<=100 ; i++) {
				requeststr = "INSERT INTO ORGANISATEUR VALUES ("+i+")";
				System.out.println(requeststr);
				 stmt.execute(requeststr);
			}
			for (int i=101; i<=150 ; i++) {
				requeststr = "INSERT INTO HEBERGEUR VALUES ("+i+")";
				System.out.println(requeststr);
				 stmt.execute(requeststr);
			}
			stmt.close();
			
		}*/
		
		
}
