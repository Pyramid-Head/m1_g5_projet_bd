package dbConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PopulateHeberFull {
	

	private static List<LocalDate> getDatesBetween(
			  LocalDate startDate, LocalDate endDate) { 
			  
			    long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate); 
			    return IntStream.iterate(0, i -> i + 1)
			      .limit(numOfDaysBetween)
			      .mapToObj(i -> startDate.plusDays(i))
			      .collect(Collectors.toList()); 
			}

	public static void aller() throws SQLException {
		
		
		int counter = 0;
		Connection conn = ConnectToDB.getConnection();
		
		Statement stmt = conn.createStatement();
		
		/*
		//GETTING THE dates  and saving them in arraylist
				ResultSet rs1D = stmt.executeQuery("select DATECAL FROM CALENDRIER");	
				ArrayList<Date> datesID = new ArrayList<Date>();
				while (rs1D.next()) {
					// retrieve and print the values for the current row
					datesID.add(rs1D.getDate("DATECAL"));
				}
				System.out.println("GOT THE DATES");*/
		
		
		ArrayList<LocalDate> dateList = (ArrayList<LocalDate>) getDatesBetween(LocalDate.of(2018, 01, 01),
				LocalDate.of(2020, 01, 01));
		 //zone
		 ZoneId defaultZoneId = ZoneId.systemDefault();
		 java.util.Date date ;
		 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		
				
		
		//getting depts
				ResultSet rs2D = stmt.executeQuery("SELECT NUMDEPARTEMENT FROM DEPARTEMENT");	
				ArrayList<String> depts = new ArrayList<String>();
				while (rs2D.next()) {
					// retrieve and print the values for the current row
					depts.add(rs2D.getString("NUMDEPARTEMENT"));
				}
				System.out.println("GOT THE DEPTS");
				
				
			
				ArrayList<Integer> hebgDesDep = new ArrayList<Integer>();
				
				for (int i = 0 ; i<depts.size();i++) {
					//getting 1 hebergement par dept
					
					/*SELECT * FROM (SELECT h.*, ROW_NUMBER() over (order by h.IDHEBERGEMENT) R FROM HEBERGEMENT h, INFRASTRUCTURE i
 WHERE  i.IDINFRASTRUCTURE=h.IDINFRASTRUCTURE AND i.IDINFRASTRUCTURE='1' )
 WHERE R BETWEEN 0 AND 1;*/
					
					
					String query="SELECT * FROM (SELECT h.*, ROW_NUMBER() over (order by h.IDHEBERGEMENT) R "
							+ "FROM HEBERGEMENT h, INFRASTRUCTURE i "
							+ "WHERE  i.IDINFRASTRUCTURE=h.IDINFRASTRUCTURE AND i.NUMDEPARTEMENT='"
							+depts.get(i)+"' ) "
							+ " WHERE R BETWEEN 0 AND 1";
					
					ResultSet rs1 = stmt.executeQuery(query);
					
					while (rs1.next()) {
						// retrieve and print the values for the current row
						hebgDesDep.add(rs1.getInt("IDHEBERGEMENT"));
					}
					System.out.println("got the hebergemnts");					
					
				}
				
	
				
				
				for (int heb = 0; heb < hebgDesDep.size();heb++) {
					int hebergementId = hebgDesDep.get(heb);
					
					//deleting all the present dispos
					 stmt.executeQuery("DELETE FROM DISPOHEBERGEMENT WHERE IDHEBERGEMENT = "+ hebergementId);
					System.out.println("DELETED");
					
					 //adding for all dates!
					 for (int i = 0; i < dateList.size(); i++) {
							date = Date.from(dateList.get(i).atStartOfDay(defaultZoneId).toInstant());
							
							String toAdd = "INSERT INTO DISPOHEBERGEMENT (IDHEBERGEMENT, DATECAL)"
									+ "VALUES ( "+ hebergementId +" , "+
									"TO_DATE('"+df.format(date)+ "' , 'dd/mm/yyyy'))";
							stmt.execute(toAdd);
							System.out.println(toAdd);
							
					 }
					 
				}
				
				
		stmt.close();
	}
	
	public static void main(String[] args) {
		try {
			PopulateHeberFull.aller();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
