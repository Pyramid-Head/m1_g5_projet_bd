package dbConnection;

import java.sql.*;

//Using Singleton pattern
public class ConnectToDB {

	private static final String CONN_URL = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";
	
	//CREDENTIALS
	private static final String USER = "himidiz";
	private static final String PASSWD = "ilMA1312";

	private static Connection conn = null; 
	
	
	private ConnectToDB() {
	//	create connection
  	    try {
  		  // Enregistrement du driver Oracle
  	  	    System.out.print("Loading Oracle driver... "); 
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
	  	    System.out.println("loaded");

	  	  // Etablissement de la connection
	  	    System.out.print("Connecting to the database... "); 
	 	    conn = DriverManager.getConnection(CONN_URL,USER,PASSWD);
	   	    System.out.println("connected");
	  	    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			 System.err.println("failed");
             System.out.println("Affichage de la pile d'erreur");
 	          e.printStackTrace(System.err);
             System.out.println("Affichage du message d'erreur");
             System.out.println(e.getMessage());
             System.out.println("Affichage du code d'erreur");
 	          System.out.println(e.getErrorCode());	    
		} 
  	    
	}
	
	
	public static Connection getConnection() {
		if(conn == null) {
			new ConnectToDB();
		}
		return conn;
	}
	
	public static void closeConnection() throws SQLException {
		if (conn != null) {
			conn.close(); 
		}else {
			throw new NullPointerException();
		}
	}

	
}
