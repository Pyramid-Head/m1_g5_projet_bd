package dbConnection;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PopulateFestival {

	
	
	
	public static void populateFestivalTable() throws SQLException, NumberFormatException, IOException, ParseException {
		 Connection conn = ConnectToDB.getConnection();
		 
		Statement stmt = conn.createStatement();
		
		
		//GETTING THE ORGANISORS IDs and saving them in arraylist
		ResultSet rs = stmt.executeQuery("select IDCOMPTE FROM COMPTE WHERE ORGANISATEUR = 'Y'");	
		ArrayList<Integer> idComptesOrg = new ArrayList<Integer>();
		
		while (rs.next()) {
			// retrieve and print the values for the current row
			idComptesOrg.add(rs.getInt("IDCOMPTE"));
		}

		int i = 0;
		
		//Getting festivals info from csv file and saving them
		
		String csvFilePath = "peuplementfestival.csv"; 
		String requeststr="";
		FileInputStream inputFile = new FileInputStream(csvFilePath);
		InputStreamReader ir = new InputStreamReader(inputFile, "UTF-8") ;
		BufferedReader lineReader = new BufferedReader(ir);
		
		String lineText = null;
		//depasser 1ere ligne
		lineReader.readLine();
		 while ((lineText = lineReader.readLine()) != null) {
			 i++;
			 System.out.println(i);
             String[] data = lineText.split(",");
             String numDep = data[0];
             String nom = data[1].replaceAll("\'" , "\'\'");
             nom = nom.replace('|', ',');
             String webSite = data[2].replace('|', ',');
             String complementDom = data[3].replaceAll("\'" , "\'\'");
             complementDom = complementDom.replace('|', ',');
             String comments = data[4].replace('|', ',');
             comments = comments.replaceAll("\'" , "\'\'");
             int numEdition = Integer.parseInt(data[5]);
             
            Date dateCreation = (Date) new SimpleDateFormat("yyyy-MM-dd").parse(data[6]);
            
             
            Date dateDebut = (Date) new SimpleDateFormat("yyyy-MM-dd").parse(data[7]);
             
            Date dateFin =(Date) new SimpleDateFormat("yyyy-MM-dd").parse(data[8]);
            
            String coord = data[9].replace('|', ',');
            int montantCaisse = Integer.parseInt(data[10]);
            String typeFest = data[11].replace('|', ',');
            typeFest= typeFest.replaceAll("\'" , "\'\'");
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            //getting random organizer id
            int rando = (int)((Math.random()*idComptesOrg.size()));
            System.out.println(rando);
            int organizerID = idComptesOrg.get(rando);
           
            
            requeststr = "INSERT INTO FESTIVAL (NUMDEPARTEMENT,IDCOMPTE,NOM,SITEWEB,COMPLEMENTDOMAINE,"
            		+ "COMMENTAIRES,NUMEROEDITION,DATEDECREATION,DATEDEDEBUT,DATEDEFIN,COORDONNEESGPS,MONTANTENCAISSE,TYPEFEST )"
            		+ " VALUES ('"+numDep +"'," + organizerID + ",'" + nom+"','"+webSite+"','"+complementDom+"','"+comments+"'," + numEdition
            		+","+ "TO_DATE('"+df.format(dateCreation)+ "', 'dd/mm/yyyy') , " +
            		"TO_DATE('"+df.format(dateDebut)+ "', 'dd/mm/yyyy') , " +
            		"TO_DATE('"+df.format(dateFin)+ "', 'dd/mm/yyyy') , '" +
            		coord +"'," + montantCaisse + ",'"+typeFest + "')";
            		
            System.out.println(requeststr);
           stmt.execute(requeststr);
            if (i == 3136) {
            	break;
            }

             }
		 lineReader.close();
		 stmt.close();
		
		
	}
	
	
	
	
	
	public static void populateJourFest() throws SQLException {
		
		
		 Connection conn = ConnectToDB.getConnection();
		 
			Statement stmt = conn.createStatement();
			Statement stmtW1 = conn.createStatement();
			Statement stmtW2 = conn.createStatement();
			Statement stmtW3 = conn.createStatement();
			
			//GETTING THE Festival IDs and starting date
			ResultSet rs = stmt.executeQuery("select IDFESTIVAL,DATEDEDEBUT FROM FESTIVAL");	
			
			while (rs.next()) {
			

				// retrieve and print the values for the current row
				String requeststr;
				
				//making fest with 2 categories or 1?
				double choix = Math.random();
				
				if(choix <= 0.5) { //faire 2 categories
					//cat1
					 requeststr = "INSERT INTO JOURFESTIVAL "
			            		+ "(IDFESTIVAL,DATEJF,PRIX,PLACESLIBRES,TYPEPLACE) "+
			            		"VALUES ("+ rs.getInt("IDFESTIVAL") + " , TO_DATE('"+rs.getDate("DATEDEDEBUT")+"' , 'yyyy-mm-dd') , "
			            		+ Math.random()*100 + " , " 
			            		+ (int) (Math.random()*1000)
			            		+ " , 'CAT1')";
					 
					 stmtW1.execute(requeststr);
					 System.out.println(requeststr);
					 //cat 2
					 requeststr = "INSERT INTO JOURFESTIVAL "
			            		+ "(IDFESTIVAL,DATEJF,PRIX,PLACESLIBRES,TYPEPLACE) "+
			            		"VALUES ("+ rs.getInt("IDFESTIVAL") + " , TO_DATE('"+rs.getDate("DATEDEDEBUT")+"' , 'yyyy-mm-dd') , "
			            		+ Math.random()*100 + " , " + (int) (Math.random()*1000) + " , 'CAT2')";
					 System.out.println(requeststr);
					 stmtW2.execute(requeststr);
					 System.out.println(requeststr);
				}
				else { //noCat
					 requeststr = "INSERT INTO JOURFESTIVAL "
			            		+ "(IDFESTIVAL,DATEJF,PRIX,PLACESLIBRES,TYPEPLACE) "+
			            		"VALUES ("+ rs.getInt("IDFESTIVAL") + " , TO_DATE('"+rs.getDate("DATEDEDEBUT")+"' , 'yyyy-mm-dd') , "
			            		+ Math.random()*100 + " , " + (int) (Math.random()*1000) + " , 'SANSCAT')";
			            		
					 stmtW3.execute(requeststr);
					 System.out.println(requeststr);
					
				}
				
				
			}
			stmt.close();
			stmtW1.close();
			stmtW2.close();
			stmtW3.close();
		
		
	}
	
}
