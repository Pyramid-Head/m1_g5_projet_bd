package dbConnection;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PopulateInfrastructure {

	
	
	
	
	public static void populateInfrastructTable() throws SQLException, IOException {
		Connection conn = ConnectToDB.getConnection();
		
		Statement stmt = conn.createStatement();
		
		
		//GETTING THE hebergeurs IDs and saving them in arraylist
		ResultSet rs = stmt.executeQuery("select IDCOMPTE FROM COMPTE WHERE HEBERGEUR = 'Y'");	
		ArrayList<Integer> idComptesHeberg = new ArrayList<Integer>();
		
		while (rs.next()) {
			// retrieve and print the values for the current row
			idComptesHeberg.add(rs.getInt("IDCOMPTE"));
		}
		
		//Getting festivals info from csv file and saving them
		
				String csvFilePath = "peuplementInfrastructure.csv"; 
				String requeststr="";
				FileInputStream inputFile = new FileInputStream(csvFilePath);
				InputStreamReader ir = new InputStreamReader(inputFile, "UTF-8") ;
				BufferedReader lineReader = new BufferedReader(ir);
				
				String lineText = null;
				//depasser 1ere ligne
				lineReader.readLine();
				int i = 0;
			
				 while ((lineText = lineReader.readLine()) != null) {
					 
					 
					 String[] data = lineText.split(",");
					 String numDept = cleanString(data[0]);
					 String nomInf = cleanString(data[1]);
					 String classement = cleanString(data[2]);
					 String categorie = cleanString(data[3]);
					 String telephonne = data[4];
					 String webSite = cleanString(data[5]);
					 String mail = cleanString(data[6]);
					 String location = cleanString(data[7]);
					 int note = Integer.parseInt(data[8]);
					// String adress = cleanString(data[9]);
					 int montantCaisse =  Integer.parseInt(data[10]);
					 String typeInfra = cleanString(data[11]);

					 //choosing the hebergeur randomly
			            int rando = (int)((Math.random()*idComptesHeberg.size()));
			            System.out.println(rando);
			            int hebergeurID = idComptesHeberg.get(rando);
					 
					 requeststr = "INSERT INTO INFRASTRUCTURE (NUMDEPARTEMENT,IDCOMPTE,NOM,CLASSEMENT,CATEGORIE,"
					 		+ "TELEPHONE,SITEINTERNET,COURRIEL,COORDONNEESGPS,NOTE,MONTANTENCAISSE,TYPEINFRA) "
					 		+ "VALUES ('" + numDept +"' , "+hebergeurID + ", '"+nomInf+"', '"+classement+"','"+categorie
					 		+"','"+telephonne+"','" + webSite + "','"+mail+"','"+location+"',"+note+","+montantCaisse
					 		+",'"+typeInfra+"')";
					 
					 System.out.println(i);
					 i++;
					 System.out.println(requeststr);
					stmt.execute(requeststr);
					 
				 }
		
		stmt.close();
		lineReader.close();
		
		
	}
	
	
	
	
	
	
	
	public static void populateHebergements() throws SQLException {
		int compteurDeRequettes = 0;
		
		Connection conn = ConnectToDB.getConnection();
		
		Statement stmt = conn.createStatement();
		
		
		//GETTING THE infrastructure Types and saving them in arraylist
		ResultSet rs = stmt.executeQuery("SELECT DISTINCT TYPEINFRA FROM INFRASTRUCTURE");	
		ArrayList<String> infraTypes = new ArrayList<String>();
		
		while (rs.next()) {
			// retrieve and print the values for the current row
			infraTypes.add(rs.getString("TYPEINFRA").trim());
		}
		
		//we have the types now... for each type we should create its hebergs.
		
		
		String typeHeb = "";
		for (int i = 0; i < infraTypes.size(); i++) {
			typeHeb = infraTypes.get(i);
			
			//let's get the id's of the infrastructure having that type
			
			ResultSet rs1 = stmt.executeQuery("SELECT IDINFRASTRUCTURE FROM INFRASTRUCTURE WHERE TYPEINFRA = '"+typeHeb+"'");	
			ArrayList<Integer> infraIDs = new ArrayList<Integer>();
			
			while (rs1.next()) {
				// retrieve and print the values for the current row
				infraIDs.add(rs1.getInt("IDINFRASTRUCTURE"));
			}
			
			
			
			
			/*RESIDENCE DE TOURISME     =    donne des residences    
			PARC RESIDENTIEL DE LOISIRS                       = residences
                                 
			CAMPING                                        = emplacements       
			HOTEL                                                       = chambre simple | double | familiale
			VILLAGE DE VACANCES                                               		= chambre individuel | dortoir
			*/
			switch (typeHeb) {
			case "RESIDENCE DE TOURISME":
			case "PARC RESIDENTIEL DE LOISIRS":
				
				for (int j = 0; j < infraIDs.size(); j++) {
					//capacitee and tarifforfaitaire
					String reqst = "INSERT INTO HEBERGEMENT (IDINFRASTRUCTURE,TYPEHEBERG,"
							+ "CAPACITE,TARIFFORFAITAIRE)"
							+ " VALUES ("+infraIDs.get(j)+", 'Residence',"+ getRandomIntdix() +","
							+ getRandomPrixMille()+")";	
					compteurDeRequettes++;
					System.out.println(compteurDeRequettes);
					System.out.println(reqst);
					stmt.execute(reqst);
				}
				
				
				break ;
				
			case "CAMPING":
				for (int j = 0; j < infraIDs.size(); j++) {
				//tarif forfaitaire
				String reqst = "INSERT INTO HEBERGEMENT (IDINFRASTRUCTURE,TYPEHEBERG,"
						+ "TARIFFORFAITAIRE)"
						+ " VALUES ("+infraIDs.get(j)+", 'Emplacement',"+ getRandomIntCent() +")";		
				compteurDeRequettes++;
				System.out.println(compteurDeRequettes);
				System.out.println(reqst);
				stmt.execute(reqst);
				}
				break ;

			case "HOTEL":
				for (int j = 0; j < infraIDs.size(); j++) {
					
					//setting random chambre type
					Double typeran = getRandomPrixdix();
					String reqst;
					if(typeran <=3.3) { //chambre simple
						
						//nb place adulte- enfant + prix des 2
						 reqst = "INSERT INTO HEBERGEMENT (IDINFRASTRUCTURE,TYPEHEBERG,"
								+ "NBPLACESADULTES,TARIFADULTE)"+ 
								 " VALUES ("+infraIDs.get(j)+", 'Chambre Simple', 1 ,"+getRandomPrixCent()+")";		
						
					}else if (typeran>3.3 && typeran<6.7) {//chambre double
						//nb place adulte- enfant + prix des 2
						
					 reqst = "INSERT INTO HEBERGEMENT (IDINFRASTRUCTURE,TYPEHEBERG,"
								+ "NBPLACESADULTES,TARIFADULTE)"
								+ " VALUES ("+infraIDs.get(j)+", 'Chambre Double', 2 ,"
								+getRandomPrixCent()+")";		
						
						
					}else {//chambre fam
						//nb place adulte- enfant + prix des 2
						reqst = "INSERT INTO HEBERGEMENT (IDINFRASTRUCTURE,TYPEHEBERG,"
								+ "NBPLACESADULTES,NBPLACESENFANTS,TARIFENFANT,TARIFADULTE)"
								+ " VALUES ("+infraIDs.get(j)+", 'Chambre Familiale',2,2,"
								+getRandomPrixCent()+","+getRandomPrixCent()+")";		
					}
					
				compteurDeRequettes++;
				System.out.println(compteurDeRequettes);
				System.out.println(reqst);
				stmt.execute(reqst);

				}
				break ;

			case "VILLAGE DE VACANCES":
				for (int j = 0; j < infraIDs.size(); j++) {
					
					//setting random chambre type
					Double typeran = getRandomPrixdix();
					String reqst;
					if(typeran<5) { //chambre individuel
						//nb place adulte- enfant + prix des 2
						 reqst = "INSERT INTO HEBERGEMENT (IDINFRASTRUCTURE,TYPEHEBERG,"
								+ "NBPLACESADULTES,TARIFADULTE)"
								+ " VALUES ("+infraIDs.get(j)+", 'Chambre Individuelle',1,"
								+getRandomPrixCent()+")";	
						
					}else {//dortoir
						//nb place adulte- enfant + prix des 2
						 reqst = "INSERT INTO HEBERGEMENT (IDINFRASTRUCTURE,TYPEHEBERG,"
								+ "NBPLACESADULTES,NBPLACESENFANTS,TARIFENFANT,TARIFADULTE)"
								+ " VALUES ("+infraIDs.get(j)+", 'Dortoir',"+ getRandomIntdix() +","
								+getRandomIntdix()+","+getRandomPrixCent()+","+getRandomPrixCent()+")";	
					}
					
					
					compteurDeRequettes++;
					System.out.println(compteurDeRequettes);
					System.out.println(reqst);
					stmt.execute(reqst);
				}
				break ;
			default:
				throw new IllegalArgumentException("Unexpected value: " + typeHeb);
			}
			
			
			                                                                          
			
			
		}
		
		
		
		
	}
	
	
	
	public static  void linkHebergCalendar() throws SQLException {
		int counter = 0;
		Connection conn = ConnectToDB.getConnection();
		
		Statement stmt = conn.createStatement();
		
		
		//GETTING THE Departments IDs and saving them in arraylist
		ResultSet rs = stmt.executeQuery("select NUMDEPARTEMENT FROM DEPARTEMENT");	
		ArrayList<String> deptId = new ArrayList<String>();
		while (rs.next()) {
			// retrieve and print the values for the current row
			deptId.add(rs.getString("NUMDEPARTEMENT"));
		}
		
		//GETTING THE dates  and saving them in arraylist
				ResultSet rs1D = stmt.executeQuery("select DATECAL FROM CALENDRIER");	
				ArrayList<String> datesID = new ArrayList<String>();
				while (rs1D.next()) {
					// retrieve and print the values for the current row
					datesID.add(rs1D.getString("DATECAL").substring(0,10));
				}
				
		
		
		for (int deptnb = 0; deptnb < deptId.size(); deptnb++) {
			String dept = deptId.get(deptnb);
			
			//getting heberg. of each department
			ResultSet rs1 = stmt.executeQuery("select IDHEBERGEMENT FROM HEBERGEMENT H WHERE " + 
					"(SELECT NUMDEPARTEMENT FROM INFRASTRUCTURE I WHERE I.IDINFRASTRUCTURE = H.IDINFRASTRUCTURE) = '"
					+dept+ "'");	
			ArrayList<String> hebgduDep = new ArrayList<String>();
			while (rs1.next()) {
				// retrieve and print the values for the current row
				hebgduDep.add(rs1.getString("IDHEBERGEMENT"));
			}
			
			
			//iterating over the dates and creating link to a heberg. in this department (so we will have
			//a department available for each date in one dept)
			if(hebgduDep.size()>0) {
				for (int i = 0; i < datesID.size(); i++) {
					String dateId = datesID.get(i);
					
					//CHOSING RANDOM DEPARTMENT
					int ranDept = (int) (Math.random()*hebgduDep.size());
					
					
					String connectingQuery = "INSERT INTO DISPOHEBERGEMENT (IDHEBERGEMENT,DATECAL) VALUES ("
							+hebgduDep.get(ranDept)+ ",TO_DATE('"+dateId+"' , 'yyyy-mm-dd'))";
					
					System.out.println(connectingQuery);
					System.out.println(counter);
					counter++;
					stmt.execute(connectingQuery);
				
				}
			}
		
		}
		stmt.close();
		
	}
	
	
	
	
	
	private static String cleanString(String s) {
		String ss = s.replaceAll("\'" , "\'\'");
		return ss.replace('|', ',');
	}
	private static int getRandomIntCent() {
		return (int) (Math.random()*100);
	}
	private static int getRandomIntdix() {
		return (int) (Math.random()*10);
	}
	private static int getRandomIntMille() {
		return (int) (Math.random()*1000);
	}
	
	private static double getRandomPrixdix() {
		return (int) (Math.random()*10);
	}
	private static double getRandomPrixCent() {
		return (int) (Math.random()*100);
	}
	private static double getRandomPrixMille() {
		return (int) (Math.random()*1000);
	}
	
}
