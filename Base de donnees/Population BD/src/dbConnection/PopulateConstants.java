package dbConnection;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class PopulateConstants {

	
	//CREATE CALENDAR
	public static void populateCalendar() throws SQLException{
		ArrayList<LocalDate> dateList = (ArrayList<LocalDate>) getDatesBetween(LocalDate.of(2018, 01, 01),
				LocalDate.of(2020, 01, 01));
		//creating statement
		Statement stmt = ConnectToDB.getConnection().createStatement();
		 String requeststr="";
		 //zone
		 ZoneId defaultZoneId = ZoneId.systemDefault();
		 java.util.Date date ;
		 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		//adding to DB
		 
		for (int i = 0; i < dateList.size(); i++) {
			date = Date.from(dateList.get(i).atStartOfDay(defaultZoneId).toInstant());
			
			requeststr="INSERT INTO CALENDRIER (DATECAL) "
			          + "VALUES (TO_DATE('"+df.format(date)+ "', 'dd/mm/yyyy')" +")";	
			stmt.execute(requeststr);
			System.out.println("added date "+i);
			}
		stmt.close();
		
	}
	
	private static List<LocalDate> getDatesBetween(
			  LocalDate startDate, LocalDate endDate) { 
			  
			    long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate); 
			    return IntStream.iterate(0, i -> i + 1)
			      .limit(numOfDaysBetween)
			      .mapToObj(i -> startDate.plusDays(i))
			      .collect(Collectors.toList()); 
			}
	
	
	
	//POPULATE DEPARTMENTS from its CSV file
	public static void populateDepartments() throws SQLException, IOException {
		
		Statement stmt = ConnectToDB.getConnection().createStatement();
		String csvFilePath = "departementBD.csv"; 
		String requeststr="";
		FileInputStream inputFile = new FileInputStream(csvFilePath);
		InputStreamReader ir = new InputStreamReader(inputFile, "UTF-8") ;
		BufferedReader lineReader = new BufferedReader(ir);
		
		String lineText = null;
		
		 while ((lineText = lineReader.readLine()) != null) {
             String[] data = lineText.split(",");
             String numDept = data[0];
             String nomDept = data[1];
             
             //adding appostrophee
             nomDept = nomDept.replaceAll("\'" , "\'\'");
             
            requeststr = "INSERT INTO DEPARTEMENT (NUMDEPARTEMENT , NOMDEPARTEMENT) VALUES ('"
           		 + numDept + "' , '"+nomDept+"')";
            System.out.println(requeststr);
           stmt.execute(requeststr);

             }
		 lineReader.close();
		 stmt.close();
	}
	
	
	
	
	
}
