import { Component, OnInit } from '@angular/core';
import {auth, User} from 'firebase';

import {DataTransfertService} from '../data-transfert.service';
import {HttpResponse} from '@angular/common/http';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: User;
  login: string;

  constructor(private afAuth: AngularFireAuth, private dts: DataTransfertService) {
    afAuth.user.subscribe( u => {
      this.user = u;
      if (this.user) {
        // this.dts.idClient = this.user.uid;
        const c: Promise<HttpResponse<string>> = this.dts.GET('/api/authentification?mail=' + this.user.email,
          {});
        c.then((resolve) => {
          this.login = resolve.body;
          this.dts.idCompte = this.login;
        });
        this.dts.isLogged = true;
      }
    } );
  }

  async loginGoogle($event: MouseEvent) {
    await this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }

  async deLoginGoogle() {
    await this.afAuth.auth.signOut();
    await this.afAuth.auth.app.delete();
    this.dts.isLogged = this.isConnected;
    location.reload();
  }

  ngOnInit() {
  }

  get isConnected(): boolean {
    return !!this.user;
  }
}
