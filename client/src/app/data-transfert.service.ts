import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {FestivalResponse, SearchFestivalsResponse} from './data/festival-data';
import {InfrastructureResponse, SearchInfrastructuresResponse} from './data/infrastructure-data';
import {HebergementResponse} from './data/hebergement-data';

@Injectable({
  providedIn: 'root'
})
export class DataTransfertService {

  startDate: Date;
  isLogged: boolean;
  nbEnfants: number;
  nbAdultes: number;

  idReservation: string;
  idCompte: string;

  // festivals
  pageFestivalCourante: number;
  festivalsAffiches: FestivalResponse[] = [];
  festivalsAffichesRecommandes: FestivalResponse[] = [];
  aAfficherFestival: FestivalResponse[] = [];
  stringRequeteFest: string;

  // infrastructures
  pageInfrastructureCourante: number;
  infrastructuresAffichees: InfrastructureResponse[] = [];
  infrastructuresAfficheesRecommandees: InfrastructureResponse[] = [];
  aAfficherInfra: InfrastructureResponse[] = [];

  stringRequeteInfra: string;
  festCommande: boolean;
  infraCommande: boolean;
  // hebergements
  hebergementsAffiches: HebergementResponse[] = [];


  constructor(private http: HttpClient) {
    this.startDate = new Date(2018, 0, 1) ;
    this.nbEnfants = 0;
    this.nbAdultes = 1;
    this.idReservation = '';
    this.idCompte = '';

    this.pageFestivalCourante = 1;
    this.pageInfrastructureCourante = 1;
    this.stringRequeteInfra = '';

    this.infrastructuresAfficheesRecommandees = [];
    this.festivalsAffichesRecommandes = [];

    this.festCommande = false;
    this.infraCommande = false;

    this.isLogged = false;
  }


  changerDate(date: string) {
    if (date !== undefined || date !== '') {
      const mois: string = this.changeMois(date.substring(3, 7).trim());
      const jour: string = date.substring(7, 10).trim();
      const annee: string = date.substring(10, 15).trim();
      return jour + '/' + mois + '/' + annee;
    } else {
      return '';
    }
  }

  changeMois(mois: string) {
    switch (mois) {
      case 'Jan' : return '01';
      case 'Feb' : return '02';
      case 'Mar' : return '03';
      case 'Apr' : return '04';
      case 'May' : return '05';
      case 'Jun' : return '06';
      case 'Jul' : return '07';
      case 'Aug' : return '08';
      case 'Sep' : return '09';
      case 'Oct' : return '10';
      case 'Nov' : return '11';
      case 'Dec' : return '12';
    }
  }

  ajouter(type: string, ajout: boolean) {
    if (ajout) {
      if (type === 'adulte') {
        this.nbAdultes++;
      } else {
        this.nbEnfants++;
      }
    } else {
      if (type === 'adulte' && this.nbAdultes > 1 ) {
        this.nbAdultes--;
      } else if (type === 'enfant' && this.nbEnfants > 0 ) {
        this.nbEnfants--;
      }
    }
  }

  POST(url, params: {[key: string]: string}): Promise<HttpResponse<string>> {
    const P = new HttpParams( {fromObject: params} );
    console.log(params, P, P.toString());
    return this.http.post( url, P, {
      observe: 'response',
      responseType: 'text',
      headers: {'content-type': 'multipart/form-data'}
    }).toPromise();
  }

  GET(url, params: {[key: string]: string}): Promise<HttpResponse<string>> {
    console.log('debutGET');
    const P = new HttpParams( {fromObject: params} );
    console.log('get');
    return this.http.get( url, {
      observe: 'response',
      responseType: 'text',
      headers: {'content-type': 'multipart/form-data'}
    }).toPromise();
  }
}
