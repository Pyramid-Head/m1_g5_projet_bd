import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';

// angular material
import {
  MAT_DIALOG_DATA,
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule, MatDialogModule, MatDialogRef,
  MatFormFieldModule,
  MatIconModule,
  MatListModule, MatMenuModule,
  MatNativeDateModule, MatProgressSpinnerModule, MatRadioModule, MatSidenavModule,
  MatTreeModule
} from '@angular/material';

// authentification firebase
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import { PageAccueilComponent } from './page-accueil/page-accueil.component';
import { ListeFestivalsComponent } from './composantsFestival/liste-festivals/liste-festivals.component';
import { ListeInfrastructuresComponent } from './composantsInfrastructure/liste-infrastructures/liste-infrastructures.component';
import { FestivalComponent } from './composantsFestival/festival/festival.component';
import { InfrastructureComponent } from './composantsInfrastructure/infrastructure/infrastructure.component';
import { ListeHebergementsComponent } from './composantsHebergement/liste-hebergements/liste-hebergements.component';
import { HebergementComponent } from './composantsHebergement/hebergement/hebergement.component';
import { FormulaireRechercheInfrastructureComponent } from './composantsInfrastructure/formulaire-recherche-infrastructure/formulaire-recherche-infrastructure.component';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import { FormulaireRechercheFestivalComponent } from './composantsFestival/formulaire-recherche-festival/formulaire-recherche-festival.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SideBarFestivalComponent } from './composantsFestival/side-bar-festival/side-bar-festival.component';
import { SideBarInfrastructureComponent } from './composantsInfrastructure/side-bar-infrastructure/side-bar-infrastructure.component';
import { FormulaireTriFestivalComponent } from './composantsFestival/formulaire-tri-festival/formulaire-tri-festival.component';
import { FormulaireTriInfrastructureComponent } from './composantsInfrastructure/formulaire-tri-infrastructure/formulaire-tri-infrastructure.component';
import { PanierComponent } from './panier/panier/panier.component';
import { MesFestivalsComponent } from './menu/mes-festivals/mes-festivals.component';
import { MesHebergementsComponent } from './menu/mes-hebergements/mes-hebergements.component';
import { DetailsFestivalComponent } from './composantsFestival/details-festival/details-festival.component';
import { InfoNonImplementeComponent } from './popup/info-non-implemente/info-non-implemente.component';
import {RouterModule, Routes} from '@angular/router';
import { ConfirmationAjoutPanierComponent } from './popup/confirmation-ajout-panier/confirmation-ajout-panier.component';
import { ErreurPasDeCorrespondanceComponent } from './popup/erreurs/erreur-pas-de-correspondance/erreur-pas-de-correspondance.component';
import { DemandeContinuerHebergComponent } from './popup/demande-continuer-heberg/demande-continuer-heberg.component';
import { DemandeContinuerFestComponent } from './popup/demande-continuer-fest/demande-continuer-fest.component';
import { RecapFestComponent } from './panier/recap-fest/recap-fest.component';
import { RecapHebComponent } from './panier/recap-heb/recap-heb.component';
import { ErreurPlusDePlacesComponent } from './popup/erreurs/erreur-plus-de-places/erreur-plus-de-places.component';
import { PanierVideComponent } from './popup/panier-vide/panier-vide.component';
import { NonConnecteComponent } from './popup/non-connecte/non-connecte.component';

const routes: Routes = [
  { path: '', component: PageAccueilComponent},
  { path: 'listeFestivals', component: ListeFestivalsComponent},
  { path: 'listeInfrastructures', component: ListeInfrastructuresComponent},
  { path: 'listeHebergements', component: ListeHebergementsComponent},
  { path: 'mesFestivals', component: MesFestivalsComponent},
  { path: 'mesHebergements', component: MesHebergementsComponent},
  { path: 'monPanier', component: PanierComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageAccueilComponent,
    ListeFestivalsComponent,
    ListeInfrastructuresComponent,
    FestivalComponent,
    InfrastructureComponent,
    ListeHebergementsComponent,
    HebergementComponent,
    FormulaireRechercheInfrastructureComponent,
    FormulaireRechercheFestivalComponent,
    SideBarFestivalComponent,
    SideBarInfrastructureComponent,
    FormulaireTriFestivalComponent,
    FormulaireTriInfrastructureComponent,
    PanierComponent,
    MesFestivalsComponent,
    MesHebergementsComponent,
    DetailsFestivalComponent,
    InfoNonImplementeComponent,
    ConfirmationAjoutPanierComponent,
    ErreurPasDeCorrespondanceComponent,
    DemandeContinuerHebergComponent,
    DemandeContinuerFestComponent,
    RecapFestComponent,
    RecapHebComponent,
    ErreurPlusDePlacesComponent,
    PanierVideComponent,
    NonConnecteComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatMenuModule,
    MatDialogModule,
    MatSidenavModule,
    MatListModule,
    MatTreeModule,
    MatRadioModule,
    MatCardModule,
    MatIconModule,
    MatDatepickerModule,
    MatButtonModule,
    MatFormFieldModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatProgressSpinnerModule,
    AngularFireModule.initializeApp(environment),
    RouterModule.forRoot(routes),
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    DetailsFestivalComponent,
    InfoNonImplementeComponent,
    ConfirmationAjoutPanierComponent,
    ErreurPasDeCorrespondanceComponent,
    DemandeContinuerFestComponent,
    PanierVideComponent,
    DemandeContinuerHebergComponent,
    ErreurPlusDePlacesComponent,
    NonConnecteComponent]
})
export class AppModule { }
