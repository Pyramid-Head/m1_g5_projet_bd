import {Component, Input, OnInit} from '@angular/core';
import {FestivalResponse} from '../../data/festival-data';
import {InfrastructureResponse} from '../../data/infrastructure-data';
import {HttpResponse} from '@angular/common/http';
import {DataTransfertService} from '../../data-transfert.service';
import {HebergementResponse, SearchHebergementResponse} from '../../data/hebergement-data';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-infrastructure',
  templateUrl: './infrastructure.component.html',
  styleUrls: ['./infrastructure.component.scss']
})
export class InfrastructureComponent implements OnInit {

  @Input() data: InfrastructureResponse;
  hebergements: SearchHebergementResponse;

  constructor(private dts: DataTransfertService, private route: Router) { }

  ngOnInit() {
  }

  voirHebergements(id: string) {
   this.recupererHebergements(id);
  }

  async recupererHebergements(id: string) {
    const c: Promise<HttpResponse<string>> = this.dts.GET('/api/hebergements? +' +
    '&idInfra=' + id + '&nbEnfants=0&nbAdultes=2', {
    });
    await c.then((resolve) => {
      this.hebergements = JSON.parse(resolve.body);
      this.dts.hebergementsAffiches = this.hebergements.hebergements;
      console.log(this.hebergements);
      this.route.navigate(['listeHebergements']);
    });
  }

}
