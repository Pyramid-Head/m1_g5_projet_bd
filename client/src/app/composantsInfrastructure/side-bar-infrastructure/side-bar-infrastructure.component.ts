import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-bar-infrastructure',
  templateUrl: './side-bar-infrastructure.component.html',
  styleUrls: ['./side-bar-infrastructure.component.scss']
})
export class SideBarInfrastructureComponent implements OnInit {

  private recherche: boolean;
  private tri: boolean;

  constructor() {
    this.recherche = false;
    this.tri = false;
  }

  ngOnInit() {
  }

}
