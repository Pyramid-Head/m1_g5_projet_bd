import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBarInfrastructureComponent } from './side-bar-infrastructure.component';

describe('SideBarInfrastructureComponent', () => {
  let component: SideBarInfrastructureComponent;
  let fixture: ComponentFixture<SideBarInfrastructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideBarInfrastructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarInfrastructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
