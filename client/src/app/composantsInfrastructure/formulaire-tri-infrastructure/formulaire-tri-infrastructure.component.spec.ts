import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireTriInfrastructureComponent } from './formulaire-tri-infrastructure.component';

describe('FormulaireTriInfrastructureComponent', () => {
  let component: FormulaireTriInfrastructureComponent;
  let fixture: ComponentFixture<FormulaireTriInfrastructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireTriInfrastructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireTriInfrastructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
