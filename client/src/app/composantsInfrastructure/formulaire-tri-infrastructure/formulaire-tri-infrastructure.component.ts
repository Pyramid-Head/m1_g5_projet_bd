import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formulaire-tri-infrastructure',
  templateUrl: './formulaire-tri-infrastructure.component.html',
  styleUrls: ['./formulaire-tri-infrastructure.component.scss']
})
export class FormulaireTriInfrastructureComponent implements OnInit {

  private choixTri: string;

  constructor() {
    this.choixTri = '';
  }

  ngOnInit() {
  }

}
