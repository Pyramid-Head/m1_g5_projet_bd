import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireRechercheInfrastructureComponent } from './formulaire-recherche-infrastructure.component';

describe('FormulaireRechercheInfrastructureComponent', () => {
  let component: FormulaireRechercheInfrastructureComponent;
  let fixture: ComponentFixture<FormulaireRechercheInfrastructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireRechercheInfrastructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireRechercheInfrastructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
