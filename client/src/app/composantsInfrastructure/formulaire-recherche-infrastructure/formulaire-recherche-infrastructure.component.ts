import {Component, Input, OnInit} from '@angular/core';
import {DataTransfertService} from '../../data-transfert.service';
import {FormControl, FormGroup} from '@angular/forms';
import {InfrastructureSearch, SearchInfrastructuresResponse} from '../../data/infrastructure-data';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-formulaire-recherche-infrastructure',
  templateUrl: './formulaire-recherche-infrastructure.component.html',
  styleUrls: ['./formulaire-recherche-infrastructure.component.scss']
})
export class FormulaireRechercheInfrastructureComponent implements OnInit {

  criteresInfrastructure: InfrastructureSearch;

  rechercheSection = new FormGroup({
    nameCtrl: new FormControl(''),
    lieuCtrl: new FormControl(''),
    dateDebutCtrl: new FormControl(''),
    dateFinCtrl: new FormControl(''),
    prixMinCtrl: new FormControl(''),
    prixMaxCtrl: new FormControl(''),
  });

  listeTypes: string[];

  private erreurDate: boolean;
  private erreurPrix: boolean;
  private erreurAucunAdulte: boolean;

  private nbAdultes: number;
  private nbEnfants: number;

  private listeTypesConcat: string;
  private stringRequete: string;

  infrastructures: SearchInfrastructuresResponse;

  constructor(private dts: DataTransfertService) {
    // @ts-ignore
    this.criteresInfrastructure = [];
    this.nbAdultes = 1;
    this.nbEnfants = 0;
    this.erreurDate = false;
    this.erreurPrix = false;
    this.erreurAucunAdulte = false;
  }

  ngOnInit() {
  }

  ajouter(type: string, ajout: boolean) {
    this.dts.ajouter(type, ajout);
    this.nbEnfants = this.dts.nbEnfants;
    this.nbAdultes = this.dts.nbAdultes;
  }

  rechercherInfrastructure() {
    this.criteresInfrastructure.nom = this.rechercheSection.get('nameCtrl').value;
    this.criteresInfrastructure.departement = this.rechercheSection.get('lieuCtrl').value;
    this.criteresInfrastructure.dateDeDebut = this.dts.changerDate(this.rechercheSection.get('dateDebutCtrl').value.toString());
    this.criteresInfrastructure.dateDeFin = this.dts.changerDate(this.rechercheSection.get('dateFinCtrl').value.toString());
    this.criteresInfrastructure.prixMin = this.rechercheSection.get('prixMinCtrl').value;
    this.criteresInfrastructure.prixMax = this.rechercheSection.get('prixMaxCtrl').value;
    this.criteresInfrastructure.nbAdultes = this.nbAdultes;
    this.criteresInfrastructure.nbEnfants = this.nbEnfants;
    this.criteresInfrastructure.typeInfra = this.listeTypes;
    this.verif();
    this.concatType();
    this.parametresToString();
    this.dts.stringRequeteInfra = this.stringRequete;
    if (!(this.erreurPrix || this.erreurDate || this.erreurAucunAdulte)) {
      this.recupererInfrastructures();
    }
  }

  verif() {
    this.criteresInfrastructure.dateDeDebut > this.criteresInfrastructure.dateDeFin ? this.erreurDate = true : this.erreurDate = false;
    this.criteresInfrastructure.prixMin > this.criteresInfrastructure.prixMax ? this.erreurPrix = true : this.erreurPrix = false;
    this.criteresInfrastructure.nbAdultes === 0 ? this.erreurAucunAdulte = true : this.erreurAucunAdulte = false;
  }

  async recupererInfrastructures() {
    const c: Promise<HttpResponse<string>> = this.dts.GET('/api/infrastructures?' +
      this.stringRequete + '&numPage=' + this.dts.pageInfrastructureCourante.toString(), {
    });
    console.log(this.stringRequete);
    await c.then((resolve) => {
      this.infrastructures = JSON.parse(resolve.body);
      this.dts.infrastructuresAffichees = this.infrastructures.infrastructures;
    });
    this.dts.aAfficherInfra = this.dts.infrastructuresAffichees;
    console.log(this.dts.infrastructuresAffichees);
  }

  concatType() {
    let i = 0;
    this.listeTypesConcat = '';
    if ( this.criteresInfrastructure.typeInfra !== undefined ) {
      while (i < this.criteresInfrastructure.typeInfra.length && this.criteresInfrastructure.typeInfra[i] !== 'tout') {
        this.listeTypesConcat += ';' + this.criteresInfrastructure.typeInfra[i];
        i++;
        console.log(i);
      }
      if (this.criteresInfrastructure.typeInfra[i] === 'tout') {
        this.listeTypesConcat = '';
      }
    } else {
      this.listeTypesConcat = '';
    }
  }

  parametresToString() {
    this.stringRequete = '';
    this.stringRequete += 'name=' + this.criteresInfrastructure.nom.toUpperCase();
    this.stringRequete += '&lieu=' + this.criteresInfrastructure.departement;
    this.stringRequete += '&dateDebut=' + this.criteresInfrastructure.dateDeDebut;
    this.stringRequete += '&dateFin=' + this.criteresInfrastructure.dateDeFin;
    this.stringRequete += '&prixMin=' + this.criteresInfrastructure.prixMin;
    this.stringRequete += '&prixMax=' + this.criteresInfrastructure.prixMax;
    this.stringRequete += '&nbAdultes=' + this.criteresInfrastructure.nbAdultes;
    this.stringRequete += '&nbEnfants=' + this.criteresInfrastructure.nbEnfants;
    this.stringRequete += '&typeInfra=' + this.listeTypesConcat;
  }

}
