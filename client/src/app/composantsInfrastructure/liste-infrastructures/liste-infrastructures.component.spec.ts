import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeInfrastructuresComponent } from './liste-infrastructures.component';

describe('ListeInfrastructuresComponent', () => {
  let component: ListeInfrastructuresComponent;
  let fixture: ComponentFixture<ListeInfrastructuresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeInfrastructuresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeInfrastructuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
