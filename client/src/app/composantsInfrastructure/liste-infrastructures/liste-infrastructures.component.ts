import { Component, OnInit } from '@angular/core';
import {DataTransfertService} from '../../data-transfert.service';
import {HttpResponse} from '@angular/common/http';
import {InfrastructureResponse, SearchInfrastructuresResponse} from '../../data/infrastructure-data';
import {MatDialog} from '@angular/material';
import {ConfirmationAjoutPanierComponent} from '../../popup/confirmation-ajout-panier/confirmation-ajout-panier.component';
import {ErreurPasDeCorrespondanceComponent} from '../../popup/erreurs/erreur-pas-de-correspondance/erreur-pas-de-correspondance.component';

@Component({
  selector: 'app-liste-infrastructures',
  templateUrl: './liste-infrastructures.component.html',
  styleUrls: ['./liste-infrastructures.component.scss']
})
export class ListeInfrastructuresComponent implements OnInit {

  private numeroPage: number;
  infrastructures: SearchInfrastructuresResponse;
  aAfficher: InfrastructureResponse[] = [];

  constructor(private dts: DataTransfertService, private dialog: MatDialog) {
    this.numeroPage = this.dts.pageInfrastructureCourante;
  }

  async init() {
    console.log(this.dts.infrastructuresAfficheesRecommandees);
    if (!this.dts.festCommande) {
      await this.recupererInfrastructures();
      console.log('h');
    } else if (this.dts.infrastructuresAfficheesRecommandees.length === 0) {
      this.openDialog();
      await this.recupererInfrastructures();
      await  window.setTimeout(() => this.dialog.closeAll(), 10);
      console.log('a');
    } else {
      this.dts.aAfficherInfra = this.dts.infrastructuresAfficheesRecommandees;
      window.setTimeout(() => this.dialog.closeAll(), 10);
    }
  }

  openDialog() {
    this.dialog.open(ErreurPasDeCorrespondanceComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }

  ngOnInit() {
    this.init();
  }

  async recupererInfrastructures() {
    const c: Promise<HttpResponse<string>> = this.dts.GET('/api/infrastructures?numPage=' +
      this.dts.pageInfrastructureCourante.toString() + '&' + this.dts.stringRequeteInfra , {
    });
    await c.then((resolve) => {
      this.infrastructures = JSON.parse(resolve.body);
      this.dts.infrastructuresAffichees = this.infrastructures.infrastructures;
      this.dts.aAfficherInfra = this.dts.infrastructuresAffichees;
      console.log(this.infrastructures);
    });
  }

  pagePrecedente() {
    if (this.numeroPage !== 1) {
      this.numeroPage --;
      this.dts.pageInfrastructureCourante = this.numeroPage;
      this.recupererInfrastructures();
    }
  }

  pageSuivante() {
    this.numeroPage ++;
    this.dts.pageInfrastructureCourante = this.numeroPage;
    this.recupererInfrastructures();
  }
}
