import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PageAccueilComponent} from './page-accueil/page-accueil.component';
import {ListeFestivalsComponent} from './composantsFestival/liste-festivals/liste-festivals.component';
import {ListeInfrastructuresComponent} from './composantsInfrastructure/liste-infrastructures/liste-infrastructures.component';
import {FormulaireRechercheInfrastructureComponent} from './composantsInfrastructure/formulaire-recherche-infrastructure/formulaire-recherche-infrastructure.component';
import {FormulaireRechercheFestivalComponent} from './composantsFestival/formulaire-recherche-festival/formulaire-recherche-festival.component';
import {MesFestivalsComponent} from './menu/mes-festivals/mes-festivals.component';
import {MesHebergementsComponent} from './menu/mes-hebergements/mes-hebergements.component';
import {PanierComponent} from './panier/panier/panier.component';
import {ListeHebergementsComponent} from './composantsHebergement/liste-hebergements/liste-hebergements.component';

const routes: Routes = [
  { path: '', component: PageAccueilComponent},
  { path: 'listeFestivals', component: ListeFestivalsComponent},
  { path: 'listeInfrastructures', component: ListeInfrastructuresComponent},
  { path: 'listeHebergements', component: ListeHebergementsComponent},
  { path: 'mesFestivals', component: MesFestivalsComponent},
  { path: 'mesHebergements', component: MesHebergementsComponent},
  { path: 'monPanier', component: PanierComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
