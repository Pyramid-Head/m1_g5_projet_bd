import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesFestivalsComponent } from './mes-festivals.component';

describe('MesFestivalsComponent', () => {
  let component: MesFestivalsComponent;
  let fixture: ComponentFixture<MesFestivalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesFestivalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesFestivalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
