import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesHebergementsComponent } from './mes-hebergements.component';

describe('MesHebergementsComponent', () => {
  let component: MesHebergementsComponent;
  let fixture: ComponentFixture<MesHebergementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesHebergementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesHebergementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
