import { Component, OnInit } from '@angular/core';
import {DataTransfertService} from '../../data-transfert.service';

@Component({
  selector: 'app-liste-hebergements',
  templateUrl: './liste-hebergements.component.html',
  styleUrls: ['./liste-hebergements.component.scss']
})
export class ListeHebergementsComponent implements OnInit {

  constructor(private dts: DataTransfertService) { }

  ngOnInit() {
  }

}
