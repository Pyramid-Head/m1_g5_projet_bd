import {Component, Input, OnInit} from '@angular/core';
import {HebergementResponse} from '../../data/hebergement-data';
import {HttpResponse} from '@angular/common/http';
import {DataTransfertService} from '../../data-transfert.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {ConfirmationAjoutPanierComponent} from '../../popup/confirmation-ajout-panier/confirmation-ajout-panier.component';
import {ReservationFestival} from '../../data/festival-data';
import {ErreurPlusDePlacesComponent} from '../../popup/erreurs/erreur-plus-de-places/erreur-plus-de-places.component';
import {DemandeContinuerFestComponent} from '../../popup/demande-continuer-fest/demande-continuer-fest.component';
import {DemandeContinuerHebergComponent} from '../../popup/demande-continuer-heberg/demande-continuer-heberg.component';
import {NonConnecteComponent} from '../../popup/non-connecte/non-connecte.component';

@Component({
  selector: 'app-hebergement',
  templateUrl: './hebergement.component.html',
  styleUrls: ['./hebergement.component.scss']
})
export class HebergementComponent implements OnInit {

  rechercheSection = new FormGroup({
    dateDebutCtrl: new FormControl(''),
    dateFinCtrl: new FormControl('')
  });


  @Input() data: HebergementResponse;
  reponseRequete: ReservationFestival;
  stringRequete: string;
  idHeberg: string;

  nbAdultes: number;
  nbEnfants: number;
  dateDeDebut: string;
  dateDeFin: string;

  erreurDate: boolean;

  constructor(private dts: DataTransfertService,
              private route: Router,
              private dialog: MatDialog) {
    this.nbAdultes = 1;
    this.nbEnfants = 0;
  }

  ngOnInit() {
  }

  ajouterAuPanier(id: number) {
    if (this.dts.idCompte === '') {
      this.openDialogNonConnecte();
    } else {
      this.dateDeDebut = this.rechercheSection.get('dateDebutCtrl').value;
      this.dateDeFin = this.rechercheSection.get('dateFinCtrl').value;
      this.idHeberg = id.toString();
      this.concat();
      console.log(this.stringRequete);
      this.verif();
      if (!this.erreurDate) {
        this.ajouterAuPanierRequete();
      }
    }
  }

  async ajouterAuPanierRequete() {
    const c: Promise<HttpResponse<string>> = this.dts.POST('/api/addHebPanier?' + this.stringRequete,
      {});
    await c.then((resolve) => {
      this.reponseRequete = JSON.parse(resolve.body);
      console.log('reponse=');
      console.log(this.reponseRequete);
    });
    if (this.reponseRequete.ERROR === 'OUI') {
      this.openDialogErreur();
    } else {
      this.dts.infraCommande = true;
      this.dts.festivalsAffichesRecommandes = this.reponseRequete.festivals;
      this.openDialog();
      if (!this.dts.festCommande) {
        console.log('a');
        this.dts.idReservation = this.reponseRequete.ReservationId.toString();
        window.setTimeout(() => this.changerPage(), 10);
      } else {
        console.log('b');
        this.demande();
        // window.setTimeout(() => this.demande(), 30);
      }
    }
  }

  openDialogNonConnecte() {
    this.dialog.open(NonConnecteComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }

  demande() {
    this.dialog.closeAll();
    this.openDialogDemande();
  }

  changerPage() {
    this.route.navigate(['\listeFestivals']);
  }

  openDialogDemande() {
    this.dialog.open(DemandeContinuerHebergComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }

  openDialogErreur() {
    this.dialog.open(ErreurPlusDePlacesComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }


  openDialog() {
    this.dialog.open(ConfirmationAjoutPanierComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }

  verif() {
    this.dateDeDebut > this.dateDeFin ? this.erreurDate = true : this.erreurDate = false;
    this.dateDeDebut.toString().length < 2 ? this.erreurDate = true : this.erreurDate = false;
    this.dateDeFin.toString().length < 2 ? this.erreurDate = true : this.erreurDate = false;
  }

  concat() {
    this.stringRequete = '';
    this.stringRequete += 'reservationId=' + this.dts.idReservation + '&compteId=' + this.dts.idCompte;
    this.stringRequete += '&idHebergement=' + this.idHeberg;
    this.stringRequete += '&nbAdultes=' + this.nbAdultes.toString();
    this.stringRequete += '&nbEnfants=' + this.nbEnfants.toString();
    this.stringRequete += '&dateDeDebut=' + this.dts.changerDate(this.dateDeDebut.toString());
    this.stringRequete += '&dateDeFin=' + this.dts.changerDate(this.dateDeFin.toString());
    this.stringRequete = this.stringRequete.trim();
    console.log(this.stringRequete);
  }

  ajouter(type: string, ajout: boolean, capacite: number) {
    switch (type) {
      case 'adulte':
        ajout ? (this.nbAdultes < capacite ? this.nbAdultes++ : this.nbAdultes) :
          (this.nbAdultes !== 0 ? this.nbAdultes-- : this.nbAdultes);
        break;
      case 'enfant':
        ajout ? (this.nbEnfants < capacite ? this.nbEnfants++ : this.nbEnfants) :
          (this.nbEnfants !== 0 ? this.nbEnfants-- : this.nbEnfants);
        break;
    }
  }

}
