import {Component, Input, OnInit} from '@angular/core';
import {DataTransfertService} from '../../data-transfert.service';
import {FestivalResponse, FestivalSearch, SearchFestivalsResponse} from '../../data/festival-data';
import {FormControl, FormGroup} from '@angular/forms';
import {HttpResponse} from '@angular/common/http';

// @ts-ignore
@Component({
  selector: 'app-formulaire-recherche-festival',
  templateUrl: './formulaire-recherche-festival.component.html',
  styleUrls: ['./formulaire-recherche-festival.component.scss']
})
export class FormulaireRechercheFestivalComponent implements OnInit {

  criteresFestival: FestivalSearch;

  rechercheSection = new FormGroup({
    nameCtrl: new FormControl(''),
    lieuCtrl: new FormControl(''),
    dateDebutCtrl: new FormControl(''),
    dateFinCtrl: new FormControl(''),
    prixMinCtrl: new FormControl(''),
    prixMaxCtrl: new FormControl('')
  });

  private erreurDate: boolean;
  private erreurPrix: boolean;
  private erreurAucunAdulte: boolean;

  private nbAdultes: number;
  private nbEnfants: number;

  // festivalSearch
  private listeTypes: string[];
  private listeTypesConcat: string;

  // affichage sous cat
  private artsDeLaScene: boolean;
  private artsVisuels: boolean;
  private musiques: boolean;
  private autres: boolean;

  // FestivalResponse
  festivalsResponse: FestivalResponse[];

  // requeteString
  stringRequete: string;

  constructor(private dts: DataTransfertService) {

    // @ts-ignore
    this.criteresFestival = [];

    this.nbAdultes = 1;
    this.nbEnfants = 0;
    this.erreurDate = false;
    this.erreurPrix = false;
    this.erreurAucunAdulte = false;

    this.artsVisuels = false;
    this.artsDeLaScene = false;
    this.musiques = false;
    this.autres = false;
    this.criteresFestival.typeFest = [];
    this.listeTypesConcat = '';
  }

  ngOnInit() {
  }

  ajouter(type: string, ajout: boolean) {
    this.dts.ajouter(type, ajout);
    this.nbEnfants = this.dts.nbEnfants;
    this.nbAdultes = this.dts.nbAdultes;
  }

  rechercherFestival() {
    console.log('a' + this.rechercheSection.get('dateDebutCtrl').value.toString() + 'a');
    this.criteresFestival.nom = this.rechercheSection.get('nameCtrl').value;
    this.criteresFestival.departement = this.rechercheSection.get('lieuCtrl').value;
    this.criteresFestival.dateDeDebut = this.dts.changerDate(this.rechercheSection.get('dateDebutCtrl').value.toString());
    this.criteresFestival.dateDeFin = this.dts.changerDate(this.rechercheSection.get('dateFinCtrl').value.toString());
    this.criteresFestival.prixMin = this.rechercheSection.get('prixMinCtrl').value.toString();
    this.criteresFestival.prixMax = this.rechercheSection.get('prixMaxCtrl').value.toString();
    this.criteresFestival.nbAdultes = this.nbAdultes.toString();
    this.criteresFestival.nbEnfants = this.nbEnfants.toString();
    this.criteresFestival.typeFest = this.listeTypes;
    this.verif();
    this.concatType();
    if (!(this.erreurPrix || this.erreurDate || this.erreurAucunAdulte)) {
      this.recupererFestivals();
    }
  }

  concatType() {
    let i = 0;
    this.listeTypesConcat = '';
    if ( this.criteresFestival.typeFest !== undefined) {
      while (i < this.criteresFestival.typeFest.length && this.criteresFestival.typeFest[i] !== 'tout') {
        this.listeTypesConcat += this.criteresFestival.typeFest[i] + ';';
        i++;
      }
      if (this.criteresFestival.typeFest[i] === 'tout') {
        this.listeTypesConcat = '';
      }
    }
  }

  verif() {
    this.criteresFestival.dateDeDebut > this.criteresFestival.dateDeFin ? this.erreurDate = true : this.erreurDate = false;
    this.criteresFestival.prixMin > this.criteresFestival.prixMax ? this.erreurPrix = true : this.erreurPrix = false;
    this.criteresFestival.nbAdultes === '0' ? this.erreurAucunAdulte = true : this.erreurAucunAdulte = false;
  }

  async recupererFestivals() {
    this.parametresToString();
    console.log(this.stringRequete);
    const c: Promise<HttpResponse<string>> = this.dts.GET('/api/festivals?' + this.stringRequete,
      {});
    await c.then((resolve) => {
      this.festivalsResponse = JSON.parse(resolve.body);
      this.dts.festivalsAffiches = this.festivalsResponse;
      this.dts.aAfficherFestival = this.dts.festivalsAffiches;
      console.log('heheh' + this.dts.aAfficherFestival);
    });
  }

  parametresToString() {
    this.stringRequete = '';
    this.stringRequete += 'name=' + this.criteresFestival.nom;
    this.stringRequete += '&lieu=' + this.criteresFestival.departement;
    this.stringRequete += '&dateDebut=' + this.criteresFestival.dateDeDebut;
    this.stringRequete += '&dateFin=' + this.criteresFestival.dateDeFin;
    this.stringRequete += '&prixMin=' + this.criteresFestival.prixMin;
    this.stringRequete += '&prixMax=' + this.criteresFestival.prixMax;
    this.stringRequete += '&nbAdultes=' + this.criteresFestival.nbAdultes;
    this.stringRequete += '&nbEnfants=' + this.criteresFestival.nbEnfants;
    this.stringRequete += '&typeFest=' + this.listeTypesConcat;
    this.dts.stringRequeteFest = this.stringRequete;
    this.stringRequete += '&numPage=' + this.dts.pageFestivalCourante.toString();
  }

}
