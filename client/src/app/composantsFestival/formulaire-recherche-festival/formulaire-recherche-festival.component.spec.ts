import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireRechercheFestivalComponent } from './formulaire-recherche-festival.component';

describe('FormulaireRechercheFestivalComponent', () => {
  let component: FormulaireRechercheFestivalComponent;
  let fixture: ComponentFixture<FormulaireRechercheFestivalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireRechercheFestivalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireRechercheFestivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
