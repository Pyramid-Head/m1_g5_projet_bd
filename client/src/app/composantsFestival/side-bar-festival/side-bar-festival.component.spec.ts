import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBarFestivalComponent } from './side-bar-festival.component';

describe('SideBarFestivalComponent', () => {
  let component: SideBarFestivalComponent;
  let fixture: ComponentFixture<SideBarFestivalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideBarFestivalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarFestivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
