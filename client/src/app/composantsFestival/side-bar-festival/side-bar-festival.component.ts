import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-bar-festival',
  templateUrl: './side-bar-festival.component.html',
  styleUrls: ['./side-bar-festival.component.scss']
})
export class SideBarFestivalComponent implements OnInit {

  private recherche: boolean;
  private tri: boolean;

  constructor() {
    this.recherche = false;
    this.tri = false;
  }

  ngOnInit() {
  }

}
