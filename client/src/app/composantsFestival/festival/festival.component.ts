import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {DetailsFestivalComponent} from '../details-festival/details-festival.component';
import {HttpResponse} from '@angular/common/http';
import {FestivalResponse} from '../../data/festival-data';
import {DataTransfertService} from '../../data-transfert.service';

@Component({
  selector: 'app-festival',
  templateUrl: './festival.component.html',
  styleUrls: ['./festival.component.scss']
})
export class FestivalComponent implements OnInit {

  @Input() data: FestivalResponse;
  festival: FestivalResponse;

  constructor(public dialog: MatDialog,
              private dts: DataTransfertService) { }

  ngOnInit() {
  }

  async openDialogDetailsFestival(idFestival: number) {
    await this.recupererDetailsFestival(idFestival);
    await this.dialog.open(DetailsFestivalComponent, {
      width: '75%',
      height: '75',
      data: this.festival, // this.festivalResponse,
      closeOnNavigation: true
    });
  }

  async recupererDetailsFestival(id: number) {
    const c: Promise<HttpResponse<string>> = this.dts.GET('/api/festivalInfo?idFestival=' + id.toString()
      + '&nbAdultes=2&nbEnfants=0',
      {});
    await c.then((resolve) => {
      this.festival = JSON.parse(resolve.body);
      console.log('festival=');
      console.log(this.festival);
    });
  }

  belAffichage(nom: string) {
    if (nom.trim().length > 20) {
      return nom.substring(0, 20) + '...';
    } else {
      return nom;
    }
  }
}
