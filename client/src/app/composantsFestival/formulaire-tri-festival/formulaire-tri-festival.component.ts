import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formulaire-tri-festival',
  templateUrl: './formulaire-tri-festival.component.html',
  styleUrls: ['./formulaire-tri-festival.component.scss']
})
export class FormulaireTriFestivalComponent implements OnInit {

  private choixTri: string;

  constructor() {
    this.choixTri = '';
  }

  ngOnInit() {
  }

}
