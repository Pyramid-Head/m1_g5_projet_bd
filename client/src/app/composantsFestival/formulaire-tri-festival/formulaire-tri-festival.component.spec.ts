import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireTriFestivalComponent } from './formulaire-tri-festival.component';

describe('FormulaireTriFestivalComponent', () => {
  let component: FormulaireTriFestivalComponent;
  let fixture: ComponentFixture<FormulaireTriFestivalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireTriFestivalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireTriFestivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
