import { Component, OnInit } from '@angular/core';
import {DataTransfertService} from '../../data-transfert.service';
import {HttpResponse} from '@angular/common/http';
import {FestivalResponse} from '../../data/festival-data';
import {ErreurPasDeCorrespondanceComponent} from '../../popup/erreurs/erreur-pas-de-correspondance/erreur-pas-de-correspondance.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-liste-festivals',
  templateUrl: './liste-festivals.component.html',
  styleUrls: ['./liste-festivals.component.scss']
})
export class ListeFestivalsComponent implements OnInit {

  festivalsResponse: FestivalResponse[];
  private numeroPage: number;

  constructor(private dts: DataTransfertService, private dialog: MatDialog) {
    this.numeroPage = this.dts.pageFestivalCourante;
  }

  async init() {
    console.log(this.dts.festivalsAffichesRecommandes);
    if (!this.dts.infraCommande) {
      await this.recupererFestivals();
    } else if (this.dts.festivalsAffichesRecommandes.length === 0) {
      this.openDialog();
      await this.recupererFestivals();
      console.log('a');
    } else {
      this.dts.aAfficherFestival = this.dts.festivalsAffichesRecommandes;
      window.setTimeout(() => this.dialog.closeAll(), 10);
    }
  }

  openDialog() {
    this.dialog.open(ErreurPasDeCorrespondanceComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }

  ngOnInit(): void {
    this.init();
  }

  async recupererFestivals() {
    const c: Promise<HttpResponse<string>> = this.dts.GET('/api/festivals?numPage='
      + this.dts.pageFestivalCourante.toString() + '&' + this.dts.stringRequeteFest, {});
    await c.then((resolve) => {
      this.festivalsResponse = JSON.parse(resolve.body);
      this.dts.festivalsAffiches = this.festivalsResponse;
      this.dts.aAfficherFestival = this.dts.festivalsAffiches;
      console.log(this.festivalsResponse);
    });
  }

  pagePrecedente() {
    if (this.numeroPage !== 1) {
      this.numeroPage --;
      this.dts.pageFestivalCourante = this.numeroPage;
      this.recupererFestivals();
    }
  }

  pageSuivante() {
    this.numeroPage ++;
    this.dts.pageFestivalCourante = this.numeroPage;
    this.recupererFestivals();
  }

}
