import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FestivalInfos, FestivalResponse} from '../../data/festival-data';
import {HttpResponse} from '@angular/common/http';
import {DataTransfertService} from '../../data-transfert.service';
import {InfoNonImplementeComponent} from '../../popup/info-non-implemente/info-non-implemente.component';
import {ConfirmationAjoutPanierComponent} from '../../popup/confirmation-ajout-panier/confirmation-ajout-panier.component';
import {ReservationInfra, SearchInfrastructuresResponse} from '../../data/infrastructure-data';
import {Router} from '@angular/router';
import {ErreurPlusDePlacesComponent} from '../../popup/erreurs/erreur-plus-de-places/erreur-plus-de-places.component';
import {DemandeContinuerFestComponent} from '../../popup/demande-continuer-fest/demande-continuer-fest.component';
import {NonConnecteComponent} from '../../popup/non-connecte/non-connecte.component';

@Component({
  selector: 'app-details-festival',
  templateUrl: './details-festival.component.html',
  styleUrls: ['./details-festival.component.scss']
})
export class DetailsFestivalComponent implements OnInit {

  reponse: ReservationInfra;

  nbPlacesSansCat: number;
  nbPlacesCat1: number;
  nbPlacesCat2: number;

  stringRequete: string;
  erreur: boolean;

  idCat1: string;
  idCat2: string;
  idSansCat: string;

  constructor(public dialogRef: MatDialogRef<DetailsFestivalComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: FestivalInfos,
              private dts: DataTransfertService,
              private dialog: MatDialog,
              private route: Router) {
    this.stringRequete = '';

    this.erreur = false;
    this.nbPlacesSansCat = 0;
    this.nbPlacesCat2 = 0;
    this.nbPlacesCat1 = 0;

    this.idCat1 = '';
    this.idCat2 = '';
    this.idSansCat = '';
  }

  ngOnInit() {
    console.log(this.data);
  }

  ajouterAuPanier() {
    if (this.dts.idCompte === '') {
      this.openDialogNonConnecte();
    } else {
      if (this.nbPlacesCat1 === 0 && this.nbPlacesCat2 === 0 && this.nbPlacesSansCat === 0) {
        this.erreur = true;
      } else {
        this.erreur = false;
        this.concat();
        this.ajouterAuPanierRequete();
      }
    }
  }

  openDialog() {
    this.dialog.open(ConfirmationAjoutPanierComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }

  openDialogErreur() {
    this.dialog.open(ErreurPlusDePlacesComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }

  openDialogDemande() {
    this.dialog.open(DemandeContinuerFestComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }

  openDialogNonConnecte() {
    this.dialog.open(NonConnecteComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }

  ajouter(cat: string, ajout: boolean, placesDispos: number, idJour: number) {
    switch (cat) {
      case 'CAT1':
        ajout ? (this.nbPlacesCat1 < placesDispos ? this.nbPlacesCat1++ : this.nbPlacesCat1) :
          (this.nbPlacesCat1 !== 0 ? this.nbPlacesCat1-- : this.nbPlacesCat1);
        this.idCat1 = idJour.toString();
        break;
      case 'CAT2':
        ajout ? (this.nbPlacesCat2 < placesDispos ? this.nbPlacesCat2++ : this.nbPlacesCat2) :
          (this.nbPlacesCat2 !== 0 ? this.nbPlacesCat2-- : this.nbPlacesCat2);
        this.idCat2 = idJour.toString();
        break;
      case 'SANSCAT':
        ajout ? (this.nbPlacesSansCat < placesDispos ? this.nbPlacesSansCat++ : this.nbPlacesSansCat) :
          (this.nbPlacesSansCat !== 0 ? this.nbPlacesSansCat-- : this.nbPlacesSansCat);
        this.idSansCat = idJour.toString();
        break;
    }
  }

  async ajouterAuPanierRequete() {
    console.log(this.stringRequete);
    const c: Promise<HttpResponse<string>> = this.dts.POST('/api/addFestPanier?' + this.stringRequete,
      {});
    await c.then((resolve) => {
      console.log(resolve.body);
      this.reponse = JSON.parse(resolve.body);
    });
    console.log(this.reponse.ERROR);
    if (this.reponse.ERROR === 'OUI') {
      this.openDialogErreur();
    } else {
      console.log('reservationId' + this.reponse.ReservationId);
      this.dts.festCommande = true;
      this.dts.infrastructuresAfficheesRecommandees = this.reponse.infrastructures.infrastructures;
      this.openDialog();
      if (!this.dts.infraCommande) {
        this.dts.idReservation = this.reponse.ReservationId.toString();
        window.setTimeout(() => this.changerPage(), 10);
      } else {
        window.setTimeout(() => this.demande(), 30);
      }
    }
  }

  demande() {
    this.dialog.closeAll();
    this.openDialogDemande();
  }

  changerPage() {
    this.dialogRef.close();
    // this.dialog.closeAll();
    this.route.navigate(['\listeInfrastructures']);
  }

  concat() {
    this.stringRequete += 'reservationId=' + this.dts.idReservation + '&compteId=' + this.dts.idCompte;
    let idJourFest = '&idJourFest=';
    let nbrPlaces = '&nbrPlace=';

    if (this.nbPlacesCat1 !== 0) {
      idJourFest += this.idCat1;
      nbrPlaces += this.nbPlacesCat1.toString();
    }
    if (this.nbPlacesCat2 !== 0) {
      if (idJourFest !== '&idJourFest=') {
        idJourFest += ',';
        nbrPlaces += ',';
      }
      idJourFest += this.idCat2;
      nbrPlaces += this.nbPlacesCat2.toString();
    }
    if (this.nbPlacesSansCat !== 0) {
      if (idJourFest !== '&idJourFest=') {
        idJourFest += ',';
        nbrPlaces += ',';
      }
      idJourFest += this.idSansCat;
      nbrPlaces += this.nbPlacesSansCat.toString();
    }
    this.stringRequete += idJourFest + nbrPlaces;
  }

  close() {
    this.dialogRef.close();
  }

}
