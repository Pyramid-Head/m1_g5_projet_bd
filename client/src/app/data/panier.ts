export interface RecapFestival {
  idJourF?: number;
  idResevation?: number;
  dateHeureMiseEnPanier?: string;
  nbrPlace: number;
}

export interface RecapHebergement {
  idHebergement?: number;
  idResevation?: number;
  dateDebut?: string;
  dateFin?: string;
  dateHeureMiseEnPanier?: string;
  nbEnfants?: number;
  nbAdultes?: number;
}

export interface Reservation {
  idReservation: number;
  idCompte: number;
  montantTotal: number;
}

export interface Panier {
  Reservation: Reservation;
  ResaHebergement?: RecapHebergement[];
  ResaJourFestival?: RecapFestival[];
}
