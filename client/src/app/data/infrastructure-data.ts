export interface InfrastructureResponse {
  idInfrastructure: number;
  numDepartement?: string;
  nom?: string;
  classement?: string;
  categorie?: string;
  telephone?: string;
  siteInternet?: string;
  courriel?: string;
  coordonneesGPS?: string;
  note?: number;
  typeInfra?: string;
  montantEncaisse?: number;
  urlPhoto?: string;
}

export interface InfrastructureSearch {
  nom?: string;
  departement?: string;
  dateDeDebut?: string;
  dateDeFin?: string;
  typeInfra?: string[];
  nbEnfants?: number;
  nbAdultes?: number;
  prixMin?: number;
  prixMax?: number;
}

export interface SearchInfrastructuresResponse {
  infrastructures?: InfrastructureResponse[];
}

export interface ReservationInfra {
  ERROR?: string;
  ReservationId?: string;
  infrastructures: SearchInfrastructuresResponse;
}
