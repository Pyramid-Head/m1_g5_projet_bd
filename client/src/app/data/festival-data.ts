import DateTimeFormat = Intl.DateTimeFormat;

export interface FestivalResponse {
  idFestival: number;
  numDepartement?: string;
  idCompte: number;
  nom: string;
  siteWeb?: string;
  complementDomaine?: string;
  commentaires?: string;
  numeroEdition?: number;
  dateDeCreation?: DateTimeFormat;
  dateDeDebut?: DateTimeFormat;
  dateDeFin?: DateTimeFormat;
  coordonneesGPS: string;
  montantEncaisse: number;
  typeFest: string;
}

export interface FestivalSearch {
  nom?: string;
  departement?: string;
  dateDeDebut?: string;
  dateDeFin?: string;
  typeFest?: string[];
  nbEnfants?: string;
  nbAdultes?: string;
  prixMin?: number;
  prixMax?: number;
}

export interface SearchFestivalsResponse {
  festivals?: FestivalResponse[];
}

export interface JourResponse {
  idJourF: number;
  idFestival: number;
  typePlace: string;
  prix: string;
  dateJf: string;
  placesLibres: number;
}

export interface FestivalInfos {
  jours: JourResponse[];
  festivalInfo: FestivalResponse;
}

export interface ReservationFestival {
  ERROR: string;
  ReservationId?: string;
  festivals: FestivalResponse[];
}


