export interface HebergementResponse {
  idHebergement: number;
  description?: string;
  typeHeberg?: string;
  capacite?: number;
  tarifForfaitaire?: number;
  nbPlacesAdultes?: number;
  nbPlacesEnfants?: number;
  tarifAdulte?: number;
  tarifEnfant?: number;
  urlPhoto?: string;
}

export interface HebergementSearch {
  idInfrastructure?: string;
  typeHeberg?: string[];
  prixMin?: number;
  prixMax?: number;
  nbAdultes: number;
  nbEnfants: number;
  // a completer
}

export interface SearchHebergementResponse {
  hebergements: HebergementResponse[];
}
