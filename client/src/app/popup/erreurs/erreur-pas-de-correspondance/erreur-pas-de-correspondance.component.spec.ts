import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErreurPasDeCorrespondanceComponent } from './erreur-pas-de-correspondance.component';

describe('ErreurPasDeCorrespondanceComponent', () => {
  let component: ErreurPasDeCorrespondanceComponent;
  let fixture: ComponentFixture<ErreurPasDeCorrespondanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErreurPasDeCorrespondanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErreurPasDeCorrespondanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
