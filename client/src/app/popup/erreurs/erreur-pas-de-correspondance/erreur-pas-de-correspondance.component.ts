import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-erreur-pas-de-correspondance',
  templateUrl: './erreur-pas-de-correspondance.component.html',
  styleUrls: ['./erreur-pas-de-correspondance.component.scss']
})
export class ErreurPasDeCorrespondanceComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ErreurPasDeCorrespondanceComponent>) { }

  ngOnInit() {
  }

}
