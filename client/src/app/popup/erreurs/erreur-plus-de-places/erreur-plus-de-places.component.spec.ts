import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErreurPlusDePlacesComponent } from './erreur-plus-de-places.component';

describe('ErreurPlusDePlacesComponent', () => {
  let component: ErreurPlusDePlacesComponent;
  let fixture: ComponentFixture<ErreurPlusDePlacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErreurPlusDePlacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErreurPlusDePlacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
