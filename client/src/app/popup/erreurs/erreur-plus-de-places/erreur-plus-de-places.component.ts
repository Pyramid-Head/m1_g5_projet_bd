import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-erreur-plus-de-places',
  templateUrl: './erreur-plus-de-places.component.html',
  styleUrls: ['./erreur-plus-de-places.component.scss']
})
export class ErreurPlusDePlacesComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ErreurPlusDePlacesComponent>) { }

  ngOnInit() {
  }

}
