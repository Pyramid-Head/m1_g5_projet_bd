import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-demande-continuer-fest',
  templateUrl: './demande-continuer-fest.component.html',
  styleUrls: ['./demande-continuer-fest.component.scss']
})
export class DemandeContinuerFestComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DemandeContinuerFestComponent>,
              private route: Router) { }

  ngOnInit() {
  }

  choix(choix: string) {
    this.dialogRef.close();
    if (choix === 'panier') {
      this.route.navigate(['/monPanier']);
    } else {
      this.route.navigate(['/listeFestivals']);
    }
  }
}
