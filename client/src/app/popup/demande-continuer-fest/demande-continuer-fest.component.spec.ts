import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeContinuerFestComponent } from './demande-continuer-fest.component';

describe('DemandeContinuerFestComponent', () => {
  let component: DemandeContinuerFestComponent;
  let fixture: ComponentFixture<DemandeContinuerFestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemandeContinuerFestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandeContinuerFestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
