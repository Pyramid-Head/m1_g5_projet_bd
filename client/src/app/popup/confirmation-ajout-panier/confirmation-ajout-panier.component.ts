import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-confirmation-ajout-panier',
  templateUrl: './confirmation-ajout-panier.component.html',
  styleUrls: ['./confirmation-ajout-panier.component.scss']
})
export class ConfirmationAjoutPanierComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ConfirmationAjoutPanierComponent>) { }

  ngOnInit() {
  }

}
