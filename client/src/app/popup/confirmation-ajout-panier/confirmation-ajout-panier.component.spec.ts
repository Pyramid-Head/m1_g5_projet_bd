import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationAjoutPanierComponent } from './confirmation-ajout-panier.component';

describe('ConfirmationAjoutPanierComponent', () => {
  let component: ConfirmationAjoutPanierComponent;
  let fixture: ComponentFixture<ConfirmationAjoutPanierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmationAjoutPanierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationAjoutPanierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
