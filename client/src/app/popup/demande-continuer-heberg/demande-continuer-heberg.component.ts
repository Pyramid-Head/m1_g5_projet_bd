import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-demande-continuer-heberg',
  templateUrl: './demande-continuer-heberg.component.html',
  styleUrls: ['./demande-continuer-heberg.component.scss']
})
export class DemandeContinuerHebergComponent implements OnInit {

  constructor( private route: Router, private dialogRef: MatDialogRef<DemandeContinuerHebergComponent>) { }

  ngOnInit() {
  }

  choix(choix: string) {
    this.dialogRef.close();
    if (choix === 'panier') {
      this.route.navigate(['/monPanier']);
    } else {
      this.route.navigate(['/listeInfrastructures']);
    }
  }

}
