import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeContinuerHebergComponent } from './demande-continuer-heberg.component';

describe('DemandeContinuerHebergComponent', () => {
  let component: DemandeContinuerHebergComponent;
  let fixture: ComponentFixture<DemandeContinuerHebergComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemandeContinuerHebergComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandeContinuerHebergComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
