import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-panier-vide',
  templateUrl: './panier-vide.component.html',
  styleUrls: ['./panier-vide.component.scss']
})
export class PanierVideComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<PanierVideComponent>) { }

  ngOnInit() {
  }

}
