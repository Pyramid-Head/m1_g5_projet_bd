import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-info-non-implemente',
  templateUrl: './info-non-implemente.component.html',
  styleUrls: ['./info-non-implemente.component.scss']
})
export class InfoNonImplementeComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<InfoNonImplementeComponent>) { }

  ngOnInit() {
  }

}
