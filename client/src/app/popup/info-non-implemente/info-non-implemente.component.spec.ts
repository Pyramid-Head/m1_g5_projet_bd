import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoNonImplementeComponent } from './info-non-implemente.component';

describe('InfoNonImplementeComponent', () => {
  let component: InfoNonImplementeComponent;
  let fixture: ComponentFixture<InfoNonImplementeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoNonImplementeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoNonImplementeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
