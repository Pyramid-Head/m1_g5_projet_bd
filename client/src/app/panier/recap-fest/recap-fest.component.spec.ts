import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecapFestComponent } from './recap-fest.component';

describe('RecapFestComponent', () => {
  let component: RecapFestComponent;
  let fixture: ComponentFixture<RecapFestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecapFestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecapFestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
