import {Component, Inject, Input, OnInit, Optional} from '@angular/core';
import {RecapFestival} from '../../data/panier';

@Component({
  selector: 'app-recap-fest',
  templateUrl: './recap-fest.component.html',
  styleUrls: ['./recap-fest.component.scss']
})
export class RecapFestComponent implements OnInit {

  @Input() data: RecapFestival;

  constructor() { }

  ngOnInit() {
  }

}
