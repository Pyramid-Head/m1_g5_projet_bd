import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import {DetailsFestivalComponent} from '../../composantsFestival/details-festival/details-festival.component';
import {InfoNonImplementeComponent} from '../../popup/info-non-implemente/info-non-implemente.component';
import {HttpResponse} from '@angular/common/http';
import {DataTransfertService} from '../../data-transfert.service';
import {Panier} from '../../data/panier';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  constructor(public dialog: MatDialog, private dts: DataTransfertService) { }

  prix: string;
  reponse: Panier;

  ngOnInit() {
    this.recupererPanier();
  }

  openDialog() {
    this.dialog.open(InfoNonImplementeComponent, {
      width: '50%',
      height: '75',
      closeOnNavigation: true
    });
  }

  calcul(nombre: number) {
    if (nombre.toString().includes('.')) {
      const point = nombre.toString().indexOf('.');
      this.prix = nombre.toString().substring(0, point + 3);
    } else {
      this.prix = nombre.toString();
    }
  }

  async recupererPanier() {
    const c: Promise<HttpResponse<string>> = this.dts.GET('/api/reservation?reservationID='
      + this.dts.idReservation,
      {});
    c.then((resolve) => {
      this.reponse = JSON.parse(resolve.body);
      console.log(this.reponse);
      this.calcul(this.reponse.Reservation.montantTotal);
      console.log(this.prix);
    });
  }

}
