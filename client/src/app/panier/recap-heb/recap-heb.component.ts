import {Component, Input, OnInit} from '@angular/core';
import {RecapHebergement} from '../../data/panier';

@Component({
  selector: 'app-recap-heb',
  templateUrl: './recap-heb.component.html',
  styleUrls: ['./recap-heb.component.scss']
})
export class RecapHebComponent implements OnInit {

  @Input() data: RecapHebergement;

  constructor() { }

  ngOnInit() {
  }

}
