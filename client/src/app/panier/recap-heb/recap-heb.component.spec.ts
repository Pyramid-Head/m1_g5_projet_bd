import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecapHebComponent } from './recap-heb.component';

describe('RecapHebComponent', () => {
  let component: RecapHebComponent;
  let fixture: ComponentFixture<RecapHebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecapHebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecapHebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
