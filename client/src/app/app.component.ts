import { Component } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {DataTransfertService} from './data-transfert.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {DetailsFestivalComponent} from './composantsFestival/details-festival/details-festival.component';
import {PanierVideComponent} from './popup/panier-vide/panier-vide.component';
import {InfoNonImplementeComponent} from './popup/info-non-implemente/info-non-implemente.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';

  constructor(private dts: DataTransfertService, private router: Router, private dialog: MatDialog) {}

  changerPage(page: string) {
    switch (page) {
      case 'panier' : this.dts.idReservation !== '' ? this.router.navigate(['/monPanier']) : this.openDialog();
                      break;
      case 'festivals' : this.openDialogNonFait();
                         break;
      case 'hebergements' : this.openDialogNonFait();
                            break;
    }
  }

  openDialog() {
   this.dialog.open(PanierVideComponent, {
      width: '40%',
      height: '75',
      closeOnNavigation: true
    });
  }

  openDialogNonFait() {
    this.dialog.open(InfoNonImplementeComponent, {
      width: '40%',
      height: '75',
      closeOnNavigation: true
    });
  }

}
