// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: 'AIzaSyBEwul1yaolqFJzwvc-gbZVAVJYCMYYXvo',
  authDomain: 'm1-festibed.firebaseapp.com',
  databaseURL: 'https://m1-festibed.firebaseio.com',
  projectId: 'm1-festibed',
  storageBucket: 'm1-festibed.appspot.com',
  messagingSenderId: '1098227873187',
  appId: '1:1098227873187:web:022575691fba4174fa1616',
  measurementId: 'G-NGPCVP16X5'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
